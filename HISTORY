#########################
ew2openapi Release History
#########################

* Release 1.19.0-rc1    (2023/02/10)
   - Compile ew2openapi dependent libraries by makefile.unix. Issue #59 (2022/03/16)
   - Add parameter ServiceType to handle different services. Issue #16 (2023/01/23)
   - Start developing Kafka handles. Issue #66 (2023/01/23)
   - Start developing OSC handles. Issue #67 (2023/01/23)
   - Fix CFLAGS and LDFLAGS for Kafka handles. Issue #66 (2023/01/24)
   - Fix CFLAGS and LDFLAGS in makefile.unix. Issue #64 #65 #66 #67 (2023/01/24)
   - For Kafka enable-static --disable-shared --disable-curl --disable-zlib. Issue #66 (2023/02/01)
   - Make general use of stand-alone mode as Conversion Tool to write to file or send to Caravel,Kafka,RabbitMQ,OSC. Issue #68, #16
   - Update configuration files in params directory. Issue #68, #16
   - Update json test files. Issue #68, #16
   - Make working embryonic sending  message to Kafka Topic. Issue #66
   - Major improvement to ew2openapi conversion tool in stand-alone mode. Issue #68, #16
   - Fix for running ew2openapi as Earthworm module. Issue #68, #16
   - Fix makefile.unix for testing. Issue #68, #16

* Release 1.18.0    (2022/03/16)
   - In TYPE_MAGNITUDE json format, set Period1 and Period2 to NULL if MAG_NULL. Issue #57 (2022/03/16)
   - Upgrade json-c to 0.14-20200419. Issue #58 (2022/03/16)

* Release 1.17.0    (2022/03/09)
   - Use ARC_EXTRA_FIELDS officially added in Earthworm Git Repository. Issue #25 (2022/03/09)
     https://gitlab.com/seismic-software/earthworm/-/issues/49
   - Checkout new Git Earthworm Repository instead of old Subversion. Issue #55 (2022/03/09)

* Release 1.16.0    (2022/01/11)
   - Add script ew2openapi_logplayer.sh. Issue #51 (2021/11/05)
   - CAST TYPE_PICK_SCNL.pickWeight to integer (not a string). Issue #52 (2021/11/05)
   - Set originId n TYPE_HYP2000ARC and TYPE_MAGNITUDE json messages using quakeId and version. Issue #53, caravel/dante8#67 (2021/11/19)

* Release 1.15.0    (2021/10/11)
   - Change route API from quakdb/status to /status. Issue #50, #43 (2021/10/11)

* Release 1.14.0    (2021/02/15)
   - Add check quakdb/status API by function ew2openapi_handler_quakedb_ws_check_status(). Issue #43 (2021/01/22)
   - Implement handling of http code 428 (Precondition Required) with HANDLER_STATUS_ERROR_LIMITED_RETRIES_WITHIN_TIME. Max 60 seconds. Issue #44 (2021/02/15)

* Release 1.13.0    (2021/01/22)
   - Add warning message about delay message handling and number of items in queue to process. Issue #35 (2021/01/20)
   - Move ew2openapi test in makefile.unix. Issue #40 (2021/01/15)
   - Optimize compilation and clean code. Issue #41 (2021/01/20)
   - Set User-Agent in request header based on ew2openapi name and version. Issue #42 (2021/01/21)

* Release 1.12.0    (2020/12/18)
   - Checkout Earthworm from official Subversion repository in gitlab-ci.yml. Issue #14 (2020/04/20)
   - Major changes in source for making easy new system integrations. Issue #31 (2020/04/20)
   - Add handling of http codes 422 (Unprocessable Entity) and 428 (Precondition Required). Issue #33 (2020/07/16)
   - Add OSC handler. (2020/04/21)
   - Fix bug after calling cpqueuering(). Issue #34 (2020/07/28)
   - Handle http code 422 (Unprocessable Entity) by HANDLER_STATUS_ERROR_NO_RETRIES. Issue #36 (2020/07/28)
   - Fixing thread managing. Issue #37 (2020/07/29)
   - Use DateTime Format RFC3339 / ISO 8601. Issue #38 (2020/11/30)
   - Change BaseApiURL and substitude '_' with '-'. (2020/12/18)

* Release 1.11.2    (2020/05/20)
   - Under development reading extra fields in ARC structure related to magnitude values. Issue #25 (2020/03/12)
   - Hotfix 1.11.1: Fix make -f makefile.unix test for ARC_EXTRA_FIELDS. Issue #25 (2020/03/12)
   - Hotfix 1.11.2: Improve README.md. (2020/05/20)

* Release 1.10.4    (2020/03/11)
   - Remove dependency from EW_LOG and logit() when run as stand-alone mode. Issue #15 (2018/04/24)
   - Handle mutual use of sending messages to Webservice or RabbitMQ. Issue #16 (2018/04/24)
   - Start developing error exception for RabbitMQ. Issue #17 (2018/04/24)
   - Remove send e-mail from code and parameters. Issue #18 (2018/04/24)
   - Several fixes and improvements. (2018/04/24)
   - Add TYPE_STRONGMOTIONII JSON conversion. Issue #19 (2018/05/22)
   - Upgrade json-c to 0.13.1. Issue #23 (2018/06/08)
   - Hotfix 1.10.1: Fix curl LDFLAGS in makefile.unix. (2018/06/11)
   - Hotfix 1.10.2: Improve and clean makefile.unix. (2018/06/11)
   - Hotfix 1.10.3: Fix LDFLAGS curl static in makefile.unix. (2018/06/11)
   - Hotfix 1.10.4: Add case for http code 429: // Too Many Requests. Issue #13 (2020/03/11)

* Release 1.9.1    (2018/04/20)
   - Add handler for classifying HTTP CODE and manage errors to retry or not to POST message. Issue #13 (2018/04/20)
   - Add parameter ApiBaseUrl in ew2openapi.d and earthworm_commonvars.d. (2018/04/20)
   - Hotfix 1.9.1: Print Api Url with complete route for specific message.

* Release 1.8.5    (2018/04/19)
   - Change version of hypocenter to alphabetic name. Issue #10 (2018/02/01)
   - Start handling exception based on http code. Issue #13 (2018/04/17)
   - Implement ew2openapi as Earthworm messages command line conversion tool. Issue #12 (2018/04/18)
   - Hotfix 1.8.1: fix format in printf for msgSize variable. Issue #12 (2018/04/18)
   - Hotfix 1.8.2: major fix. Parsing ARC based on parse_arc() ew library function. (2018/04/19)
   - Hotfix 1.8.3: just set version. (2018/04/19)
   - Hotfix 1.8.4: Add and test gitlab-ci.yml. Issue #14 (2018/04/19)
   - Hotfix 1.8.5: Fixes 1) ew2openapi_get_name_version() const char return value, 2) remove always false condition. (2018/04/19)

* Release 1.7.1    (2017/11/02)
   - Add user and hostname to ewLogo. Issue #1.
   - Rename module to ew2openapi.
   - Hotfix 1.7.1: Add curl_global_init() and curl_global_cleanup() before starting and after ending threads. Issue #9 (2017/11/02)

* Release 1.6.0    (2017/04/06)
   - Major change: add ewLogo and ewMessage fields into all JSON Earthworm message.
   - In TYPE_PICK_SCNL and TYPE_PICK_CODA rename sequenceNumber to pickId.
   - In TYPE_QUAKE2K rename qkSeq to quakeId.
   - In TYPE_LINK rename pkSeq to pickId and qkSeq to quakeId.
   - In TYPE_PICK_SCNL change pAmplitude into JSON array.
   - In TYPE_PICK_CODA change caav into JSON array.
   - In TYPE_PICK_SCNL and TYPE_PICK_CODA remove suffix 'Code' from station, network, component, location.
   - In TYPE_HYP2000ARC phase rename JSON field 'site' to 'sta'.

* Release 1.5.2    (2017/04/05)
   - In GeoJSON format use only longitude and latitude, without elevation.
   - Add phases in GeoJSON conversion for TYPE_HYP2000ARC.
   - Change caav and ccntr into JSON array.
   - Hotfix 1.5.1: Rename ewMsgType to ewType JSON field name.
   - Hotfix 1.5.2: Rename ewMsgType to ewType JSON field name also in README.md

* Release 1.4.0    (2017/04/05)
   - Rename MessageConversion to SendMessageFormat and allow multiple declarations.
   - Rename None to RAW for parameter SendMessageFormat in ew2openapi.d.
   - Add GeoJSON conversion for TYPE_PICK_SCNL, TYPE_QUAKE2K and TYPE_HYP2000ARC.

* Release 1.3.1    (2017/04/04)
   - Add TYPE_MAGNITUDE JSON conversion.
   - Add TYPE_QUAKE2K geoJSON conversion.
   - Add TYPE_LINK JSON conversion.
   - Add TYPE_ERROR JSON conversion.
   - Add TYPE_HEARTBEAT JSON conversion.
   - Add TYPE_PICK_CODA JSON conversion.
   - Add parameter MessageConversion to ew2openapi.d. It can be None, JSON or GeoJSON.
   - Add to the routing key the message conversion type as FORMAT_EW, FORMAT_JSON or FORMAT_GEOJSON.
   - Hotfix 1.3.1: update README.md renaming Earthworm field names in json format.

* Release 1.2.0    (2017/03/31)
   - Fix makefile.unix for static compilation.
   - Add TYPE_HYP2000ARC geoJSON conversion.

* Release 1.1.0    (2017/03/30)
   - Move sources to src directory.
   - Move *.d and *.desc to params directory.

* Release 0.1.0    (2017/03/30)
   - Initial release
   - Add TYPE_PICK_SCNL geoJSON conversion.

