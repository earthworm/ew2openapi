#-----------------------------------------------------------------------------
#                    Earthworm Common Variables
#
#  Define values for the Earthworm Common variables.
#  Values of the variables already set within the shell environment will be
#  overwritten by the declarations contained within this file.
#
#  Variables declared inside this file can be recalled within all .d file using
#  the following syntax:   ${VARIABLE_NAME}
#  The respective values will be expanded within the original .d file.
#  The variable expansion for this file is implemented only from the
#  environment shell, avoiding recursivity.
#
#  An example copy of earthworm_commonvars.d resides in the vX.XX/environment
#  directory of this Earthworm distribution.
#  A copy of earthworm_commonvars.d should be placed in your EW_PARAMS
#  directory.
#
#  Syntax:
#          SetEnvVariable VARIABLE_NAME VARIABLE_VALUE
#          SetEnvVariable VARIABLE_NAME "VARIABLE_VALUE"
#
#  VARIABLE_NAME can contain only characters [A-Za-Z0-9_] and it must start
#  with a nonnumeric character.
#  VARIABLE_VALUE can contain all characters execpt for '}',
#                 use quote when it contains spaces.
#  The maximum length for VARIABLE_NAME and VARIABLE_VALUE is 255 characters.
#
#  Best practice:
#     - Avoid to declare EW_HOME, EW_VERSION, EW_LOG, EW_PARAMS, ...
#
#  Examples:
#
#     1) SetEnvVariable MYDERIVEDVAR "${ENVVARFROMSHELL}/myfile"
#        # Value for ENVVARFROMSHELL will be substituted only if the variable
#        # has been set within the shell environment before launching
#        # the module startstop.
#
#     2) SetEnvVariable MYVAR1 xxxxxxx
#        SetEnvVariable MYVAR2 ${MYVAR1}
#        # MYVAR2 will not expanded with variable  from this file, but only
#        # from the shell environment if MYVAR1 has been set outside from here.
#
#-----------------------------------------------------------------------------

# SetEnvVariable EWLOGFILE 1
# SetEnvVariable STATIONFILE "${EW_PARAMS}/italy.hinv"

SetEnvVariable INSTALLATION_ID       INST_INGV

SetEnvVariable EWMOLE_INSTANCENAME   ew2openapi_convtool

SetEnvVariable MYHOSTNAME            ew2openapi_test.int.ingv.it

SetEnvVariable EW2OPENAPI_APIBASEURL http://caravel.int.ingv.it:80/api/

SetEnvVariable RABBITMQ_HOSTNAME     rabbitmq1-rm.int.ingv.it
SetEnvVariable RABBITMQ_PORT         5672
SetEnvVariable RABBITMQ_EXCHANGE     messages
SetEnvVariable RABBITMQ_VIRTUALHOST  /ew
SetEnvVariable RABBITMQUSER          ew
SetEnvVariable RABBITMQPASS          ewwriter0

SetEnvVariable KAFKA_BROKERS         hgp6.int.ingv.it:19092
SetEnvVariable KAFKA_TOPIC_PREFIX    ew2openapi_test

SetEnvVariable OSC_SERVER            10.0.4.198
SetEnvVariable OSC_SERVER_PORT       57120

