#
#   ew2openapi generic configuration file for HYPO_RING
#
#
 MyModuleId     MOD_EW2OPENAPI       # module id for this program

# Multiple declaration of InRing. The first one is used for reporting
# heartbeat and status messages. Be careful with messages duplicated
# in different rings.
# InRing         WAVE_RING           # transport ring to use for input
 InRing         PICK_RING           # transport ring to use for input
# InRing         HYPO_RING           # transport ring to use for input
# InRing         GMEW_RING           # transport ring to use for input
# Earlybird Message types
#  InRing         ALARM_RING          # transport ring to use for input

 HeartBeatInt   30                  # EW internal heartbeat interval (sec)
 LogFile        1                   # If 0, don't write logfile
                                    # if 2, write to module log but not to
                                    # stderr/stdout .

# Unique name that indentifies the EW instance is usually declared as variable
EWInstanceName    ${EWMOLE_INSTANCENAME}

# Logos of messages to export to client systems
# Each message of a given logo will be written to a file with the specified
# File Suffix at the end of the file name folloing `.'.
# Do NOT include a `.' in the suffix.
# Use as many GetMsgLogo commands as you want.

#              Installation           Module       Message Type

# Message types usually read from WAVE_RING
 # GetMsgLogo    ${EW_INSTALLATION}   MOD_WILDCARD   TYPE_TRACEBUF2

# Message types usually read from PICK_RING
# GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_PICK_SCNL
# GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_CODA_SCNL
 GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_QUAKE2K
# GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_LINK

# Message types usually read from HYPO_RING
 GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_HYP2000ARC
 GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_MAGNITUDE
# GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_STRONGMOTIONII

# Earlybird Message types
# GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_PICKTWC
# GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_HYPOTWC
# GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_ALARM

# Other message types
# WARNING: if you use copystatus, the messages TYPE_HEARTBEAT and TYPE_ERROR
# are duplicated on different rings. If you want to archive them, read only
# from the destination ring of the copystatus.
# GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_HEARTBEAT
# GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_ERROR


 MaxMsgSize        113400       # maximum size (bytes) for input msgs
                                # MAX_BYTES_PER_EQ from earthworm_defs.h
 QueueSize         50000         # number of msgs to buffer. Useful in case of temporary
				# database connection failures, ew2openapi buffers the
                                # loading operations and postpone their executions until
                                # the connection was reestablished.

# ServiceType is mutually exclusive.
ServiceType CaravelWS
# ServiceType Kafka
# ServiceType RabbitMQ
# ServiceType OSC

# ew2openapi Webservice connection parameters
ApiBaseUrl ${EW2OPENAPI_APIBASEURL}

# Seconds to wait after a Service Error
WaitSecAfterServiceError 5

# Rabbit DB connection parameters
# RMQHostname        ${RABBITMQ_HOSTNAME}
# RMQPort            ${RABBITMQ_PORT}
# RMQExchange        ${RABBITMQ_EXCHANGE}
# RMQVirtualHost     ${RABBITMQ_VIRTUALHOST}
# RMQUsername        ${RABBITMQUSER}
# RMQPassword        ${RABBITMQPASS}

# Kafka connection parameters. Used when ServiceType is Kafka.
# Set bootstrap broker(s) as a comma-separated list of
# host or host:port (default port 9092).
# KafkaBrokers       ${KAFKA_BROKERS}
# KafkaTopicPrefix   ${KAFKA_TOPIC_PREFIX}

# OSC connection parameters. Used when ServiceType is OSC.
# OscServer       ${OSC_SERVER}
# OscServerPort   ${OSC_SERVER_PORT}

# SendMessageFormat declare which format use to convert Earthworm messages and send to RabbitMQ server.
# If no SendMessageFormat is declared then default is only RAW.
# Available message conversion format are RAW, JSON or GeoJSON. JSON and GeoJSON arei mutually exclusive.
# RAW format:     messages are sent to RabbitMQ server as they are
# JSON format:    messages are converted to JSON format before sending to RabbitMQ server
# GeoJSON format: messages are converted to GeoJSON format before sending to RabbitMQ server.
#                 Only the following message types can be converted to GeoJSON:
#                     TYPE_PICK_SCNL, TYPE_CODA_SCNL, TYPE_QUAKE2K, TYPE_HYP2000ARC
#                 Other types of messages will be converted to JSON with no geographic features.

# SendMessageFormat RAW
SendMessageFormat JSON
# SendMessageFormat GeoJSON

# Station list needed only for GeoJSON conversion
 maxsite     10000
 site_file   params/all_stations.hinv

