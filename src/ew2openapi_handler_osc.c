#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include "lo/lo.h"

#include <earthworm.h>

#include "ew2openapi_logit.h"
#include "ew2openapi_handler_osc.h"
#include "ew2openapi_handler_status.h"

lo_address osc_address = NULL;

int ew2openapi_handler_osc_init(TYPE_PARAMS *params) {
	int ret = 0;
	char osc_port_char[256];

	/* TODO */

	/* osc_address = lo_address_new(NULL, "7770"); */

	snprintf(osc_port_char, 255, "%d", params->osc_port);

	osc_address = lo_address_new(params->osc_hostname, osc_port_char);

	ew2openapi_logit( "et", "OSC SERVER: %s:%s.\n", params->osc_hostname, osc_port_char);

	if(osc_address) {
		ew2openapi_logit( "et", "OSC connection: ok.\n");
	} else {
		ew2openapi_logit( "et", "OSC connection: failed.\n");
	}
	return ret;
}

int ew2openapi_handler_osc_destroy(TYPE_PARAMS *params) {
	int ret = 0;
	/* TODO */
	return ret;
}

int ew2openapi_handler_osc_handle(TYPE_DATA_HANDLER *data_handler) {
	int     handler_status_code = HANDLER_STATUS_OK;

	/* TODO */

	/* send a jamin scene change instruction with a 32bit integer argument */
	lo_send(osc_address, "/chat", "i", 2);
	/* lo_send(osc_address, "/jamin", "i", 10); */

	ew2openapi_logit( "et", "lo_send ... \n");

	return handler_status_code;
}
