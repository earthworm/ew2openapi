#ifndef EW2OPENAPI_HANDLER_CONVTOOL_H
#define EW2OPENAPI_HANDLER_CONVTOOL_H 1

#include <transport.h>
#include "ew2openapi_params.h"

int ew2openapi_handler_convtool_init(TYPE_PARAMS *params);

int ew2openapi_handler_convtool_destroy(TYPE_PARAMS *params);

int ew2openapi_handler_convtool_handle(TYPE_DATA_HANDLER *data_handler);

int earthworm_message_conversion_tool(TYPE_PARAMS *params, int (* p_data_handler_func_handle)(TYPE_DATA_HANDLER *));

#endif

