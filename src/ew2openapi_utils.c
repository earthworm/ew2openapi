#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include <earthworm.h>

#include "ew2openapi_utils.h"


void timestr_to_datetime_str_iso8601_utc(char out_str[EW2OPENAPI_DATETIME_MAXLEN_STR], char *in_str) {
	/*
	   INPUT
	   20090812001522.56
	   01234567890123456

	   OUTPUT
	   2009-08-12 12:35:37.160000
	   01234567890123456789012345
	   */
	out_str[0] = in_str[0];
	out_str[1] = in_str[1];
	out_str[2] = in_str[2];
	out_str[3] = in_str[3];

	out_str[4] = '-';

	out_str[5] = in_str[4];
	out_str[6] = in_str[5];

	out_str[7] = '-';

	out_str[8] = in_str[6];
	out_str[9] = in_str[7];

	out_str[10] = 'T';

	out_str[11] = in_str[8];
	out_str[12] = in_str[9];

	out_str[13] = ':';

	out_str[14] = in_str[10];
	out_str[15] = in_str[11];

	out_str[16] = ':';

	out_str[17] = in_str[12];
	out_str[18] = in_str[13];

	out_str[19] = in_str[14];

	out_str[20] = in_str[15];
	out_str[21] = in_str[16];

	out_str[22] = '0';

	out_str[23] = 'Z';
	out_str[24] = 0;

}

int epoch_to_datetime_str_iso8601_utc(char *out_str, int len_out_str, double time_d) {
	time_t time_t_start_time;
	struct tm *tm_start_time;
	double integral;

	out_str[len_out_str - 1] = 0;

	if(time_d > 0.0) {
		time_t_start_time = (time_t) time_d;
	} else {
		time_t_start_time = 0;
	}
	tm_start_time = gmtime(&time_t_start_time);

	snprintf(out_str, len_out_str - 1, "%04d-%02d-%02dT%02d:%02d:%02d.%03ldZ",
			tm_start_time->tm_year + 1900,
			tm_start_time->tm_mon + 1,
			tm_start_time->tm_mday,
			/*
			   tm_start_time->tm_yday + 1,
			   */
			tm_start_time->tm_hour,
			tm_start_time->tm_min,
			tm_start_time->tm_sec,
			(long) ( (modf(time_d, &integral) * 1000.0) + 0.5)
			);

	return 0;
}

