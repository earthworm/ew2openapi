#ifndef EW2OPENAPI_HANDLER_QUAKEDB_WS_H
#define EW2OPENAPI_HANDLER_QUAKEDB_WS_H 1

#include <curl/curl.h>
#include <transport.h>
#include "ew2openapi_params.h"

int ew2openapi_handler_quakedb_ws_init(TYPE_PARAMS *params);

int ew2openapi_handler_quakedb_ws_check_status(char *ApiBaseUrl);

int ew2openapi_handler_quakedb_ws_destroy(TYPE_PARAMS *params);

int ew2openapi_handler_quakedb_ws_handle(TYPE_DATA_HANDLER *data_handler);


#endif

