#include <stdio.h>
#include <string.h>

#include "ew2openapi_eq_version_name.h"

const char *ew2openapi_version_name[MAX_EW2OPENAPI_VERSION_NAME] = {
	"ew prelim",
	"ew rapid",
	"ew final"
};

const char *ew2openapi_version_name_unknown = "ew unknown version";

const char *ew2openapi_get_name_version(int version) {
	const char *ret = NULL;

	if(version >=0  &&  version < MAX_EW2OPENAPI_VERSION_NAME) {
		ret = ew2openapi_version_name[version];
	} else {
		ret = ew2openapi_version_name_unknown;
	}
	return ret;
}

