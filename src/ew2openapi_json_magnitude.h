#ifndef EW2OPENAPI_JSON_MAGNITUDE_H
#define EW2OPENAPI_JSON_MAGNITUDE_H 1

#include <transport.h>

char *get_json_string_from_magnitude_ew_msg(char *msg, int msg_size, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName);

#endif

