#ifndef EW2OPENAPI_JSON_QUAKELINK_H
#define EW2OPENAPI_JSON_QUAKELINK_H 1

#include <transport.h>

typedef struct {
	long   qkseq;
	char   timestr[20];
	double lat;
	double lon;
	double z;
	float rms;
	float dmin;
	float ravg;
	float gap;
	int nph;
} QUAKE2K;

typedef struct {
	long qkseq;
	int iinstid;
	int isrc;
	int pkseq;
	int iphs;
} LINK;

int read_quake2k(char *msg, int msg_size, QUAKE2K *quake);
int read_link(char *msg, int msg_size, LINK *link);

char *get_json_string_from_quake2k_ew_msg(char *msg, int msg_size, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName, char flag_geojson);
char *get_json_string_from_link_ew_msg(char *msg, int msg_size, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName);

#endif

