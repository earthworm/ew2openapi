#ifndef EW2OPENAPI_HANDLER_STATUS_H
#define EW2OPENAPI_HANDLER_STATUS_H 1

/* Status for OK */
#define HANDLER_STATUS_OK                                 0

/* Status for error which implies infinity retries */
#define HANDLER_STATUS_ERROR_INFINITY_RETRIES             1
/* Status for error which implies limited retries */
#define HANDLER_STATUS_ERROR_LIMITED_RETRIES              2
/* Status for error which implies limited retries but within max time interval */
#define HANDLER_STATUS_ERROR_LIMITED_RETRIES_WITHIN_TIME  3
/* Status for error which implies no retries */
#define HANDLER_STATUS_ERROR_NO_RETRIES                   4

#endif

