#include <stdio.h>
#include <json-c/json.h>

#include <earthworm.h>
#include <chron3.h>

#include "ew2openapi_logit.h"
#include "ew2openapi_json_quakelink.h"
#include "ew2openapi_utils.h"
#include "ew2openapi_json_utils.h"
#include "ew2openapi_geojson_utils.h"


#define MAX_JSON_STRING_QUAKE2K 4096
char *get_json_string_from_quake2k_ew_struct(QUAKE2K *quake, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName, char flag_geojson) {
	/*Creating a json object*/
	json_object *jobj = json_object_new_object();
	json_object *jobj_ew_message = json_object_new_object();

	static char json_string[MAX_JSON_STRING_QUAKE2K];
	char myjson_string_datetime[EW2OPENAPI_DATETIME_MAXLEN_STR];

	json_string[MAX_JSON_STRING_QUAKE2K - 1] = 0;

	timestr_to_datetime_str_iso8601_utc(myjson_string_datetime, quake->timestr);

	snprintf(json_string, MAX_JSON_STRING_QUAKE2K - 1, "CALL sp_ins_ew_quake2k('%s','%s', %ld, '%s', %lf, %lf, %lf, %f, %f, %f, %f, %d);",
			ewInstanceName,
			GetModIdName(reclogo->mod),
			quake->qkseq,
			/*
			   quake->timestr,
			   */
			myjson_string_datetime,
			quake->lat,
			quake->lon,
			quake->z,
			quake->rms,
			quake->dmin,
			quake->ravg,
			quake->gap,
			quake->nph
			);

	/* ew2openapi_logit("et", "DEBUG: timestr (%s)   myjson_string_datetime (%s)\n", quake->timestr, myjson_string_datetime); */

	/* Add Earthworm logo to JSON object */
	json_object_ew_logo_objects_add(jobj, reclogo, ewInstanceName, ewUserName, ewHostName);

	json_object_object_add(jobj_ew_message,"quakeId", json_object_new_int64(quake->qkseq));
	json_object_object_add(jobj_ew_message,"originTime", json_object_new_string(myjson_string_datetime));
	json_object_object_add(jobj_ew_message,"latitude", json_object_new_double_with_precision(quake->lat, 4));
	json_object_object_add(jobj_ew_message,"longitude", json_object_new_double_with_precision(quake->lon, 4));
	json_object_object_add(jobj_ew_message,"depth", json_object_new_double_with_precision(quake->z, 2));
	json_object_object_add(jobj_ew_message,"rms", json_object_new_double_with_precision(quake->rms, 2));
	json_object_object_add(jobj_ew_message,"dmin", json_object_new_double_with_precision(quake->dmin, 1));
	json_object_object_add(jobj_ew_message,"ravg", json_object_new_double_with_precision(quake->ravg, 1));
	json_object_object_add(jobj_ew_message,"gap", json_object_new_double_with_precision(quake->gap, 0));
	json_object_object_add(jobj_ew_message,"nph", json_object_new_int64(quake->nph));

	/* Add Earthworm message to JSON object */
	json_object_ew_message_objects_add(jobj, jobj_ew_message);

	/* GeoJSON conversion */
	if(flag_geojson) {
		jobj = json_object_new_geojson_from_json_object(jobj, quake->lon, quake->lat);
	}

	/* Build JSON string from json object */
	snprintf(json_string, MAX_JSON_STRING_QUAKE2K - 1, "%s", json_object_to_json_string_ew2openapi(jobj));

	/* clean the json object */
	json_object_put(jobj);

	return json_string;
}

char *get_json_string_from_link_ew_struct(LINK *link, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName) {
	/*Creating a json object*/
	json_object *jobj = json_object_new_object();
	json_object *jobj_ew_message = json_object_new_object();

	static char json_string[MAX_JSON_STRING_QUAKE2K];

	json_string[MAX_JSON_STRING_QUAKE2K - 1] = 0;

	snprintf(json_string, MAX_JSON_STRING_QUAKE2K - 1, "CALL sp_ins_ew_link('%s', '%s', %ld, %d, %d);",
			ewInstanceName,
			GetModIdName(reclogo->mod),
			link->qkseq,
			/*
			   link->iinstid,
			   link->isrc,
			   */
			link->pkseq,
			link->iphs
			);

	/* Add Earthworm logo to JSON object */
	json_object_ew_logo_objects_add(jobj, reclogo, ewInstanceName, ewUserName, ewHostName);

	json_object_object_add(jobj_ew_message,"quakeId", json_object_new_int64(link->qkseq));
	json_object_object_add(jobj_ew_message,"pickId", json_object_new_int64(link->pkseq));
	json_object_object_add(jobj_ew_message,"iphs", json_object_new_int64(link->iphs));

	/* Add Earthworm message to JSON object */
	json_object_ew_message_objects_add(jobj, jobj_ew_message);

	snprintf(json_string, MAX_JSON_STRING_QUAKE2K - 1, "%s", json_object_to_json_string_ew2openapi(jobj));

	/* clean the json object */
	json_object_put(jobj);

	return json_string;
}

/****************************************************************************/
/*  from eqassemple eqas_quake2k() processes a TYPE_QUAKE2K message from binder              */
/****************************************************************************/
int read_quake2k(char *msg, int msg_size, QUAKE2K *quake) {
	double     tOrigin;
	// double     tNow;
	int        narg;
	char       Text[4096];

	/* Read info from ascii message
	 ******************************/
	narg =  sscanf( msg,
			"%*d %*d %ld %s %lf %lf %lf %f %f %f %f %d",
			&(quake->qkseq), quake->timestr, &(quake->lat), &(quake->lon), &(quake->z),
			&(quake->rms), &(quake->dmin), &(quake->ravg), &(quake->gap), &(quake->nph) );

	if ( narg < 10 ) {
		sprintf( Text, "eqas_quake2k: Error reading ascii quake msg: %s", msg );
		ew2openapi_logit("et", "%s", Text);
		return EW_FAILURE;
	}

	// tNow = tnow();
	tOrigin = julsec17( quake->timestr );
	if ( tOrigin == 0. ) {
		sprintf( Text, "eqas_quake2k: Error decoding quake time: %s", quake->timestr );
		ew2openapi_logit("et", "%s", Text);
		return EW_FAILURE;
	}

	return EW_SUCCESS;
}

/****************************************************************************/
/* from eqassemble eqas_link() processes a TYPE_LINK message                                */
/****************************************************************************/
int read_link(char *msg, int msg_size, LINK *link) {
	int           narg;
	char          Text[4096];

	narg  = sscanf( msg, "%ld %d %d %d %d",
			&(link->qkseq), &(link->iinstid), &(link->isrc), &(link->pkseq), &(link->iphs) );

	if ( narg < 5 ) {
		sprintf( Text, "eqas_link: Error reading ascii link msg: %s", msg );
		ew2openapi_logit("et", "%s", Text);
		return EW_FAILURE;
	}

	return EW_SUCCESS;
}

char *get_json_string_from_quake2k_ew_msg(char *msg, int msg_size, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName, char flag_geojson) {
	char *ret = NULL;
	char Text[1024];
	QUAKE2K quake;

	/* Read the pick into an EWPICK struct
	 *  *************************************/
	if( read_quake2k( msg, msg_size, &quake ) != EW_SUCCESS )
	{
		sprintf( Text, "Error reading pick: %s", msg );
		ew2openapi_logit("et", "%s", Text);
	} else {
		ret = get_json_string_from_quake2k_ew_struct(&quake, reclogo, ewInstanceName, ewUserName, ewHostName, flag_geojson);
	}

	return ret;
}

char *get_json_string_from_link_ew_msg(char *msg, int msg_size, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName) {
	char *ret = NULL;
	char Text[1024];
	LINK link;

	/* Read the coda into an EWCODA struct
	 *  *************************************/
	if( read_link( msg, msg_size, &link ) != EW_SUCCESS )
	{
		sprintf( Text, "Error reading coda: %s", msg );
		ew2openapi_logit("et", "%s", Text);
	} else {
		ret = get_json_string_from_link_ew_struct(&link, reclogo, ewInstanceName, ewUserName, ewHostName);
	}

	return ret;
}

