#include <stdio.h>
#include <json-c/json.h>
#include <string.h>

#include <earthworm.h>

#include "ew2openapi_json_heartbeat.h"
#include "ew2openapi_utils.h"
#include "ew2openapi_json_utils.h"

#define EW2OPENAPI_MAXLEN_HEARTBEAT_MSG 4096

char *get_json_string_from_heartbeat_ew_msg(char *msg, int msg_size, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName) {
	/*Creating a json object*/
	json_object *jobj = json_object_new_object();
	json_object *jobj_ew_message = json_object_new_object();

	static char json_string[EW2OPENAPI_MAXLEN_HEARTBEAT_MSG];
	long int time_heartbeat;
	char myjson_string_datetime_heartbeat[EW2OPENAPI_DATETIME_MAXLEN_STR];
	long pid;

	json_string[EW2OPENAPI_MAXLEN_HEARTBEAT_MSG - 1] = 0;

	sscanf(msg, "%ld %ld", &time_heartbeat, &pid);
	epoch_to_datetime_str_iso8601_utc(myjson_string_datetime_heartbeat, EW2OPENAPI_DATETIME_MAXLEN_STR, time_heartbeat);

	snprintf(json_string, EW2OPENAPI_MAXLEN_HEARTBEAT_MSG - 1, "CALL sp_ins_ew_heartbeat('%s', '%s', '%s', %ld);",
			ewInstanceName,
			GetModIdName(reclogo->mod),
			myjson_string_datetime_heartbeat,
			pid
			);

	/* Add Earthworm logo to JSON object */
	json_object_ew_logo_objects_add(jobj, reclogo, ewInstanceName, ewUserName, ewHostName);

	json_object_object_add(jobj_ew_message,"time", json_object_new_string(myjson_string_datetime_heartbeat));
	json_object_object_add(jobj_ew_message,"pid", json_object_new_int64(pid));

	/* Add Earthworm message to JSON object */
	json_object_ew_message_objects_add(jobj, jobj_ew_message);

	/* Build JSON string from json object */
	snprintf(json_string, EW2OPENAPI_MAXLEN_HEARTBEAT_MSG - 1, "%s", json_object_to_json_string_ew2openapi(jobj));

	/* clean the json object */
	json_object_put(jobj);

	return json_string;
}

