#ifndef EW2OPENAPI_HANDLER_RABBITMQ_H
#define EW2OPENAPI_HANDLER_RABBITMQ_H 1

#include <transport.h>
#include "ew2openapi_params.h"


#include <amqp_tcp_socket.h>
#include <amqp.h>
#include <amqp_framing.h>

#include "amqp_utils.h"

#define  RABBITMQ_DEFAULT_PORT       5672   /* Default RabbitMQ port */


int ew2openapi_handler_rabbitmq_init(TYPE_PARAMS *params);

int ew2openapi_handler_rabbitmq_destroy(TYPE_PARAMS *params);

int ew2openapi_handler_rabbitmq_handle(TYPE_DATA_HANDLER *data_handler);

#endif

