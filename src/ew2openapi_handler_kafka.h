#ifndef EW2OPENAPI_HANDLER_KAFKA_H
#define EW2OPENAPI_HANDLER_KAFKA_H 1

#include "librdkafka/rdkafka.h"
#include <transport.h>
#include "ew2openapi_params.h"

#define  KAFKA_DEFAULT_PORT       19092   /* Default Kafka port */


int ew2openapi_handler_kafka_init(TYPE_PARAMS *params);

int ew2openapi_handler_kafka_destroy(TYPE_PARAMS *params);

int ew2openapi_handler_kafka_handle(TYPE_DATA_HANDLER *data_handler);

#endif

