#ifndef EW2OPENAPI_JSON_STRONGMOTIONII_H
#define EW2OPENAPI_JSON_STRONGMOTIONII_H 1

#include <transport.h>

char *get_json_string_from_strongmotionII_ew_msg(char *msg, int msg_size, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName);

#endif

