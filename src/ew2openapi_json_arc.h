#ifndef EW2OPENAPI_JSON_ARC_H
#define EW2OPENAPI_JSON_ARC_H 1

#include <transport.h>

/* TODO change logic to dynamic allocate json string */
/* At the moment set fixed length to 2 MB */
#define MAX_JSON_STRING_ARC ((1024 * 1024) * 2)

char *get_json_string_from_arc_ew_msg(char *msg, int msg_size, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName, char flag_geojson);

#endif

