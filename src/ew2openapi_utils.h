#ifndef EW2OPENAPI_UTILS_H
#define EW2OPENAPI_UTILS_H 1

#define EW2OPENAPI_DATETIME_MAXLEN_STR 40

int epoch_to_datetime_str_iso8601_utc(char *out_str, int len_out_str, double time_d);
void timestr_to_datetime_str_iso8601_utc(char out_str[EW2OPENAPI_DATETIME_MAXLEN_STR], char *in_str);

#endif

