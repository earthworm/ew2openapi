#include <stdio.h>

#include <earthworm.h>

#include "ew2openapi_json_utils.h"

#include "ew2openapi_logit.h"
/*
#include "ew2openapi_tracebuf.h"
*/
#include "ew2openapi_json_pickcoda.h"
#include "ew2openapi_json_arc.h"
#include "ew2openapi_json_magnitude.h"
#include "ew2openapi_json_strongmotionII.h"
#include "ew2openapi_json_quakelink.h"
#include "ew2openapi_json_error.h"
#include "ew2openapi_json_heartbeat.h"
/*
#include "ew2openapi_json_eb_picktwc.h"
#include "ew2openapi_json_eb_hypotwc.h"
#include "ew2openapi_json_eb_alarm.h"
*/


/* General function to guarantee omogenous json export. Wrapper json_object_to_json_string_ext() */
const char *json_object_to_json_string_ew2openapi(struct json_object *obj) {
	return json_object_to_json_string_ext(obj, JSON_C_TO_STRING_PLAIN);
}

/* Return new string json object from a single character */
struct json_object *json_object_new_string_from_char (char c) {
	char str[3];
	snprintf(str, 3, "%c", c);
	return json_object_new_string(str);
}

/* Return new double json object rounded to specific precision */
struct json_object* json_object_new_double_with_precision(double d, int precision) {
	struct json_object* ret = NULL;
	char str_format[128];
	char str_double_output[128];
	snprintf(str_double_output, 127, "%.f", d);
	if(precision >= 0  &&  precision <= 10) {
		snprintf(str_format, 127, "%%.%df", precision);
		snprintf(str_double_output, 127, str_format, d);
		ret = json_object_new_double_s(d, str_double_output);
	} else {
		/* default */
		ret = json_object_new_double(d);
	}
	return ret;
}

void json_object_ew_logo_objects_add(struct json_object *jobj, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName) {
	struct json_object* jobj_ew_logo = json_object_new_object();
	json_object_object_add(jobj_ew_logo,"type", json_object_new_string(GetTypeName(reclogo->type)));
	json_object_object_add(jobj_ew_logo,"module", json_object_new_string(GetModIdName(reclogo->mod)));
	json_object_object_add(jobj_ew_logo,"installation", json_object_new_string(GetInstName(reclogo->instid)));
	json_object_object_add(jobj_ew_logo,"instance", json_object_new_string(ewInstanceName));
	json_object_object_add(jobj_ew_logo,"user", json_object_new_string(ewUserName));
	json_object_object_add(jobj_ew_logo,"hostname", json_object_new_string(ewHostName));
	json_object_object_add(jobj,"ewLogo", jobj_ew_logo);
}

void json_object_ew_message_objects_add(struct json_object *jobj, struct json_object *jobj_ew_message) {
	json_object_object_add(jobj,"ewMessage", jobj_ew_message);
}


char *get_json_string_from_ew_msg(char *Wrmsg, int msgSize, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName, char *flag_geojson) {

	char    *json_string = NULL;

	unsigned char TYPE_ERROR;
	unsigned char TYPE_HEARTBEAT;
	unsigned char TYPE_TRACEBUF2;
	unsigned char TYPE_PICK_SCNL;
	unsigned char TYPE_CODA_SCNL;
	unsigned char TYPE_QUAKE2K;
	unsigned char TYPE_LINK;
	unsigned char TYPE_HYP2000ARC;
	unsigned char TYPE_PICKTWC;
	unsigned char TYPE_HYPOTWC;
	unsigned char TYPE_ALARM;
	unsigned char TYPE_MAGNITUDE;
	unsigned char TYPE_STRONGMOTIONII;

	GetType("TYPE_ERROR", &TYPE_ERROR);
	GetType("TYPE_HEARTBEAT", &TYPE_HEARTBEAT);
	GetType("TYPE_TRACEBUF2", &TYPE_TRACEBUF2);
	GetType("TYPE_PICK_SCNL", &TYPE_PICK_SCNL);
	GetType("TYPE_CODA_SCNL", &TYPE_CODA_SCNL);
	GetType("TYPE_QUAKE2K", &TYPE_QUAKE2K);
	GetType("TYPE_LINK", &TYPE_LINK);
	GetType("TYPE_HYP2000ARC", &TYPE_HYP2000ARC);
	GetType("TYPE_PICKTWC", &TYPE_PICKTWC);
	GetType("TYPE_HYPOTWC", &TYPE_HYPOTWC);
	GetType("TYPE_ALARM", &TYPE_ALARM);
	GetType("TYPE_MAGNITUDE", &TYPE_MAGNITUDE);
	GetType("TYPE_STRONGMOTIONII", &TYPE_STRONGMOTIONII);

	if(reclogo->type == TYPE_PICK_SCNL) {
		json_string = get_json_string_from_pick_scnl_ew_msg(Wrmsg, msgSize, reclogo, ewInstanceName, ewUserName, ewHostName, *flag_geojson);
	} else if(reclogo->type == TYPE_CODA_SCNL) {
		/* reset flag_geojson */
		*flag_geojson =  0;
		json_string = get_json_string_from_coda_scnl_ew_msg(Wrmsg, msgSize, reclogo, ewInstanceName, ewUserName, ewHostName);
	} else if(reclogo->type == TYPE_QUAKE2K) {
		json_string = get_json_string_from_quake2k_ew_msg(Wrmsg, msgSize, reclogo, ewInstanceName, ewUserName, ewHostName, *flag_geojson);
	} else if(reclogo->type == TYPE_LINK) {
		/* reset flag_geojson */
		*flag_geojson =  0;
		json_string = get_json_string_from_link_ew_msg(Wrmsg, msgSize, reclogo, ewInstanceName, ewUserName, ewHostName);
	} else if(reclogo->type == TYPE_HYP2000ARC) {
		json_string = get_json_string_from_arc_ew_msg(Wrmsg, msgSize, reclogo, ewInstanceName, ewUserName, ewHostName, *flag_geojson);
	} else if(reclogo->type == TYPE_MAGNITUDE) {
		/* reset flag_geojson */
		*flag_geojson =  0;
		json_string = get_json_string_from_magnitude_ew_msg(Wrmsg, msgSize, reclogo, ewInstanceName, ewUserName, ewHostName);
	} else if(reclogo->type == TYPE_ERROR) {
		/* reset flag_geojson */
		*flag_geojson =  0;
		json_string = get_json_string_from_error_ew_msg(Wrmsg, msgSize, reclogo, ewInstanceName, ewUserName, ewHostName);
	} else if(reclogo->type == TYPE_HEARTBEAT) {
		/* reset flag_geojson */
		*flag_geojson =  0;
		json_string = get_json_string_from_heartbeat_ew_msg(Wrmsg, msgSize, reclogo, ewInstanceName, ewUserName, ewHostName);
	} else if(reclogo->type == TYPE_STRONGMOTIONII) {
		/* reset flag_geojson */
		*flag_geojson =  0;
		json_string = get_json_string_from_strongmotionII_ew_msg(Wrmsg, msgSize, reclogo, ewInstanceName, ewUserName, ewHostName);

		// TODO } else if(reclogo->type == TYPE_PICKTWC) {
		// TODO } else if(reclogo->type == TYPE_HYPOTWC) {
		// TODO } else if(reclogo->type == TYPE_ALARM) {

	} else {
		/* Unhandled type messages */
		json_string = NULL;
		/* reset flag_geojson */
		*flag_geojson =  0;
		ew2openapi_logit("et", "Unhandled message type %s!\n", GetTypeName(reclogo->type));
	}

	return json_string;
}
