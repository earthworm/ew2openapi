#include <stdio.h>
#include <string.h>

#include <earthworm.h>
#include <chron3.h>
#include <rw_strongmotionII.h>

#include "ew2openapi_logit.h"
#include "ew2openapi_json_strongmotionII.h"
#include "ew2openapi_utils.h"
#include "ew2openapi_json_utils.h"
#include "ew2openapi_geojson_utils.h"


#define MAX_JSON_STRING_STRONGMOTIONII 4069
#define MAX_RSA_STRING 512
char *get_json_string_from_strongmotionII_ew_struct(SM_INFO *strongmotionII_msg, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName) {
	/*Creating a json object*/
	json_object *jobj = json_object_new_object();
	json_object *jobj_rsa = NULL;
	json_object *jobj_rsa_array = json_object_new_array();
	json_object *jobj_ew_message = json_object_new_object();

	static char json_string[MAX_JSON_STRING_STRONGMOTIONII];
	char RSA_string[MAX_RSA_STRING];
	char tmp[64];
	int i;
	long qid_num = -1;
	char datetime_t[EW2OPENAPI_DATETIME_MAXLEN_STR];
	char datetime_talt[EW2OPENAPI_DATETIME_MAXLEN_STR];
	char datetime_tpga[EW2OPENAPI_DATETIME_MAXLEN_STR];
	char datetime_tpgv[EW2OPENAPI_DATETIME_MAXLEN_STR];
	char datetime_tpgd[EW2OPENAPI_DATETIME_MAXLEN_STR];
	/* char datetime_trsa[EW2OPENAPI_DATETIME_MAXLEN_STR]; */

	json_string[MAX_JSON_STRING_STRONGMOTIONII - 1] = 0;
	RSA_string[MAX_RSA_STRING - 1] = 0;
	tmp[64 - 1] = 0;

	/* Print the response spectrum */
	snprintf( RSA_string, MAX_RSA_STRING - 1, "RSA: %d", strongmotionII_msg->nrsa );
	for( i=0; i<strongmotionII_msg->nrsa; i++ )
	{
		snprintf( tmp, 64 - 1, "/%.2lf %.6lf", strongmotionII_msg->pdrsa[i], strongmotionII_msg->rsa[i] );
		if (strlen(tmp) + 1 > MAX_RSA_STRING - strlen(RSA_string)) {
			ew2openapi_logit("et", "RSA_string from strongmotionII messge would be truncated.\n");
		} else {
			strncat(RSA_string, tmp, MAX_RSA_STRING - strlen(RSA_string) - 1);
		}
	}

	epoch_to_datetime_str_iso8601_utc(datetime_t, EW2OPENAPI_DATETIME_MAXLEN_STR, strongmotionII_msg->t);
	epoch_to_datetime_str_iso8601_utc(datetime_talt, EW2OPENAPI_DATETIME_MAXLEN_STR, strongmotionII_msg->talt);

	epoch_to_datetime_str_iso8601_utc(datetime_tpga, EW2OPENAPI_DATETIME_MAXLEN_STR, strongmotionII_msg->tpga);
	epoch_to_datetime_str_iso8601_utc(datetime_tpgv, EW2OPENAPI_DATETIME_MAXLEN_STR, strongmotionII_msg->tpgv);
	epoch_to_datetime_str_iso8601_utc(datetime_tpgd, EW2OPENAPI_DATETIME_MAXLEN_STR, strongmotionII_msg->tpgd);

	snprintf(json_string, MAX_JSON_STRING_STRONGMOTIONII - 1, "CALL sp_ins_ew_strongmotionII('%s', '%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', %d, %lf, '%s', %lf, '%s', %lf, '%s', '%s');",
			ewInstanceName,
			GetModIdName(reclogo->mod),
			strongmotionII_msg->qid,
			strongmotionII_msg->sta,
			strongmotionII_msg->comp,
			strongmotionII_msg->net,
			strongmotionII_msg->loc,
			strongmotionII_msg->qauthor,
			/*
			   strongmotionII_msg->t,
			   strongmotionII_msg->talt,
			   */
			datetime_t,
			datetime_talt,
			strongmotionII_msg->altcode,
			strongmotionII_msg->pga,
			/*
			   strongmotionII_msg->tpga,
			   */
			datetime_tpga,
			strongmotionII_msg->pgv,
			/*
			   strongmotionII_msg->tpgv,
			   */
			datetime_tpgv,
			strongmotionII_msg->pgd,
			/*
			   strongmotionII_msg->tpgd,
			   */
			datetime_tpgd,
			RSA_string
				);

	/* Add Earthworm logo to JSON object */
	json_object_ew_logo_objects_add(jobj, reclogo, ewInstanceName, ewUserName, ewHostName);

	/* Convert qid to unsigned long */
	qid_num = strtoumax(strongmotionII_msg->qid, NULL, 10);
	json_object_object_add(jobj_ew_message,"quakeId", json_object_new_int64(qid_num));

	json_object_object_add(jobj_ew_message,"station", json_object_new_string(strongmotionII_msg->sta));
	json_object_object_add(jobj_ew_message,"component", json_object_new_string(strongmotionII_msg->comp));
	json_object_object_add(jobj_ew_message,"network", json_object_new_string(strongmotionII_msg->net));
	json_object_object_add(jobj_ew_message,"location", json_object_new_string((strongmotionII_msg->loc[0]==0)? "--" : strongmotionII_msg->loc));
	json_object_object_add(jobj_ew_message,"qAuthor", json_object_new_string(strongmotionII_msg->qauthor));

	json_object_object_add(jobj_ew_message,"time", json_object_new_string(datetime_t));
	json_object_object_add(jobj_ew_message,"alternateTime", json_object_new_string(datetime_talt));
	json_object_object_add(jobj_ew_message,"alternateCode", json_object_new_int64(strongmotionII_msg->altcode));

	/* For pga, the precision 6 has taken from rw_strongmotionII.c when rsa value is written by snprintf. */
	json_object_object_add(jobj_ew_message,"pga", json_object_new_double_with_precision(strongmotionII_msg->pga, 6));
	json_object_object_add(jobj_ew_message,"pgaTime", json_object_new_string(datetime_tpga));

	/* For pgv, the precision 6 has taken from rw_strongmotionII.c when rsa value is written by snprintf. */
	json_object_object_add(jobj_ew_message,"pgv", json_object_new_double_with_precision(strongmotionII_msg->pgv, 6));
	json_object_object_add(jobj_ew_message,"pgvTime", json_object_new_string(datetime_tpgv));

	/* For pgd, the precision 6 has taken from rw_strongmotionII.c when rsa value is written by snprintf. */
	json_object_object_add(jobj_ew_message,"pgd", json_object_new_double_with_precision(strongmotionII_msg->pgd, 6));
	json_object_object_add(jobj_ew_message,"pgdTime", json_object_new_string(datetime_tpgd));

	for( i=0; i<strongmotionII_msg->nrsa; i++ )
	{
		/* Create RSA json object */
		jobj_rsa = json_object_new_object();

		/* Add fields period and value in json object RSA */
		/* TODO trsa and pdrsa should refer to the same time but time seems to be always equal to 0. */
		/* For period, the precision 2 has taken from rw_strongmotionII.c when rsa value is written by snprintf. */
		json_object_object_add(jobj_rsa,"period", json_object_new_double_with_precision(strongmotionII_msg->pdrsa[i], 2));
		/* For value, the precision 6 has taken from rw_strongmotionII.c when rsa value is written by snprintf. */
		json_object_object_add(jobj_rsa,"value", json_object_new_double_with_precision(strongmotionII_msg->rsa[i], 6));

		/*
		 * json_object_object_add(jobj_rsa,"time", json_object_new_double(strongmotionII_msg->trsa[i]));
		 */
		/*
		 * epoch_to_datetime_str_iso8601_utc(datetime_trsa, EW2OPENAPI_DATETIME_MAXLEN_STR, strongmotionII_msg->trsa[i]);
		 * json_object_object_add(jobj_rsa,"time", json_object_new_string(datetime_trsa));
		 */

		/* Add json object phase into json object phase array */
		json_object_array_add(jobj_rsa_array, jobj_rsa);
	}

	/* Add rsa to the Earthworm message JSON object */
	json_object_object_add(jobj_ew_message,"RSA", jobj_rsa_array);

	/* Add Earthworm message to JSON object */
	json_object_ew_message_objects_add(jobj, jobj_ew_message);

	/* Build JSON string from json object */
	snprintf(json_string, MAX_JSON_STRING_STRONGMOTIONII - 1, "%s", json_object_to_json_string_ew2openapi(jobj));

	if (strlen(json_string) + 1 > MAX_JSON_STRING_STRONGMOTIONII - 1) {
		ew2openapi_logit("et", "json_string for strongmotionII messge will be truncated.\n");
	}

	/* clean the json object */
	json_object_put(jobj);

	return json_string;
}


char *get_json_string_from_strongmotionII_ew_msg(char *msg, int msg_size, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName) {
	char *ret = NULL;
	int error = 0;
	char Text[1024];
	SM_INFO strongmotionII_msg;
	char *msgP;

	/* Fill the SM_DATA struct from the received message
	 *     ***************************************************/
	msgP = msg;
	while ((error = rd_strongmotionII( &msgP, &strongmotionII_msg, 1 )) != 0 ) {

		// if( rd_strongmotionII( msg, msg_size, &strongmotionII_msg ) != EW_SUCCESS )
		if(error < 1 )
		{
			sprintf( Text, "Error reading strongmotionII: %s", msg );
			ew2openapi_logit("et", "%s", Text);
		} else {
			ret = get_json_string_from_strongmotionII_ew_struct(&strongmotionII_msg, reclogo, ewInstanceName, ewUserName, ewHostName);
		}

	}

	return ret;
}

