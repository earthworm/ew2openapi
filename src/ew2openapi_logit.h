#ifndef EW2OPENAPI_LOGIT_H
#define EW2OPENAPI_LOGIT_H 1

enum EW2OPENAPI_RUN_MODE { RUN_MODE_AS_EW_MODULE = 1, RUN_MODE_AS_STAND_ALONE };

extern enum EW2OPENAPI_RUN_MODE ew2openapi_run_mode;

void ew2openapi_set_run_mode( enum EW2OPENAPI_RUN_MODE flag_run_mode );
void ew2openapi_logit( const char *flag, const char *format, ... );
void ew2openapi_logit_init( const char *prog, short mid, int bufSize, int logflag );

#endif

