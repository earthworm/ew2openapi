#include <stdio.h>
#include <json-c/json.h>
#include <string.h>
#include <earthworm.h>
#include <rw_mag.h>

#include "ew2openapi_logit.h"
#include "ew2openapi_json_magnitude.h"
#include "ew2openapi_utils.h"
#include "ew2openapi_json_utils.h"
#include "ew2openapi_geojson_utils.h"
#include "ew2openapi_eq_version_name.h"

void ingv_mag_quality_magnitude_ew_struct(MAG_INFO *magSum, MAG_CHAN_INFO *magChan, char *ingv_mag_quality1, char *ingv_mag_quality2) {
	/* Initialization */
	*ingv_mag_quality1 = ' ';
	*ingv_mag_quality2 = ' ';

	if(magSum->quality <= 0.25) {
		*ingv_mag_quality1 = 'D';
	} else if(magSum->quality <= 0.5) {
		*ingv_mag_quality1 = 'C';
	} else if(magSum->quality <= 0.75) {
		*ingv_mag_quality1 = 'B';
	} else {
		*ingv_mag_quality1 = 'A';
	}

}

#define MAX_JSON_STRING_MAGNITUDE 65536*4
#define MAX_JSON_STRING_TMP 4096
char *get_json_string_from_magnitude_ew_struct(MAG_INFO *magSum, MAG_CHAN_INFO *magChan, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName) {
	/*Creating a json object*/
	json_object *jobj = json_object_new_object();
	json_object *jobj_phase = NULL;
	json_object *jobj_phase_array = json_object_new_array();
	json_object *jobj_ew_message = json_object_new_object();

	long qid_long = 0;
	char *ptr = NULL;

	static char json_string[MAX_JSON_STRING_MAGNITUDE];
	char json_string_tmp[MAX_JSON_STRING_TMP];
	char myjson_string_datetime_Time1[EW2OPENAPI_DATETIME_MAXLEN_STR];
	char myjson_string_datetime_Time2[EW2OPENAPI_DATETIME_MAXLEN_STR];
	char ingv_mag_quality1;
	char ingv_mag_quality2;
	char ingv_mag_quality_str[4];

	/* char ampType[] = "WoodAnderson"; */

	int i;

	json_string[MAX_JSON_STRING_MAGNITUDE - 1] = 0;
	json_string_tmp[MAX_JSON_STRING_TMP - 1] = 0;

	ingv_mag_quality_magnitude_ew_struct(magSum, magChan, &ingv_mag_quality1, &ingv_mag_quality2);
	snprintf(ingv_mag_quality_str, 4, "%c%c", ingv_mag_quality1, ingv_mag_quality2);

	qid_long = strtol(magSum->qid, &ptr, 10);

	snprintf(json_string, MAX_JSON_STRING_MAGNITUDE - 1, "CALL sp_ins_ew_magnitude_summary('%s', '%s', %s, %u, %lf, %lf, %lf, %lf, %d, %d, %d, '%s', %u, %d, '%s', '%s', '%c%c');",
			ewInstanceName,
			GetModIdName(reclogo->mod),
			magSum->qid,
			magSum->origin_version,
			magSum->mag,
			magSum->error,
			magSum->quality,
			magSum->mindist,
			magSum->azimuth,
			magSum->nstations,
			magSum->nchannels,
			magSum->qauthor,
			magSum->qdds_version,
			magSum->imagtype,
			magSum->szmagtype,
			magSum->algorithm,
			ingv_mag_quality1,
			ingv_mag_quality2
			);

	/* Add Earthworm logo to JSON object */
	json_object_ew_logo_objects_add(jobj, reclogo, ewInstanceName, ewUserName, ewHostName);

	json_object_object_add(jobj_ew_message,"quakeId", json_object_new_int64(qid_long));
	// json_object_object_add(jobj_ew_message,"version", json_object_new_int64(magSum->origin_version));
	json_object_object_add(jobj_ew_message,"version", json_object_new_string(ew2openapi_get_name_version(magSum->origin_version)));
	// https://gitlab.rm.ingv.it/earthworm/ew2openapi/-/issues/53
	// Set originId using quakeId and version
	json_object_object_add(jobj_ew_message,"originId", json_object_new_int64((qid_long * 10) + magSum->origin_version));
	json_object_object_add(jobj_ew_message,"mag", json_object_new_double_with_precision(magSum->mag, 2));
	json_object_object_add(jobj_ew_message,"error", json_object_new_double_with_precision(magSum->error, 2));
	json_object_object_add(jobj_ew_message,"quality", json_object_new_double_with_precision(magSum->quality, 2));
	json_object_object_add(jobj_ew_message,"minDist", json_object_new_double_with_precision(magSum->mindist, 2));
	json_object_object_add(jobj_ew_message,"azimuth", json_object_new_int64(magSum->azimuth));
	json_object_object_add(jobj_ew_message,"nStations", json_object_new_int64(magSum->nstations));
	json_object_object_add(jobj_ew_message,"nChannels", json_object_new_int64(magSum->nchannels));
	json_object_object_add(jobj_ew_message,"qAuthor", json_object_new_string(magSum->qauthor));
	json_object_object_add(jobj_ew_message,"qddsVersion", json_object_new_int64(magSum->qdds_version));
	json_object_object_add(jobj_ew_message,"iMagType", json_object_new_int64(magSum->imagtype));
	json_object_object_add(jobj_ew_message,"magType", json_object_new_string(magSum->szmagtype));
	// json_object_object_add(jobj_ew_message,"ampType", json_object_new_string(ampType));
	json_object_object_add(jobj_ew_message,"algorithm", json_object_new_string(magSum->algorithm));
	json_object_object_add(jobj_ew_message,"ingvQuality", json_object_new_string(ingv_mag_quality_str));

	for(i=0; i < magSum->nchannels; i++) {

		jobj_phase = json_object_new_object();

		epoch_to_datetime_str_iso8601_utc(myjson_string_datetime_Time1, EW2OPENAPI_DATETIME_MAXLEN_STR, magChan[i].Time1);
		epoch_to_datetime_str_iso8601_utc(myjson_string_datetime_Time2, EW2OPENAPI_DATETIME_MAXLEN_STR, magChan[i].Time2);

		snprintf(json_string_tmp, MAX_JSON_STRING_TMP - 1, "CALL sp_ins_ew_magnitude_phase('%s', '%s', %s, %u, '%s', '%s', '%s', '%s', %lf, %lf, %lf, '%s', %f, %f, '%s', %f, %f);",
				ewInstanceName,
				GetModIdName(reclogo->mod),
				magSum->qid,
				magSum->origin_version,
				magChan[i].sta,
				magChan[i].comp,
				magChan[i].net,
				magChan[i].loc,
				magChan[i].mag,
				magChan[i].dist,
				magChan[i].corr,
				/*
				   magChan[i].Time1,
				   */
				myjson_string_datetime_Time1,
				magChan[i].Amp1,
				magChan[i].Period1,
				/*
				   magChan[i].Time2,
				   */
				myjson_string_datetime_Time2,
				magChan[i].Amp2,
				magChan[i].Period2
					);

		/* Add fields in json object phase */
		json_object_object_add(jobj_phase,"sta", json_object_new_string(magChan[i].sta));
		json_object_object_add(jobj_phase,"comp", json_object_new_string(magChan[i].comp));
		json_object_object_add(jobj_phase,"net", json_object_new_string(magChan[i].net));
		json_object_object_add(jobj_phase,"loc", json_object_new_string(magChan[i].loc));
		json_object_object_add(jobj_phase,"mag", json_object_new_double_with_precision(magChan[i].mag, 2));
		json_object_object_add(jobj_phase,"dist", json_object_new_double_with_precision(magChan[i].dist, 2));
		json_object_object_add(jobj_phase,"corr", json_object_new_double_with_precision(magChan[i].corr, 2));
		json_object_object_add(jobj_phase,"time1", json_object_new_string(myjson_string_datetime_Time1));
		json_object_object_add(jobj_phase,"amp1", json_object_new_double_with_precision(magChan[i].Amp1, 4));
		if(magChan[i].Period1 == MAG_NULL) {
			json_object_object_add(jobj_phase,"period1", NULL);
		} else {
			json_object_object_add(jobj_phase,"period1", json_object_new_double_with_precision(magChan[i].Period1, 4));
		}
		json_object_object_add(jobj_phase,"time2", json_object_new_string(myjson_string_datetime_Time2));
		json_object_object_add(jobj_phase,"amp2", json_object_new_double_with_precision(magChan[i].Amp2, 4));
		if(magChan[i].Period2 == MAG_NULL) {
			json_object_object_add(jobj_phase,"period2", NULL);
		} else {
			json_object_object_add(jobj_phase,"period2", json_object_new_double_with_precision(magChan[i].Period2, 4));
		}

		/* Add json object phase into json object phase array */
		json_object_array_add(jobj_phase_array, jobj_phase);

		/*
		   if (strlen(json_string_tmp) + 1 > MAX_JSON_STRING_MAGNITUDE - strlen(json_string)) {
		   ew2openapi_logit("e", "json_string for magnitude message would be truncated.\n");
		   } else {
		   strncat(json_string, json_string_tmp, MAX_JSON_STRING_MAGNITUDE - strlen(json_string) - 1);
		   }
		   */

	}

	/* Add phase to the Earthworm message JSON object */
	json_object_object_add(jobj_ew_message,"phases", jobj_phase_array);

	/* Add Earthworm message to JSON object */
	json_object_ew_message_objects_add(jobj, jobj_ew_message);

	/* Build JSON string from json object */
	snprintf(json_string, MAX_JSON_STRING_MAGNITUDE - 1, "%s", json_object_to_json_string_ew2openapi(jobj));

	if (strlen(json_string) + 1 > MAX_JSON_STRING_MAGNITUDE - 1) {
		ew2openapi_logit("et", "json_string for magnitude messge will be truncated.\n");
	}

	/* clean the json object */
	json_object_put(jobj);

	return json_string;
}


char *get_json_string_from_magnitude_ew_msg(char *msg, int msg_size, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName) {
	char *ret = NULL;
	char Text[1024];
	MAG_INFO magSum;
	MAG_CHAN_INFO *magChan = NULL;
	int magChanSize;

	/* Before calling rd_mag() set this value */
	magSum.size_aux = 0;
	magSum.pMagAux = NULL;

	/* Fill the MAG_INFO struct from the received message */
	if (rd_mag (msg, msg_size, &magSum) < 0)
	{
		sprintf( Text, "Error reading magnitude summary: %s", msg );
		ew2openapi_logit("et", "%s", Text);
	} else {
		magChanSize = 4 * MAX_PHS_PER_EQ * sizeof (MAG_CHAN_INFO);
		if ((magChan = (MAG_CHAN_INFO *) malloc (magChanSize)) == NULL)
		{
			sprintf( Text, "Error allocating memory for magnitude stations");
			ew2openapi_logit("et", "%s", Text);
		} else {
			/* Fill the MAG_CHAN_INFO struct from the received message */
			if (rd_chan_mag (msg, msg_size, magChan, magChanSize) < 0)
			{
				sprintf( Text, "Error reading magnitude station info: %s", msg );
				ew2openapi_logit("et", "%s", Text);
			} else {
				ret = get_json_string_from_magnitude_ew_struct(&magSum, magChan, reclogo, ewInstanceName, ewUserName, ewHostName);
			}
			free(magChan);
		}
	}

	return ret;
}

