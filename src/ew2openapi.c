#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <time_ew.h>
#include <errno.h>
#include <signal.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <site.h>
#include <mem_circ_queue.h> 

#include "ew2openapi_version.h"
#include "ew2openapi_logit.h"
#include "ew2openapi_params.h"
#include "ew2openapi_handler_status.h"
/*
#include "ew2openapi_json_tracebuf.h"
*/
#include "ew2openapi_json_pickcoda.h"
#include "ew2openapi_json_arc.h"
#include "ew2openapi_json_magnitude.h"
#include "ew2openapi_json_strongmotionII.h"
#include "ew2openapi_json_quakelink.h"
#include "ew2openapi_json_error.h"
#include "ew2openapi_json_heartbeat.h"
/*
#include "ew2openapi_json_eb_picktwc.h"
#include "ew2openapi_json_eb_hypotwc.h"
#include "ew2openapi_json_eb_alarm.h"
#include "ew2openapi_mysql.h"
*/

#include "ew2openapi_json_utils.h"
#include "ew2openapi_handler_quakedb_ws.h"
#include "ew2openapi_handler_kafka.h"
#include "ew2openapi_handler_rabbitmq.h"
#include "ew2openapi_handler_osc.h"
#include "ew2openapi_handler_convtool.h"

/* Thread things
 ***************/
#define THREAD_STACK 8192

DECLARE_SPECIFIC_SEMAPHORE_EW(sema_CountAvail);
DECLARE_SPECIFIC_SEMAPHORE_EW(sema_CountBusy);
DECLARE_SPECIFIC_SEMAPHORE_EW(sema_CountToProcess);

/* just to compile without warning from thread function returning */
#ifdef _WINNT
#define RETURN_THREAD_FUNCTION return
#else
#define RETURN_THREAD_FUNCTION return (thr_ret) NULL
#endif

thr_ret MessageStacker( void *p );    /* thread: messages from ring to queue */
thr_ret MessageHandler( void *p );        /* thread: messages from queue to database */
thr_ret QueueDumpFile( void *p );     /* thread: messages from queue to file */

void       inc_nStackerThread();
void       dec_nStackerThread();
mutex_t    mutex_nStackerThread;
int        nStackerRunning = 0;

mutex_t   mutex_OutQueue;            /* mutex for variable OutQueue */
QUEUE     OutQueue; 		     /* queue for saving messages read from ring */

#define   FLAG_TERM_NULL   0         /* flag value for null info */
#define   FLAG_TERM_REQ_DB 1         /* flag value for termination requested DB */
#define   FLAG_TERM_REQ_MS 2         /* flag value for termination requested Stackers */
#define   FLAG_TERM_REQ_QF 4         /* flag value for termination queue dump file */
#define   FLAG_TERM_THR_DB 8         /* flag value DB thread is terminated */
#define   FLAG_TERM_THR_MS 16        /* flag value MS thread is terminated */
#define   FLAG_TERM_THR_QF 32        /* flag value MS thread is terminated */
mutex_t   mutex_flags_term;          /* mutex for variable  flags_term */
int       flags_term = FLAG_TERM_NULL;          /* flags variable for handling termination */
void      setlck_flags_term(int flags_bitmap);  /* set flags_bitmap by lock mechanism */
int       testlck_flags_term(int flags_bitmap); /* test flags_bitmap by lock mechanism */

int remove_duplicated_ringname(char ringNames[MAX_NUM_OF_RINGS][MAX_RING_STR], int *nRings);
void ew2openapi_config( char *configfile, TYPE_PARAMS *params );
void ew2openapi_lookup( TYPE_PARAMS *params );
void ew2openapi_status( MSG_LOGO *logo, char *msg, TYPE_PARAMS *params);
// void ew2openapi_mysql_status(int ret_ex_mysql_query, TYPE_PARAMS *params);

/* Error messages used by ew2openapi 
 ***********************************/
#define  ERR_MISSMSG       0   /* message missed in transport ring        */
#define  ERR_TOOBIG        1   /* retreived msg too large for buffer      */
#define  ERR_NOTRACK       2   /* msg retreived; tracking limit exceeded  */
#define  ERR_QUEUE         3   /* queue error                             */

/* maximum length for error message */
#define STR_LEN_ERRTEXT 2048

int main( int argc, char **argv ) {
	time_t       now;	                 /* current time, used for timing heartbeats */
	pid_t        MyPid;		         /* Our own pid, sent with heartbeat for restart purposes */
	time_t       MyLastBeat;             /* time of last local (into Earthworm) hearbeat */
	ew_thread_t  tidStacker[MAX_NUM_OF_RINGS];   /* Thread moving messages from transport to queue */
	ew_thread_t  tidMessageHandler;          /* Thread read message from queue and populate DB */
	ew_thread_t  tidQueueDumpFile;       /* Thread read message from queue and populate DB */
	int          ret_dq;                 /* stores errors from dumpqueue() and undumpqueue() */
	int          cur_NumOfElements = 0;  /* Current value of number of elements in OutQueue */
	const int    wait_time_thr_tot   = 5000;  /* total of waiting time for joining all threads */
	const int    wait_time_thr_slice = wait_time_thr_tot / 10;   /* slice of waiting time for joining all threads */
	int          wait_time_thr_count = 0;     /* counter of waiting time for joining all threads */
	char         errText[STR_LEN_ERRTEXT];  /* string for log/error/heartbeat messages           */
	char         *configfile;       /* pointer to configuration file name argv[1] */ 
	char         default_configfilename_standalone[] = "ew2openapi_not_defined_for_standalone.d";
	int          (*p_data_handler_func_init) (TYPE_PARAMS *) = NULL;
	int          (*p_data_handler_func_handle) (TYPE_DATA_HANDLER *) = NULL;
	int          (*p_data_handler_func_destroy) (TYPE_PARAMS *) = NULL;
	unsigned char TYPE_MSG;

	int          r = 0;
	TYPE_PARAMS  params;

	/* set last character to zero */
	errText[STR_LEN_ERRTEXT-1]=0;

	/* Check command line arguments 
	 ******************************/
	params.cmdname = argv[0];
	if ( argc != 2 && argc != 4)
	{
		fprintf( stderr, "%s\n", EW2OPENAPI_NAME_AND_VERSION);
		fprintf( stderr, "\n" );
		fprintf( stderr, "Earthworm module for transporting Earthworm messages from rings to:\n");
		fprintf( stderr, "       - ew2openapi Webservice\n");
		fprintf( stderr, "       - RabbitMQ exchanges\n");
		fprintf( stderr, "       - JSON files.\n");
		fprintf( stderr, "\n" );
		fprintf( stderr, "ew2openapi can be used in two different ways:\n");
		fprintf( stderr, "       - launched as an Earthworm module to redirect data into webservices or RabbitMQ\n");
		fprintf( stderr, "       - stand-alone to convert Earthworm message to JSON ew2openapi format\n");
		fprintf( stderr, "\n" );
		fprintf( stderr, "Earthworm messages can optionally be converted in JSON format.\n" );
		fprintf( stderr, "GeoJSON format could be also used but only when geographic references\n");
		fprintf( stderr,  "are directly or inderictely available for a specific message type.\n");
		fprintf( stderr, "\n" );
		fprintf( stderr, "Usage as Earthworm module:\n" );
		fprintf( stderr, "       %s <configfile.d>\n", params.cmdname );
		fprintf( stderr, "\n" );
		fprintf( stderr, "Usage as Earthworm messages conversion tool:\n" );
		fprintf( stderr, "   output to file or stdout:\n");
		fprintf( stderr, "       %s <TYPE_MSG> <input_ew_messagefile> <output_jsonfile>\n", params.cmdname );
		fprintf( stderr, "       %s <TYPE_MSG> <input_ew_messagefile> -\n", params.cmdname );
		fprintf( stderr, "       %s <TYPE_MSG> -                      <output_jsonfile>\n", params.cmdname );
		fprintf( stderr, "       %s <TYPE_MSG> -                      -\n", params.cmdname );
		fprintf( stderr, "   output based on configfile.d:\n");
		fprintf( stderr, "       %s <configfile.d> <TYPE_MSG> <input_ew_messagefile>\n", params.cmdname );
		fprintf( stderr, "       %s <configfile.d> <TYPE_MSG> <input_ew_messagefile>\n", params.cmdname );
		fprintf( stderr, "       %s <configfile.d> <TYPE_MSG> -                     \n", params.cmdname );
		fprintf( stderr, "       %s <configfile.d> <TYPE_MSG> -                     \n", params.cmdname );
		fprintf( stderr, "\n");
		fprintf( stderr, "N.B. character '-' stands for standard input/output.\n" );
		fprintf( stderr, "     Environment variable EW_PARAMS is required.\n" );
		fprintf( stderr, "     You could define EW_PARAMS before command:\n");
		fprintf( stderr, "         (i.e. EW_PARAMS=./params ew2openapi ...).\n" );
		fprintf( stderr, "\n" );
		fprintf( stderr, "Matteo Quintiliani - Istituto Nazionale di Geofisica e Vulcanologia - Italy\n" );
		fprintf( stderr, "Mail bug reports and suggestions to <matteo.quintiliani@ingv.it>\n" );
		return( 0 );
	}

	/* Start initialization configfile */
	configfile = default_configfilename_standalone;
	/* ew2openapi_logit_init( configfile, 0, MAX_JSON_STRING_ARC, 1 ); */

	/* If launched as standalone command */
	if(argc == 4) {

		ew2openapi_set_run_mode(RUN_MODE_AS_STAND_ALONE);

		if ( GetType(argv[1], &TYPE_MSG)  != 0 ) {
			configfile = argv[1];
			ew2openapi_config( configfile, &params );

			params.TYPE_MSG_ARG = argv[2];
			params.fnamein = argv[3];
			params.fnameout = NULL;
		} else {
			params.TYPE_MSG_ARG = argv[1];
			params.fnamein = argv[2];
			params.fnameout = argv[3];
			configfile = default_configfilename_standalone;
			params.flag_handler = FLAG_HANDLER_CONVTOOL;
		}


	} else {

		ew2openapi_set_run_mode(RUN_MODE_AS_EW_MODULE);

		configfile = argv[1];
		ew2openapi_config( configfile, &params );
	}


	/* Initialize name of log-file & open it 
	 ****************************************/
	ew2openapi_logit_init( configfile, 0, MAX_JSON_STRING_ARC, 1 );

	/* Switch on different handlers. Set functions _init() and _destroy */
	switch(params.flag_handler) {
		/* Send messages to QuakeDB webservice */
		case FLAG_HANDLER_QUAKEDB_WS:
			p_data_handler_func_init    = ew2openapi_handler_quakedb_ws_init;
			p_data_handler_func_handle  = ew2openapi_handler_quakedb_ws_handle;
			p_data_handler_func_destroy = ew2openapi_handler_quakedb_ws_destroy;
			break;
			/* Send messages to Kafka */
		case FLAG_HANDLER_KAFKA:
			p_data_handler_func_init    = ew2openapi_handler_kafka_init;
			p_data_handler_func_handle  = ew2openapi_handler_kafka_handle;
			p_data_handler_func_destroy = ew2openapi_handler_kafka_destroy;
			break;
			/* Send messages to RabbitMQ */
		case FLAG_HANDLER_RABBITMQ:
			p_data_handler_func_init    = ew2openapi_handler_rabbitmq_init;
			p_data_handler_func_handle  = ew2openapi_handler_rabbitmq_handle;
			p_data_handler_func_destroy = ew2openapi_handler_rabbitmq_destroy;
			break;
			/* Send messages to OSC Server */
		case FLAG_HANDLER_OSC:
			p_data_handler_func_init    = ew2openapi_handler_osc_init;
			p_data_handler_func_handle  = ew2openapi_handler_osc_handle;
			p_data_handler_func_destroy = ew2openapi_handler_osc_destroy;
			break;
			/* Write message to file/stdou */
		case FLAG_HANDLER_CONVTOOL:
			p_data_handler_func_init    = ew2openapi_handler_convtool_init;
			p_data_handler_func_handle  = ew2openapi_handler_convtool_handle;
			p_data_handler_func_destroy = ew2openapi_handler_convtool_destroy;
			break;
		default:
			/* ERROR */
			ew2openapi_logit( "e", "%s(%s): error params.flag_handler value %d not valid; exiting!\n",
					params.cmdname, params.MyModName, params.flag_handler);
			exit( -1 );
			break;
	}

	/* Call function _init() before starting threads */
	p_data_handler_func_init(&params);

	/* Stand-alone or EW loop */

	/* Run as Earthworm messages conversion tool and then exit
	 ****************************************/
	if(argc == 4) {
		exit( earthworm_message_conversion_tool(&params, p_data_handler_func_handle) );
	}

	/* Create Mutexes
	 ********************************************/
	CreateSpecificMutex(&mutex_nStackerThread);
	CreateSpecificMutex(&mutex_OutQueue);
	CreateSpecificMutex(&mutex_flags_term);

	/* Read the configuration file(s)
	 ********************************/
	/* ew2openapi_config( configfile, &params ); */
	ew2openapi_logit("et", "ew2openapi: version %s\n", EW2OPENAPI_VERSION);
	ew2openapi_logit("et" , "%s(%s): Read command file <%s>\n", 
			params.cmdname, params.MyModName, configfile );

	/* Remove duplicated ring names (Call function after ew2openapi_logit_init()) */
	remove_duplicated_ringname(params.InRings, &(params.nRings));

	/* Init params_stacker array */
	for(r=0; r < params.nRings; r++) {
		strncpy(params.pstacker[r].InRing, params.InRings[r], MAX_RING_STR-1);
		params.pstacker[r].GetLogo = params.GetLogo;
		params.pstacker[r].nLogo = params.nLogo;
		params.pstacker[r].params = &params;
	}

	/* Look up important info from earthworm.h tables
	 *************************************************/
	ew2openapi_lookup(&params);

	/* Initialize the message queue
	 *******************************/
	RequestSpecificMutex(&mutex_OutQueue);
	initqueue( &OutQueue, (unsigned long)params.QueueSize,(unsigned long)(params.MaxMsgSize+1));
	ReleaseSpecificMutex(&mutex_OutQueue);

	/* Reinitialize the logging level
	 *********************************/
	ew2openapi_logit_init( configfile, 0, 4096, params.LogSwitch );

	/* Test MySQL connection before continuing
	 *****************************************/
	//     if( ew2openapi_mysql_connect_p(&test_mysql_connection,
	// 		    params.db_hostname, params.db_username, params.db_password, params.db_name, params.db_port) != NULL) {
	// 	/* close MySQL connection */
	// 	ew2openapi_mysql_close_p(&test_mysql_connection);
	//     } else {
	// 	/* notify error status. exit */
	// 	ew2openapi_logit("e", "%s(%s): Error initializing connection to the database; exiting!\n", params.cmdname, params.MyModName);
	// 	exit( -1 );
	//     }

	/* Get our own Pid for restart purposes
	 ***************************************/
	MyPid = getpid();
	if(MyPid == -1)
	{
		ew2openapi_logit("e", "%s(%s): Cannot get pid; exiting!\n", params.cmdname, params.MyModName);
		return(0);
	}

	/* Attach to shared memory for tport_getflag() and putting
	 * heartbeat or status messages
	 ********************************************/
	tport_attach( &params.HeartBeatRegion, params.HeartBeatRingKey );

	/* One heartbeat to announce ourselves to statmgr
	 ************************************************/
	time(&MyLastBeat);
	snprintf( errText, STR_LEN_ERRTEXT-1, "%ld %ld\n%c", (long) MyLastBeat, (long) MyPid, 0);
	ew2openapi_status( &params.HeartLogo, errText, &params );

	/* Undump queue messages from file
	 *******************************/
	RequestSpecificMutex(&mutex_OutQueue);
	ret_dq=undumpqueue( &OutQueue, params.QueueFilename);
	cur_NumOfElements = OutQueue.NumOfElements;
	ReleaseSpecificMutex(&mutex_OutQueue);

	/* Create Semaphores
	 ********************************************/
	CREATE_SPECIFIC_SEMAPHORE_EW(sema_CountAvail,     params.QueueSize - cur_NumOfElements);
	CREATE_SPECIFIC_SEMAPHORE_EW(sema_CountBusy,      cur_NumOfElements );
	CREATE_SPECIFIC_SEMAPHORE_EW(sema_CountToProcess, cur_NumOfElements);

	switch (ret_dq) {
		case 1:
			ew2openapi_logit("et", "dump file '%s' does not exist.\n", params.QueueFilename);
			break;
		case 0:
			ew2openapi_logit("et", "read %d messages from queue dump file '%s'.\n", cur_NumOfElements, params.QueueFilename);
			break;
		case -1:
			ew2openapi_logit("et", "errors opening or reading dump file '%s'.\n", params.QueueFilename);
			break;
		case -2:
			ew2openapi_logit("et", "there are more entries in the dump file '%s' than will fit in queue. Read %d messages.\n", params.QueueFilename, cur_NumOfElements);
			break;
		case -3:
			ew2openapi_logit("et", "dump file entry in '%s' is larger than will fit in queue.\n", params.QueueFilename);
			break;
		case -4:
			ew2openapi_logit("et", "timestamps at each end of file '%s' don't match.\n", params.QueueFilename);
			break;
		default:
			ew2openapi_logit("et", "Unknown error %d from undumpqueue() reading file '%s'.\n", ret_dq, params.QueueFilename);
			break;
	}

	/* Start the message handler thread
	 ***********************************/
	if ( StartThreadWithArg(  MessageHandler, (void *) &params, (unsigned)THREAD_STACK, &tidMessageHandler ) == -1 )
	{
		ew2openapi_logit( "e", "%s(%s): Error starting thread MessageHandler(); exiting!\n",
				params.cmdname, params.MyModName );
		tport_detach( &params.HeartBeatRegion );
		exit( -1 );
	}

	/* Start the message stacking threads
	 ****************************************************************/
	for(r=0; r < params.nRings; r++) {
		inc_nStackerThread();
	}
	for(r=0; r < params.nRings; r++) {
		if ( StartThreadWithArg(  MessageStacker, (void *) &(params.pstacker[r]),  (unsigned)THREAD_STACK, &(tidStacker[r]) ) == -1 )
		{
			ew2openapi_logit( "e", 
					"%s(%s): Error starting thread MessageStacker() on ring %s; exiting!\n",
					params.cmdname, params.MyModName, params.pstacker[r].InRing );
			tport_detach( &params.HeartBeatRegion );
			exit( -1 );
		}
	}

	ew2openapi_logit( "et", "%s(%s): joined to thread main()!\n", params.cmdname, params.MyModName );

	/* Start main ew2openapi service loop, which aimlessly beats its heart.
	 **********************************/
	while( tport_getflag( &params.HeartBeatRegion ) != TERMINATE  &&
			tport_getflag( &params.HeartBeatRegion ) != MyPid         )
	{
		/* Beat the heart into the transport ring
		 ****************************************/
		time(&now);
		if (difftime(now,MyLastBeat) > (double)params.HeartBeatInt ) 
		{
			snprintf( errText, STR_LEN_ERRTEXT-1, "%ld %ld\n%c", (long) now, (long) MyPid,0);
			ew2openapi_status( &params.HeartLogo, errText, &params );
			MyLastBeat = now;
		}

		/* take a brief nap
		 ************************************/
		sleep_ew(100);
	} /*end while of monitoring loop */

	ew2openapi_logit("et", "%s(%s): termination requested; waiting for all threads (max. %d sec.)!\n", 
			params.cmdname, params.MyModName, 3 * (wait_time_thr_tot/1000) );

	/* Ask to terminate to all Stacker threads */
	setlck_flags_term(FLAG_TERM_REQ_MS);
	wait_time_thr_count=0;
	while( wait_time_thr_count < wait_time_thr_tot && !testlck_flags_term(FLAG_TERM_THR_MS) ) {
		sleep_ew(wait_time_thr_slice);
		wait_time_thr_count+=wait_time_thr_slice;
	}

	/* The queue could be still full.
	 * (i.e. MessageHandler() could not be able to  process messages in the queue. ) */
	if(!testlck_flags_term(FLAG_TERM_THR_MS)) {
		ew2openapi_logit("et", "Warning: threads MessageStacker() not all terminated yet!\n");
	}

	/* Ask to terminate to MessageHandler thread */
	setlck_flags_term(FLAG_TERM_REQ_DB);
	wait_time_thr_count=0;
	while( wait_time_thr_count < wait_time_thr_tot && !testlck_flags_term(FLAG_TERM_THR_DB) ) {
		sleep_ew(wait_time_thr_slice);
		wait_time_thr_count+=wait_time_thr_slice;
	}

	/* The queue could be still empty. */
	if(!testlck_flags_term(FLAG_TERM_THR_DB)) {
		ew2openapi_logit("et", "Warning: thread MessageHandler() not terminated yet!\n");
		/* One for MessageHandler which must quit since FLAG_TERM_REQ_DB */
		POST_SPECIFIC_SEMAPHORE_EW(sema_CountToProcess);
	}

	/* Start the Queue Dump File thread
	 ***********************************/
	if ( StartThreadWithArg(  QueueDumpFile, (void *) &params, (unsigned)THREAD_STACK, &tidQueueDumpFile ) == -1 )
	{
		ew2openapi_logit( "e", "%s(%s): Error starting thread QueueDumpFile(); exiting!\n",
				params.cmdname, params.MyModName );
		tport_detach( &params.HeartBeatRegion );
		exit( -1 );
	}

	wait_time_thr_count=0;
	while( wait_time_thr_count < wait_time_thr_tot
			&& (   !testlck_flags_term(FLAG_TERM_THR_MS)
				|| !testlck_flags_term(FLAG_TERM_THR_DB)
				|| !testlck_flags_term(FLAG_TERM_THR_QF)
			   )
		 ) {
		if(!testlck_flags_term(FLAG_TERM_THR_QF)) {
			/* One for QueueDumpFile which must quit since FLAG_TERM_REQ_DB */
			POST_SPECIFIC_SEMAPHORE_EW(sema_CountToProcess);
		}
		sleep_ew(wait_time_thr_slice);
		wait_time_thr_count+=wait_time_thr_slice;
	}

	/* Still problem ??? */
	if(!testlck_flags_term(FLAG_TERM_THR_MS)) {
		ew2openapi_logit("et", "Warning: threads MessageStacker() not all terminated yet!\n");
	}
	if(!testlck_flags_term(FLAG_TERM_THR_DB)) {
		ew2openapi_logit("et", "Warning: thread MessageHandler() not terminated yet!\n");
	}
	if(!testlck_flags_term(FLAG_TERM_THR_QF)) {
		ew2openapi_logit("et", "Warning: thread QueueDumpFile() not terminated yet!\n");
	}

	/* Detach from Input/Output shared memory ring 
	 ********************************************/
	tport_detach( &params.HeartBeatRegion );

	/* Destroy Semaphores
	 ********************************************/
	DESTROY_SPECIFIC_SEMAPHORE_EW(sema_CountAvail);
	DESTROY_SPECIFIC_SEMAPHORE_EW(sema_CountBusy);
	DESTROY_SPECIFIC_SEMAPHORE_EW(sema_CountToProcess);

	/* Shut it down
	 ***************/
	ew2openapi_logit("et", "%s(%s): termination requested; exiting!\n", 
			params.cmdname, params.MyModName );

	/* Deallocate memory from OutQueue */
	RequestSpecificMutex(&mutex_OutQueue);
	freequeue(&OutQueue);
	ReleaseSpecificMutex(&mutex_OutQueue);

	/* Destroy Mutexes
	 ********************************************/
	CloseSpecificMutex(&mutex_nStackerThread);
	CloseSpecificMutex(&mutex_OutQueue);
	CloseSpecificMutex(&mutex_flags_term);

	/* Call function _destroy() after closing threads */
	p_data_handler_func_destroy(&params);

	ew2openapi_logit("et", "%s(%s): exit!\n", 
			params.cmdname, params.MyModName );

	exit( 0 );	
}
/* *******************  end of main *******************************
 ******************************************************************/

#define MAX_SUB_STRPOS 4096
char *sub_strpos(char *iter_s[], char sep) {
	static char ret[MAX_SUB_STRPOS];
	char *p;
	int l;
	int i;

	ret[0] = 0;

	if(iter_s) {
		p = *iter_s;
		l = strlen(p);
		i = 0;

		if(p) {

			/* Build ret */
			while(i < l  &&  *p != sep  &&  *p != 0  &&  i < MAX_SUB_STRPOS-1) {
				ret[i] = *p;
				i++;
				p++;
			}
			if(*p == sep) {
				ret[i] = sep;
				i++;
				p++;
			}
			ret[i] = 0;

			/* Check */
			if(i >= MAX_SUB_STRPOS) {
				ew2openapi_logit("et", "sub_strpos() returned value has not been completed, its length execeds %d bytes\n", MAX_SUB_STRPOS);
			}

			/* Set new value for *iter_s */
			if(*p == 0  ||  i >= l) {
				*iter_s = NULL;
			} else {
				*iter_s = p;
			}

		}
	}

	return ret;
}


/* set flags_bitmap by lock mechanism 
 ***************************************************/
void       setlck_flags_term(int flags_bitmap) {
	RequestSpecificMutex(&mutex_flags_term);
	flags_term = flags_term | flags_bitmap;
	ReleaseSpecificMutex(&mutex_flags_term);
}


/* test flags_bitmap by lock mechanism
 ***************************************************/
int       testlck_flags_term(int flags_bitmap) {
	int ret = 0;
	RequestSpecificMutex(&mutex_flags_term);
	if(flags_bitmap == (flags_term & flags_bitmap)) {
		ret = 1;
	}
	ReleaseSpecificMutex(&mutex_flags_term);
	return ret;
}

/************* Main Thread for populating DB from queue  ***************
 *          Pull a messsage from the queue, and call stored procedure  *
 *          for inserting information in to the database               *
 **********************************************************************/
thr_ret MessageHandler( void *p )
{
	TYPE_PARAMS *params = (TYPE_PARAMS *) p;
	MSG_LOGO reclogo;
	int      ret;
	int      i;

	unsigned char inseq = 0;

	char    *Wrmsg = NULL;           /* message to get from queue         */
	long     msgSize;
	long     inkey;
	time_t   time_now;
	time_t   time_msg;
	double   difftime_seconds;
	const double   max_difftime_seconds = 60.0;

	TYPE_DATA_HANDLER data_handler;
	int (*p_data_handler_func_handle) (TYPE_DATA_HANDLER *) = NULL;

	int flag_remove_last_msg = 0;
	int flag_wait_after_error = 0;
	int status_count_attempts = 0;  /* Default set here */
	const int max_status_count_attempts = 5;
	int handler_status_code = HANDLER_STATUS_OK;
	int num_elements_in_queue = 0;

	/* TODO get username Cross-Platform */
	char ewUserName[MAX_LENGTH_STRING] = "ew";
	char ewHostName[MAX_LENGTH_STRING] = "UNKNOWN";
	if(getsysname_ew(ewHostName, MAX_LENGTH_STRING) != 0) {
		/* error */
	}

	ew2openapi_logit( "et", "ew2openapi: thread MessageHandler() started.\n");

	/* Set function pointer to the handler function */
	switch(params->flag_handler) {

		/* Send messages to QuakeDB webservice */
		case FLAG_HANDLER_QUAKEDB_WS:
			p_data_handler_func_handle = ew2openapi_handler_quakedb_ws_handle;
			break;
			/* Send messages to Kafka */
		case FLAG_HANDLER_KAFKA:
			p_data_handler_func_handle = ew2openapi_handler_kafka_handle;
			break;
			/* Send messages to RabbitMQ */
		case FLAG_HANDLER_RABBITMQ:
			p_data_handler_func_handle = ew2openapi_handler_rabbitmq_handle;
			break;
			/* Send messages to OSC */
		case FLAG_HANDLER_OSC:
			p_data_handler_func_handle = ew2openapi_handler_osc_handle;
			break;
		default:
			/* ERROR */
			ew2openapi_logit( "et", "%s(%s): error setting data handler function; exiting!\n",
					params->cmdname, params->MyModName );
			exit( -1 );
			break;
	}


	/* Allocate buffer for reading message from queue  */
	if ( ( Wrmsg = (char *) malloc(params->MaxMsgSize+1) ) ==  NULL ) 
	{
		ew2openapi_logit( "et", "%s(%s): error allocating Wrmsg; exiting!\n",
				params->cmdname, params->MyModName );
		exit( -1 );
	}

	while (!testlck_flags_term(FLAG_TERM_REQ_DB)) {   /* main loop */

		/* Get message from queue without removing it
		 *************************/
		/* N.B. use sema_CountToProcess to synchronize receiving of messages
		 * and avoid to use sleep_ew() when queue is empty */
		WAIT_SPECIFIC_SEMAPHORE_EW(sema_CountToProcess);

		if(testlck_flags_term(FLAG_TERM_REQ_DB)) {
			POST_SPECIFIC_SEMAPHORE_EW(sema_CountToProcess);
			continue;
		}

		RequestSpecificMutex(&mutex_OutQueue);
		ret=cpqueuering( &OutQueue, Wrmsg, &msgSize, &reclogo, &inkey, &inseq);
		ReleaseSpecificMutex(&mutex_OutQueue);

		/* The following condition could occur only one time
		 * when the module quits.  */
		if (ret < 0 )
		{ /* -1 means empty queue */
			ew2openapi_logit("et", "MessageHandler: queue is empty!\n");
			sleep_ew(100);
			continue;
		}

		/* Set for safety but only after checking ret value */
		memcpy(&time_msg, &Wrmsg[msgSize - sizeof(time_msg)], sizeof(time_msg));
		msgSize -= sizeof(time_msg);
		Wrmsg[msgSize] = '\0';

		/* Determine which GetLogo this message used */
		for(i = 0; i < params->nLogo; i++) {
			if ( (params->GetLogo[i].type   == WILD || 
						params->GetLogo[i].type   == reclogo.type) &&
					(params->GetLogo[i].mod    == WILD || 
					 params->GetLogo[i].mod    == reclogo.mod) &&
					(params->GetLogo[i].instid == WILD || 
					 params->GetLogo[i].instid == reclogo.instid) )
				break;
		}
		if (i == params->nLogo) {
			ew2openapi_logit("et", "%s error: logo <%d.%d.%d> not found\n",
					params->cmdname, reclogo.instid, reclogo.mod, reclogo.type);
			continue;
		}

		ew2openapi_logit("et", "LOGO: <%s.%s.%s.%s>\n",
				GetKeyName(inkey), GetInstName(reclogo.instid), GetModIdName(reclogo.mod), GetTypeName(reclogo.type));

		/* Init default value handler_status_code */
		handler_status_code = HANDLER_STATUS_OK;

		/* Init default values for flag_remove_last_msg and flag_wait_after_error */
		flag_remove_last_msg = 1;
		flag_wait_after_error = 0;

		/* Set values in data_handler */
		data_handler.Wrmsg = Wrmsg;
		data_handler.msgSize = msgSize;
		data_handler.inkey = inkey;
		data_handler.reclogo = &reclogo;
		data_handler.params = params;
		data_handler.ewUserName = ewUserName;
		data_handler.ewHostName = ewHostName;

		/* Call handler function passing data_handler variable */
		handler_status_code = p_data_handler_func_handle(&data_handler);

		/* Set flag_remove_last_msg and flag_wait_after_error based on handler_status_code */
		switch(handler_status_code) {

			case HANDLER_STATUS_OK:
				/* Do nothing. Leave default values */
				break;

			case HANDLER_STATUS_ERROR_INFINITY_RETRIES:
				/* Do not remove item from queue, wait a while */
				flag_remove_last_msg  = 0;
				flag_wait_after_error = 1;
				break;

			case HANDLER_STATUS_ERROR_LIMITED_RETRIES_WITHIN_TIME:
				/* Compute time difference between now and inserted message time.
				 * If it exceeds max_difftime_seconds, then the message will be discarded. */

				time(&time_now); difftime_seconds = difftime(time_now, time_msg);
				if(difftime_seconds < max_difftime_seconds) {
					/* Do not remove item from queue, wait until max_difftime_seconds */
					flag_remove_last_msg  = 0;
					flag_wait_after_error = 1;
				}
				break;

			case HANDLER_STATUS_ERROR_LIMITED_RETRIES:
				/* Count how many times occurred the error to POST
				 * the same message before giving up after a maximum
				 * number of attempts */

				if(status_count_attempts < max_status_count_attempts) {
					/* Do not remove item from queue, wait a while, count attempt */
					flag_remove_last_msg  = 0;
					flag_wait_after_error = 1;
					status_count_attempts++;
				} else {
					/* Remove item from queue, reset count attempt */
					status_count_attempts = 0;
				}

				break;

			case HANDLER_STATUS_ERROR_NO_RETRIES:
				/* Do nothing. Leave default values */
				break;

			default:
				/* ERROR */
				ew2openapi_logit( "et", "%s(%s): error handler_status_code value %d not valid; exiting!\n",
						params->cmdname, params->MyModName, handler_status_code);
				exit( -1 );
				break;
		}

		/* Do actions based on flag_remove_last_msg, flag_wait_after_error and status_count_attempts */

		/* Remove last message from queue
		 *************************/
		if(flag_remove_last_msg) {
			WAIT_SPECIFIC_SEMAPHORE_EW(sema_CountBusy);
			RequestSpecificMutex(&mutex_OutQueue);
			ret=dequeuering( &OutQueue, Wrmsg, &msgSize, &reclogo, &inkey, &inseq);
			ReleaseSpecificMutex(&mutex_OutQueue);
			POST_SPECIFIC_SEMAPHORE_EW(sema_CountAvail);
		} else {
			/* The item has not been successfully processed due to
			 * WS connection error, then re-increment sema_CountToProcess */
			POST_SPECIFIC_SEMAPHORE_EW(sema_CountToProcess);
			ew2openapi_logit("et", "Retry to POST last  message later.\n");
		}

		/* Get number of elements in queue */
		RequestSpecificMutex(&mutex_OutQueue);
		num_elements_in_queue = getNumOfElementsInQueue(&OutQueue);
		ReleaseSpecificMutex(&mutex_OutQueue);

		/* Get time message delay handler in seconds */
		time(&time_now); difftime_seconds = difftime(time_now, time_msg);

		/* WARNING message about possible delay based on following conditions */
		if(difftime_seconds >= 5.0 || num_elements_in_queue >= 5) {
			ew2openapi_logit("et", "WARNING: Current number of elements in queue: %d. Last message delay %.0f seconds.\n", num_elements_in_queue, difftime_seconds);
		}

		/* At the end of the loop */
		if(flag_wait_after_error) {
			ew2openapi_logit("et", "Wait %d seconds before retrying.\n", params->WaitSecAfterServiceError);
			/* Wait a while after a Service Error */
			sleep_ew(params->WaitSecAfterServiceError * 1000);
		}

	}   /* End of main loop */


	/* At the end close possible open connection to DB server */
	// ew2openapi_mysql_close_p(&mysql);

	/* we're quitting 
	 *****************/
	ew2openapi_logit("et", "ew2openapi: thread MessageHandler() terminated !\n");
	setlck_flags_term(FLAG_TERM_THR_DB);
	KillSelfThread(); /* main thread will not restart us */
	RETURN_THREAD_FUNCTION;
}


/********************** Message Stacking Thread *******************
 *           Move messages from transport to memory queue         *
 ******************************************************************/
thr_ret MessageStacker( void *p )
{
	TYPE_PARAMS_STACKER *params_stacker = (TYPE_PARAMS_STACKER *) p;
	SHM_INFO   InRegion;
	long       inkey;             /* Key to input ring            */
	unsigned char inseq = 0;      /* transport seq# in input ring */
	long       recsize;	/* size of retrieved message             */
	MSG_LOGO   reclogo;       /* logo of retrieved message             */
	time_t     now;
	int        ret;
	int        error_occurred = 0;
	int        NumOfTimesQueueLapped= 0; /* number of messages lost due to 
											queue lap */

	/* Message Buffers to be allocated
	 *********************************/
	char       *msgb = NULL;           /* msg retrieved from transport      */ 
	char       errText[STR_LEN_ERRTEXT]; /* string for log/error/heartbeat messages */
	time_t     time_msg;

	/* set last character to zero */
	errText[STR_LEN_ERRTEXT-1]=0;

	/* Look up transport region keys earthworm.h tables
	 ************************************************/
	if( ( inkey = GetKey(params_stacker->InRing) ) == -1 )
	{
		ew2openapi_logit( "et", "ew2openapi: thread MessageStacker() Invalid input ring name <%s>; skipping these ring!\n",
				params_stacker->InRing );
		error_occurred = 1;
	} else {
		ew2openapi_logit( "et", "ew2openapi: thread MessageStacker() reading messages from input ring name <%s>.\n",
				params_stacker->InRing );
	}

	if(!error_occurred) {

		/* Allocate space for messages buffer
		 ***********************************************************/
		/* Buffer for Read thread: */
		if ( ( msgb = (char *) malloc(params_stacker->params->MaxMsgSize+1) ) ==  NULL ) 
		{
			ew2openapi_logit( "et", "%s(%s): thread MessageStacker() error allocating Rawmsg; exiting!\n",
					params_stacker->params->cmdname, params_stacker->params->MyModName );
			exit( -1 );
		}

		/* Attach to input and output transport rings
		 ******************************************/
		tport_attach( &InRegion,  inkey );

		/* Flush all old messages from the ring
		 ************************************/
		while( tport_copyfrom( &InRegion, params_stacker->GetLogo, params_stacker->nLogo, &reclogo,
					&recsize, msgb, params_stacker->params->MaxMsgSize, &inseq ) != GET_NONE );

		ew2openapi_logit( "et", "%s(%s): thread MessageStacker() starting main loop on input ring name <%s>.\n",
					params_stacker->params->cmdname, params_stacker->params->MyModName, params_stacker->InRing);

		/* Start main service loop for current connection
		 ************************************************/
		while( !testlck_flags_term(FLAG_TERM_REQ_MS) && !error_occurred)
		{
			/* Get a message from transport ring
			 ************************************/
			ret = tport_copyfrom( &InRegion, params_stacker->GetLogo, params_stacker->nLogo, &reclogo,
					&recsize, msgb, params_stacker->params->MaxMsgSize, &inseq );

			switch (ret) {
				case GET_NONE:
					/* Wait if no messages for us */
					sleep_ew(50); 
					continue;
					break;
				case GET_TOOBIG:
					time(&now);
					snprintf( errText, STR_LEN_ERRTEXT-1, "%s(%s): %ld %d msg[%ld] i%d m%d t%d too long for target",
							params_stacker->params->cmdname, params_stacker->params->MyModName,
							now, ERR_TOOBIG, recsize, (int) reclogo.instid,
							(int) reclogo.mod, (int)reclogo.type );
					ew2openapi_status( &params_stacker->params->ErrorLogo, errText, params_stacker->params );
					continue;
					break;
				case GET_MISS_LAPPED:
					time(&now);
					snprintf( errText, STR_LEN_ERRTEXT-1, "%s(%s): %ld %d msg(s) overwritten i%d m%d t%d in %s",
							params_stacker->params->cmdname, params_stacker->params->MyModName,
							now, ERR_MISSMSG, (int) reclogo.instid,
							(int) reclogo.mod, (int)reclogo.type, params_stacker->InRing );
					ew2openapi_status( &params_stacker->params->ErrorLogo, errText, params_stacker->params );
					break;
				case GET_MISS_SEQGAP:
					time(&now);
					snprintf( errText, STR_LEN_ERRTEXT-1, "%s(%s): %ld %d gap in msg sequence i%d m%d t%d in %s",
							params_stacker->params->cmdname, params_stacker->params->MyModName,
							now, ERR_MISSMSG, (int) reclogo.instid,
							(int) reclogo.mod, (int)reclogo.type, params_stacker->InRing );
					ew2openapi_status( &params_stacker->params->ErrorLogo, errText, params_stacker->params );
					break;
					/* only for tport_getmsg() 
					   case GET_MISS:
					   time(&now);
					   snprintf( errText, STR_LEN_ERRTEXT-1, "%s(%s): %ld %d missed msg(s) i%d m%d t%d in %s",
					   params_stacker->params->cmdname, params_stacker->params->MyModName,
					   now, ERR_MISSMSG, (int) reclogo.instid,
					   (int) reclogo.mod, (int)reclogo.type, params_stacker->InRing );
					   ew2openapi_status( &params_stacker->params->ErrorLogo, errText, params_stacker->params );
					   break;
					   */
				case GET_NOTRACK:
					time(&now);
					snprintf( errText, STR_LEN_ERRTEXT-1, "%s(%s): %ld %d no tracking for logo i%d m%d t%d in %s",
							params_stacker->params->cmdname, params_stacker->params->MyModName,
							now, ERR_NOTRACK, (int) reclogo.instid, (int) reclogo.mod, 
							(int)reclogo.type, params_stacker->InRing );
					ew2openapi_status( &params_stacker->params->ErrorLogo, errText, params_stacker->params );
					break;
			}


			/* Process retrieved msg (ret==GET_OK,GET_MISS,GET_NOTRACK) 
			 ***********************************************************/
			time(&time_msg);
			memcpy(&msgb[recsize], &time_msg, sizeof(time_msg));
			recsize += sizeof(time_msg);
			msgb[recsize] = '\0';

			/* put the message into the queue */
			WAIT_SPECIFIC_SEMAPHORE_EW(sema_CountAvail);
			RequestSpecificMutex(&mutex_OutQueue);
			ret=enqueuering( &OutQueue, msgb, recsize, reclogo, inkey, inseq); 
			ReleaseSpecificMutex(&mutex_OutQueue);
			POST_SPECIFIC_SEMAPHORE_EW(sema_CountBusy);

			/* Increment sema_CountToProcess */
			POST_SPECIFIC_SEMAPHORE_EW(sema_CountToProcess);

			switch(ret) {
				case -2:
					/* Serious: quit */
					/* Currently, eneueue() in mem_circ_queue.c never returns this error. */
					time(&now);
					snprintf(errText, STR_LEN_ERRTEXT-1,"%ld %d internal queue error. Terminating.", now, ERR_QUEUE);
					ew2openapi_status( &params_stacker->params->ErrorLogo, errText, params_stacker->params );
					error_occurred = 1;
					break;
				case -1:
					time(&now);
					snprintf(errText, STR_LEN_ERRTEXT-1,"%ld %d message too big for queue.", now,
							ERR_QUEUE);
					ew2openapi_status( &params_stacker->params->ErrorLogo, errText, params_stacker->params );
					continue;
					break;
				case -3:
					NumOfTimesQueueLapped++;
					ew2openapi_logit("et", "%s(%s): Circular queue lapped. Messages lost.\n",
							params_stacker->params->cmdname, params_stacker->params->MyModName);
					continue; 
			}
		} /* end of while */

		/* Detach from shared memory regions and terminate
		 ***********************************************/
		tport_detach( &InRegion );

		/* Deallocate space of messages buffer
		 ***********************************************************/
		free(msgb);
		msgb=NULL;
	}

	/* we're quitting 
	 *****************/
	ew2openapi_logit("et", "ew2openapi: thread MessageStacker() on ring %s terminated !\n",
			params_stacker->InRing );
	dec_nStackerThread();
	KillSelfThread(); /* main thread will not restart us */
	RETURN_THREAD_FUNCTION;
}


/************* Main Thread for populating DB from queue  ***************
 *          Pull a messsage from the queue, and call stored procedure  *
 *          for inserting information in to the database               *
 **********************************************************************/
thr_ret QueueDumpFile( void *p )
{
	TYPE_PARAMS *params = (TYPE_PARAMS *) p;
	MSG_LOGO reclogo;
	int      ret;
	long     msgSize;
	// char     errText[STR_LEN_ERRTEXT];
	char     *Wrmsg = NULL;           /* message to get from queue         */

	unsigned char inseq = 0;
	long     inkey;

	int      ret_dq;                 /* stores errors from dumpqueue() and undumpqueue() */
	int      cur_NumOfElements = 0;  /* Current value of number of elements in OutQueue */
	QUEUE    LocOutQueue;  /* queue for saving messages read from global queue */
	int      flag_queue_is_empty = 0;

	ew2openapi_logit( "et", "ew2openapi: thread QueueDumpFile() started.\n");

	initqueue( &LocOutQueue, (unsigned long)params->QueueSize,(unsigned long)(params->MaxMsgSize+1));

	/* set last character to zero */
	// errText[STR_LEN_ERRTEXT-1]=0;

	/* Allocate buffer for reading message from queue  */
	if ( ( Wrmsg = (char *) malloc(params->MaxMsgSize+1) ) ==  NULL ) 
	{
		ew2openapi_logit( "et", "%s(%s): error allocating Wrmsg; exiting!\n",
				params->cmdname, params->MyModName );
		exit( -1 );
	}

	while (!testlck_flags_term(FLAG_TERM_REQ_QF) && flag_queue_is_empty == 0) {   /* main loop */

		/* Get message from queue without removing it
		 *************************/
		/* N.B. use sema_CountToProcess to synchronize receiving of messages
		 * and avoid to use sleep_ew() when queue is empty */
		WAIT_SPECIFIC_SEMAPHORE_EW(sema_CountToProcess);

		RequestSpecificMutex(&mutex_OutQueue);
		ret=cpqueuering( &OutQueue, Wrmsg, &msgSize, &reclogo, &inkey, &inseq);
		ReleaseSpecificMutex(&mutex_OutQueue);

		/* The following condition could occur only one time
		 * when the module quits.  */
		if (ret < 0 )
		{ /* -1 means empty queue */
			ew2openapi_logit("et", "QueueDumpFile: queue is empty!\n");
			flag_queue_is_empty = 1;
			continue;
		}

		/* Set for safety but only after checking ret value */
		Wrmsg[msgSize] = '\0';

		/* Remove last message from queue
		 *************************/
		WAIT_SPECIFIC_SEMAPHORE_EW(sema_CountBusy);
		RequestSpecificMutex(&mutex_OutQueue);
		ret=dequeuering( &OutQueue, Wrmsg, &msgSize, &reclogo, &inkey, &inseq);
		ReleaseSpecificMutex(&mutex_OutQueue);
		POST_SPECIFIC_SEMAPHORE_EW(sema_CountAvail);

		if(ret == 0) {
			ret=enqueuering( &LocOutQueue, Wrmsg, msgSize, reclogo, inkey, inseq ); 
		}

	}   /* End of main loop */

	/* Dump queue messages to file */
	cur_NumOfElements = LocOutQueue.NumOfElements;
	ret_dq=dumpqueue( &LocOutQueue, params->QueueFilename);

	switch (ret_dq) {
		case -1:
			ew2openapi_logit("et", "errors opening or writing dump file '%s'; dump file is deleted.\n", params->QueueFilename);
			break;
		case 0:
			ew2openapi_logit("et", "dumped %d queue messages to file '%s'.\n", cur_NumOfElements, params->QueueFilename);
			break;
		default:
			ew2openapi_logit("et", "Unknown error %d from dumpqueue() writing file '%s'.\n", ret_dq, params->QueueFilename);
			break;
	}

	freequeue(&LocOutQueue);

	/* we're quitting 
	 *****************/
	ew2openapi_logit("et", "ew2openapi: thread QueueDumpFile() terminated !\n");
	setlck_flags_term(FLAG_TERM_THR_QF);
	KillSelfThread(); /* main thread will not restart us */
	RETURN_THREAD_FUNCTION;
}


/*****************************************************************************
 *  ew2openapi_config() processes command file(s) using kom.c functions;        *
 *                    exits if any errors are encountered.	             *
 *****************************************************************************/
#define NCOMMANDS 9
void ew2openapi_config( char *configfile, TYPE_PARAMS *params )
{
	int      ncommand;     /* # of required commands you expect to process   */ 
	char     init[NCOMMANDS];     /* init flags, one byte for each required command */
	int      nmiss;        /* number of required commands that were missed   */
	char    *com;
	int      nfiles;
	int      success;
	int      i;	
	char*    str;
	unsigned char modid;
	char     processor[30];
	int      len = 0;

	params->HeartBeatRing[0] = 0;
	params->nRings = 0;
	params->WaitSecAfterServiceError = 5; /* Seconds to wait after a Service Error */

	/* Set to zero one init flag for each required command 
	 *****************************************************/   
	ncommand = NCOMMANDS;
	for( i=0; i<ncommand; i++ )  init[i] = 0;
	params->nLogo = 0;
	params->GetLogo = NULL;

	params->kafka_brokers[0] = 0;
	strncpy( params->kafka_topic_prefix, "ew2openapi_test", 255);

	params->rmq_hostname[0] = 0;
	params->rmq_port = RABBITMQ_DEFAULT_PORT;
	params->rmq_exchange[0] = 0;
	params->rmq_vhost[0] = 0;
	params->rmq_username[0] = 0;
	params->rmq_password[0] = 0;

	params->osc_hostname[0] = 0;
	params->osc_port = OSC_DEFAULT_PORT;

	/* init values */
	params->send_message_format_flags = MESSAGE_FORMAT_UNDEF;

	params->flag_handler = FLAG_HANDLER_NONE;
	params->ewinstancename[0] = 0;

	params->ServiceType[0] = 0;

	params->ApiBaseUrl[0] = 0;

	params->QueueFilename[0] = 0;
	params->QueueFilename[FILENAME_MAXLEN - 1] = 0;

	/* Open the main configuration file 
	 **********************************/
	nfiles = k_open( configfile ); 
	if ( nfiles == 0 ) {
		ew2openapi_logit( "e" ,
				"%s: Error opening command file <%s>; exiting!\n", 
				params->cmdname, configfile );
		exit( -1 );
	}

	/* Process all command files
	 ***************************/
	while(nfiles > 0)   /* While there are command files open */
	{
		while(k_rd())        /* Read next line from active file  */
		{  
			com = k_str();         /* Get the first token from line */

			/* Ignore blank lines & comments
			 *******************************/
			if( !com )           continue;
			if( com[0] == '#' )  continue;

			/* Open a nested configuration file 
			 **********************************/
			if( com[0] == '@' ) {
				success = nfiles+1;
				nfiles  = k_open(&com[1]);
				if ( nfiles != success ) {
					ew2openapi_logit( "e" , 
							"%s: Error opening command file <%s>; exiting!\n",
							params->cmdname, &com[1] );
					exit( -1 );
				}
				continue;
			}

			/* Process anything else as a command
			 *              ************************************/
			strcpy( processor, "ew2openapi_config" );

			/* Process anything else as a command 
			 ************************************/
			/*0*/     if( k_its("LogFile") ) {
				params->LogSwitch = k_int();
				init[0] = 1;
			}
			/*1*/     else if( k_its("MyModuleId") ) {
				str = k_str();
				if(str) strncpy( params->MyModName, str, MAX_MOD_STR-1 );
				if ( GetModId( params->MyModName, &modid ) != 0 ) {
					ew2openapi_logit( "e",
							"%s: Invalid module name <%s>; exiting!\n", 
							params->cmdname, params->MyModName );
					exit( -1 );
				}
				snprintf(params->QueueFilename, FILENAME_MAXLEN - 1, "ew2openapi_%03d.queue", (int) modid);
				init[1] = 1;
			}
			/*2*/     else if( k_its("InRing") ) {
				str = k_str();
				if(str) {
					/* First ring declared will be used as HeartBeat ring */
					if(params->HeartBeatRing[0] == 0 ) {
						strncpy( params->HeartBeatRing, str, MAX_RING_STR-1 );
					}
					strncpy( params->InRings[params->nRings], str, MAX_RING_STR-1 );
					params->nRings++;
				}
				init[2] = 1;
			}
			/*3*/     else if( k_its("HeartBeatInt") ) {
				params->HeartBeatInt = k_int();
				init[3] = 1;
			}

			/* Enter installation & module & message types to get
			 ****************************************************/
			/*4*/     else if( k_its("GetMsgLogo") ) {
				if ((params->GetLogo = (MSG_LOGO*)realloc(params->GetLogo, (params->nLogo+1) * sizeof(MSG_LOGO))) 
						== NULL) {
					ew2openapi_logit( "e" , 
							"%s: out of memory for Logos\n", params->cmdname );
					exit( -1 );
				}		
				if( ( str=k_str() ) ) {
					if( GetInst( str, &params->GetLogo[params->nLogo].instid ) != 0 ) {
						ew2openapi_logit( "e" , 
								"%s: Invalid installation name <%s>", params->cmdname, str ); 
						ew2openapi_logit( "e" , " in <GetMsgLogo> cmd; exiting!\n" );
						exit( -1 );
					}
				}
				if( ( str=k_str() ) ) {
					if( GetModId( str, &params->GetLogo[params->nLogo].mod ) != 0 ) {
						ew2openapi_logit( "e" , 
								"%s: Invalid module name <%s>", params->cmdname, str ); 
						ew2openapi_logit( "e" , " in <GetMsgLogo> cmd; exiting!\n" );
						exit( -1 );
					}
				}
				if( ( str=k_str() ) ) {
					if( GetType( str, &params->GetLogo[params->nLogo].type ) != 0 ) {
						ew2openapi_logit( "e" , 
								"%s: Invalid msgtype <%s>", params->cmdname, str ); 
						ew2openapi_logit( "e" , " in <GetMsgLogo> cmd; exiting!\n" );
						exit( -1 );
					}
				}
				params->nLogo++;
				init[4] = 1;
			}

			/* Maximum size (bytes) for incoming/outgoing messages
			 *****************************************************/ 
			/*5*/     else if( k_its("MaxMsgSize") ) {
				params->MaxMsgSize = k_long();
				init[5] = 1;
			}

			/* Maximum number of messages in outgoing circular buffer
			 ********************************************************/ 
			/*6*/     else if( k_its("QueueSize") ) {
				params->QueueSize = k_long();
				init[6] = 1;
			}

			/* EW instance name
			 ********************/
			/*7*/   else if( k_its("EWInstanceName") ) {
				if ( (str = k_str() ) ) {
					strncpy(params->ewinstancename, str, 255);
					params->ewinstancename[255] = '\0';
					init[7] = 1;
				}
			}

			/* Service Type
			 ********************/
			/*8*/   else if( k_its("ServiceType") ) {
				if ( (str = k_str() ) ) {
					strncpy(params->ServiceType, str, 256);
					params->ServiceType[255] = '\0';
					init[8] = 1;
				}
			}

			/* Optional commands */

			/* Api Base Url
			 ********************/
			else if( k_its("ApiBaseUrl") ) {
				/*
				 * if(params->rmq_hostname[0] != 0) {
				 *     ew2openapi_logit( "et" , "%s: You can define only one parameter between ApiBaseUrl and RMQHostname.\n", params->cmdname );
				 *     exit( -1 );
				 * }
				 */
				if ( (str = k_str() ) ) {
					strncpy(params->ApiBaseUrl, str, 1023);
					params->ApiBaseUrl[1023] = '\0';

					/* if occurs, trim last character equal to '/' */
					len = strlen(params->ApiBaseUrl);
					if(params->ApiBaseUrl[len - 1] == '/') {
						params->ApiBaseUrl[len - 1] = '\0';
					}

				}
			}

			/* Kafka Broker(s) comma-separated-values of host or host:port (default port 9092)
			 ********************/
			else if( k_its("KafkaBrokers") ) {
				if ( (str = k_str() ) ) {
					strncpy(params->kafka_brokers, str, 255);
					params->kafka_brokers[255] = '\0';
				}
			}

			/* Kafka Topic Prefix
			 ********************/
			else if( k_its("KafkaTopicPrefix") ) {
				if ( (str = k_str() ) ) {
					strncpy(params->kafka_topic_prefix, str, 255);
					params->kafka_topic_prefix[255] = '\0';
				}
			}


			/* RabbitMQ hostname
			 ********************/
			else if( k_its("RMQHostname") ) {
				/*
				 * if(params->ApiBaseUrl[0] != 0) {
				 *     ew2openapi_logit( "et" , "%s: You can define only one parameter between ApiBaseUrl and RMQHostname.\n", params->cmdname );
				 *     exit( -1 );
				 * }
				 */
				if ( (str = k_str() ) ) {
					strncpy(params->rmq_hostname, str, 255);
					params->rmq_hostname[255] = '\0';
				}
			}

			/* RabbitMQ username
			 ********************/
			else if( k_its("RMQUsername") ) {
				if ( (str = k_str() ) ) {
					strncpy(params->rmq_username, str, 255);
					params->rmq_username[255] = '\0';
				}
			}

			/* RabbitMQ password
			 ********************/
			else if( k_its("RMQPassword") ) {
				if ( (str = k_str() ) ) {
					strncpy(params->rmq_password, str, 255);
					params->rmq_password[255] = '\0';
				}
			}

			/* RabbitMQ Exchange
			 ********************/
			else if( k_its("RMQExchange") ) {
				if ( (str = k_str() ) ) {
					strncpy(params->rmq_exchange, str, 255);
					params->rmq_exchange[255] = '\0';
				}
			}

			/* RabbitMQ VirtualHost
			 ********************/
			else if( k_its("RMQVirtualHost") ) {
				if ( (str = k_str() ) ) {
					strncpy(params->rmq_vhost, str, 255);
					params->rmq_vhost[255] = '\0';
				}
			}

			/*opt*/ else if( k_its("RMQPort") ) {
				params->rmq_port = k_int();
			}

			/* OSC Server
			 ********************/
			else if( k_its("OscServer") ) {
				if ( (str = k_str() ) ) {
					strncpy(params->osc_hostname, str, 255);
					params->osc_hostname[255] = '\0';
				}
			}

			/*opt*/ else if( k_its("OscServerPort") ) {
				params->osc_port = k_int();
			}

			/* Read the full path to the program used to send mail
			 *       ****************************************************/
			else if( k_its( "SendMessageFormat" ) )  /* optional */
			{
				if( (str = k_str()) )
				{
					if(strcmp(str, "RAW") == 0) {
						params->send_message_format_flags |= MESSAGE_FORMAT_RAW;
					} else if(strcmp(str, "JSON") == 0) {
						params->send_message_format_flags |= MESSAGE_FORMAT_JSON;
					} else if(strcmp(str, "GeoJSON") == 0) {
						params->send_message_format_flags |= MESSAGE_FORMAT_GEOJSON;
					} else {
						ew2openapi_logit("e","ew2openapi: Invalid SendMessageFormat value '%s'\n"
								"It must be one of the following: RAW, JSON or GeoJSON; exiting.\n", str
							 );
						exit( -1 );
					}
				}
			}

			/* Seconds to wait after a Service Error
			 ********************************************************/ 
			else if( k_its("WaitSecAfterServiceError") ) {
				params->WaitSecAfterServiceError = k_long();
			}

			/* Some commands may be processed by other functions
			 *****************/
			else if( site_com()   ) strcpy( processor, "site_com" );

			/* Unknown command
			 *****************/ 
			else {
				ew2openapi_logit( "e" , "%s: <%s> Unknown command in <%s>.\n", 
						params->cmdname, com, configfile );
				continue;
			}

			/* See if there were any errors processing the command 
			 *****************************************************/
			if( k_err() ) {
				ew2openapi_logit( "e" , 
						"%s: Bad <%s> command for %s() in <%s>; exiting!\n",
						params->cmdname, com, processor, configfile );
				exit( -1 );
			}
		}
		nfiles = k_close();
	}

	/* After all files are closed, check init flags for missed commands
	 ******************************************************************/
	nmiss = 0;
	for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
	if ( nmiss ) {
		ew2openapi_logit( "e", "%s: ERROR, no ", params->cmdname );
		if ( !init[0] )  ew2openapi_logit( "e", "<LogFile> "      );
		if ( !init[1] )  ew2openapi_logit( "e", "<MyModuleId> "   );
		if ( !init[2] )  ew2openapi_logit( "e", "<InRing> "     );
		if ( !init[3] )  ew2openapi_logit( "e", "<HeartBeatInt> " );
		if ( !init[4] )  ew2openapi_logit( "e", "<GetMsgLogo> "   );
		if ( !init[5] )  ew2openapi_logit( "e", "<MaxMsgSize> "  );
		if ( !init[6] )  ew2openapi_logit( "e", "<Queue>"   );
		if ( !init[7] )  ew2openapi_logit( "e", "<EWInstanceName>"   );
		if ( !init[8] )  ew2openapi_logit( "e", "<ServiceType>"   );
		ew2openapi_logit( "e" , "command(s) in <%s>; exiting!\n", configfile );
		exit( -1 );
	}

	/* Check values for ServiceType */
	if(strncmp(params->ServiceType, "CaravelWS", 255) == 0) {
		/* Check if ApiBaseUrl is defined */
		if(params->ApiBaseUrl[0] == 0) {
			ew2openapi_logit( "e" , "%s: Since ServiceType = 'CaravelWS', then you must define parameter ApiBaseUrl.", params->cmdname );
			exit( -1 );
		}

		/* Set params->flag_handler based on several other parameters */
		params->flag_handler = FLAG_HANDLER_QUAKEDB_WS;

	} else if(strncmp(params->ServiceType, "Kafka", 255) == 0) {
		/* Check if KafkaBrokers is defined */
		if(params->kafka_brokers[0] == 0) {
			ew2openapi_logit( "e" , "%s: Since ServiceType = 'Kafka', then you must define parameter KafkaBrokers.", params->cmdname );
			exit( -1 );
		}

		/* Set params->flag_handler based on several other parameters */
		params->flag_handler = FLAG_HANDLER_KAFKA;

	} else if(strncmp(params->ServiceType, "RabbitMQ", 255) == 0) {
		/* Check if RMQHostname is defined */
		if(params->rmq_hostname[0] == 0) {
			ew2openapi_logit( "e" , "%s: Since ServiceType = 'RabbitMQ', then you must define parameter RMQHostname.", params->cmdname );
			exit( -1 );
		}

		/* Set params->flag_handler based on several other parameters */
		params->flag_handler = FLAG_HANDLER_RABBITMQ;

	} else if(strncmp(params->ServiceType, "OSC", 255) == 0) {

		/* Check if OSC hostname is defined */
		if(params->osc_hostname[0] == 0) {
			ew2openapi_logit( "e" , "%s: Since ServiceType = 'OSC', then you must define parameter OscServer.", params->cmdname );
			exit( -1 );
		}

		/* Set params->flag_handler based on several other parameters */
		params->flag_handler = FLAG_HANDLER_OSC;

	} else {
		ew2openapi_logit( "et" , "%s: ServiceType can be only 'CaravelWS', 'Kafka', 'RabbitMQ' or 'OSC'.\n", params->cmdname );
		exit( -1 );
	}

	/* Check and set SendMessageFormat */
	if(params->send_message_format_flags == MESSAGE_FORMAT_UNDEF) {
		/* default value */
		params->send_message_format_flags = MESSAGE_FORMAT_RAW;
		ew2openapi_logit("e","ew2openapi: WARNING: set default value for SendMessageFormat to RAW.\n");
	} else if(
			params->send_message_format_flags & MESSAGE_FORMAT_JSON
			&&
			params->send_message_format_flags & MESSAGE_FORMAT_GEOJSON
			) {
		ew2openapi_logit("e","ew2openapi: SendMessageFormat values JSON and GeoJSON are mutually exclusive; exiting.\n");
		exit( -1 );
	}

	return;
}


/****************************************************************************
 *  ew2openapi_lookup()   Look up important info from earthworm.h tables     *
 ****************************************************************************/
void ew2openapi_lookup( TYPE_PARAMS *params )
{
	/* Look up keys to shared memory regions
	 *************************************/
	if( ( params->HeartBeatRingKey = GetKey(params->HeartBeatRing) ) == -1 ) {
		ew2openapi_logit( "e",
				"%s:  Invalid ring name <%s>; exiting!\n", 
				params->cmdname, params->HeartBeatRing);
		exit( -1 );
	}   

	/* Look up installations of interest
	 *********************************/
	if ( GetLocalInst( &params->HeartLogo.instid ) != 0 ) {
		ew2openapi_logit( "e",
				"%s: error getting local installation id; exiting!\n",
				params->cmdname );
		exit( -1 );
	}
	params->ErrorLogo.instid = params->HeartLogo.instid;

	/* Look up modules of interest
	 ***************************/
	if ( GetModId( params->MyModName, &params->HeartLogo.mod ) != 0 ) {
		ew2openapi_logit( "e",
				"%s: Invalid module name <%s>; exiting!\n", 
				params->cmdname, params->MyModName );
		exit( -1 );
	}
	params->ErrorLogo.mod = params->HeartLogo.mod;

	/* Look up message types of interest
	 *********************************/
	if ( GetType( "TYPE_HEARTBEAT", &params->HeartLogo.type ) != 0 ) {
		ew2openapi_logit( "e",
				"%s: Invalid message type <TYPE_HEARTBEAT>; exiting!\n", params->cmdname );
		exit( -1 );
	}
	if ( GetType( "TYPE_ERROR", &params->ErrorLogo.type ) != 0 ) {
		ew2openapi_logit( "e",
				"%s: Invalid message type <TYPE_ERROR>; exiting!\n", params->cmdname );
		exit( -1 );
	}

} 


/***************************************************************************
 * ew2openapi_status() sends an error or heartbeat message to transport       *
 *    If the message is of TYPE_ERROR, the text will also be logged.       *
 *    Since ew2openapi_status is called be more than one thread, the logo     *
 *    and message must be constructed by the caller.                       *
 ***************************************************************************/
void ew2openapi_status( MSG_LOGO *logo, char *msg, TYPE_PARAMS *params)
{
	size_t size;

	if( logo->type == params->ErrorLogo.type)
		ew2openapi_logit( "et", "%s\n", msg );

	size = strlen( msg );   /* don't include the null byte in the message */ 	

	/* Write the message to shared memory
	 ************************************/
	if( tport_putmsg( &params->HeartBeatRegion, logo, size, msg ) != PUT_OK ) {
		ew2openapi_logit("et", "%s(%s):  Error sending message to transport.\n", 
				params->cmdname, params->MyModName );
	}
}

/***************************************************************************
 * ew2openapi_mysql_status()                                                *
 *    If ret_ex_mysql_query is not equal to EW2OPENAPI_MYSQL_OK then report error *
 *    message on ring by ew2openapi_status().                               *
 ***************************************************************************/
// void ew2openapi_mysql_status(int ret_ex_mysql_query, TYPE_PARAMS *params) {
//   time_t   now;	/* current time, used for error messages */
//   char    errText[STR_LEN_ERRTEXT];
// 
//   /* set last character to zero */
//   errText[STR_LEN_ERRTEXT-1]=0;
// 
//   if(ret_ex_mysql_query != EW2OPENAPI_MYSQL_OK) {
//     time(&now);
//     switch(ret_ex_mysql_query) {
//       case EW2OPENAPI_MYSQL_ERR_CONNDB:
//       case CR_SERVER_GONE_ERROR:
// 	snprintf( errText, STR_LEN_ERRTEXT-1, "%ld %d %s Error connecting to %s:%ld. (MySQL error %d).", now, 0, params->MyModName, params->db_hostname, params->db_port, ret_ex_mysql_query);
// 	break;
//       default:
// 	snprintf( errText, STR_LEN_ERRTEXT-1, "%ld %d %s Error executing query for %s:%ld. (MySQL error %d).", now, 0, params->MyModName, params->db_hostname, params->db_port, ret_ex_mysql_query);
// 	break;
//     }
//     ew2openapi_status( &params->ErrorLogo, errText, params );
//   }
// }

/* increment the number of running Stacker Threads
 ***************************************************/
void inc_nStackerThread() {
	RequestSpecificMutex(&mutex_nStackerThread);
	nStackerRunning++;
	ReleaseSpecificMutex(&mutex_nStackerThread);
}

/* decrement the number of running Stacker Threads and
 * in case they are zero set the flag FLAG_TERM_THR_STACKER
 ***************************************************/
void dec_nStackerThread() {
	RequestSpecificMutex(&mutex_nStackerThread);
	nStackerRunning--;
	if(nStackerRunning <= 0) {
		setlck_flags_term(FLAG_TERM_THR_MS);
	}
	ReleaseSpecificMutex(&mutex_nStackerThread);
}


/* Remove duplicated entries in ringNames
 ***************************************************/
int remove_duplicated_ringname(char ringNames[MAX_NUM_OF_RINGS][MAX_RING_STR], int *nRings) {
	int ret = 0;
	int r = 0;
	int k = 0;
	int j = 0;

	r = 0;
	while(r < *nRings) {
		k = r+1;

		while(k < *nRings) {
			if(strcmp(ringNames[r], ringNames[k]) == 0) {
				ret++;
				ew2openapi_logit("et",  "ew2openapi: WARNING: ring name %s duplicated, skip!\n", ringNames[r]);
				/* override the string at k position */
				for(j=k; j < *nRings - 1; j++) {
					strncpy(ringNames[j], ringNames[j+1], MAX_RING_STR-1);
				}
				*nRings = *nRings - 1;
			} else {
				k++;
			}
		}

		r++;
	}

	return ret;
}

