#ifndef EW2OPENAPI_EQ_VERSION_NAME_H
#define EW2OPENAPI_EQ_VERSION_NAME_H 1


#define MAX_EW2OPENAPI_VERSION_NAME 3
extern const char *ew2openapi_version_name[MAX_EW2OPENAPI_VERSION_NAME];
extern const char *ew2openapi_version_name_unknown;

extern const char *ew2openapi_get_name_version(int version);


#endif

