#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <earthworm.h>

#include "ew2openapi_version.h"
#include "ew2openapi_logit.h"
#include "ew2openapi_handler_quakedb_ws.h"
#include "ew2openapi_json_utils.h"
#include "ew2openapi_handler_status.h"

/* * * * * * * * * *
 * Private functions
 * * * * * * * * * */
long ew2openapi_handler_quakedb_ws_sendstring(char *ApiBaseUrl, char *buf, size_t len, MSG_LOGO *reclogo, char flag_geojson);

/*
 * static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
 * {
 *     size_t realsize = size * nmemb;
 *     struct MemoryStruct *mem = (struct MemoryStruct *)userp;
 *
 *     mem->memory = realloc(mem->memory, mem->size + realsize + 1);
 *     if(mem->memory == NULL) {
 *         [> out of memory! <]
 *         printf("not enough memory (realloc returned NULL)\n");
 *         return 0;
 *     }
 *
 *     memcpy(&(mem->memory[mem->size]), contents, realsize);
 *     mem->size += realsize;
 *     mem->memory[mem->size] = 0;
 *
 *     return realsize;
 * }
 */

static size_t ew2openapi_handler_quakedb_ws_WRITEFUNCTION_ew2openapi_logit(void *contents, size_t size, size_t nmemb, void *userp)
{
	size_t realsize = size * nmemb;
	char *contents_string = NULL;

	if(realsize > 0) {
		if( (contents_string = malloc(realsize + 1)) != NULL) {
			memcpy(contents_string, contents, realsize);
			contents_string[realsize] = 0;
			ew2openapi_logit("et", "RESPONSE: %s\n", contents_string);
			free(contents_string);
		}
	}

	return realsize;
}

/* * * * * * * * * *
 * Public functions
 * * * * * * * * * */

/* Wrapper to curl_global_init() */
int ew2openapi_handler_quakedb_ws_init(TYPE_PARAMS *params) {
	int ret = 0;
	CURLcode ret_curl;
	long flags = CURL_GLOBAL_ALL;
	long curl_http_code = -1;  /* Init error code */

	/* Exit if ret_curl is non-zero https://curl.se/libcurl/c/curl_global_init.html */
	ret_curl = curl_global_init(flags);
	if(ret_curl != 0) {
		ew2openapi_logit("et", "ew2openapi_handler_quakedb_ws_init(): initialization error. Exit.\n");
		exit(-1);
	}

	/* Exit if curl_http_code == -1 or not between 100 and 299 */
	curl_http_code = ew2openapi_handler_quakedb_ws_check_status(params->ApiBaseUrl);
	if(curl_http_code >= 100 && curl_http_code < 300) {
		ew2openapi_logit("et", "ew2openapi_handler_quakedb_ws_check_status(): ok http code=%ld. Exit.\n", curl_http_code);
	} else {
		ew2openapi_logit("et", "ew2openapi_handler_quakedb_ws_check_status(): error %ld. Exit.\n", curl_http_code);
		exit(-1);
	}

	return ret;
}

/* Check quakedb/status API. Return http code. Otherwise, return -1. */
int ew2openapi_handler_quakedb_ws_check_status(char *ApiBaseUrl) {
	CURL *curl;
	CURLcode res;
	long curl_http_code = -1;  /* Init error code */
	char url[MAX_API_URL];

	struct curl_slist *headers = NULL;
	const char *curl_header_accept_json          = "Accept: application/json";
	const char *curl_header_content_type_json    = "Content-Type: application/json";

	snprintf(url, MAX_API_URL - sizeof(EW2OPENAPI_URL_PATH_STATUS) - 1, "%s/%s", ApiBaseUrl, EW2OPENAPI_URL_PATH_STATUS);

	headers = curl_slist_append(headers, curl_header_accept_json);
	headers = curl_slist_append(headers, curl_header_content_type_json);
	headers = curl_slist_append(headers, "charsets: utf-8");

	curl = curl_easy_init();

	if(curl) {
		/* some servers don't like requests that are made without a user-agent
		   field, so we provide one */
		curl_easy_setopt(curl, CURLOPT_USERAGENT, EW2OPENAPI_USERAGENT);
		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

		/* send all data to this function  */
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, ew2openapi_handler_quakedb_ws_WRITEFUNCTION_ew2openapi_logit);

		/* Perform the request, res will get the return code */
		res = curl_easy_perform(curl);

		/* Check for errors */
		if(res == CURLE_OK) {
			curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &curl_http_code);
			/* If http code is between 100 and 299, it should be ok. Print only the returned http code */
			if(curl_http_code >= 100 && curl_http_code < 300) {
				// Do nothing
			} else {
				switch(curl_http_code) {
					default:
						ew2openapi_logit("et", "Error POST to %s with http code %ld.\n", url, curl_http_code);
						break;
				}
			}
		} else {
			ew2openapi_logit("et", "curl_easy_perform() failed: %s\n",
					curl_easy_strerror(res));
		}

		/* always cleanup */
		curl_easy_cleanup(curl);
	}

	return curl_http_code;
}

/* Wrapper to curl_global_cleanup() */
int ew2openapi_handler_quakedb_ws_destroy(TYPE_PARAMS *params) {
	int ret = 0;
	curl_global_cleanup();
	return ret;
}

#define MAX_LEN_TYPE_EW_MESSAGE 64

/* Send Earthworm message to EWOpenAPI */
long ew2openapi_handler_quakedb_ws_sendstring(char *ApiBaseUrl, char *buf, size_t len, MSG_LOGO *reclogo, char flag_geojson)
{
	CURL *curl;
	CURLcode res;
	long curl_http_code = -1;  /* Init error code */

	unsigned long i = 0;
	char ch;

	char url[MAX_API_URL];
	char api_type_ew_msg[MAX_LEN_TYPE_EW_MESSAGE];
	const char *JsonObj_pre = "{\"data\":";
	const char *JsonObj_post = "}";
	size_t JsonObj_len = len + strlen(JsonObj_pre) + strlen(JsonObj_post) + 10;
	char *JsonObj = malloc(JsonObj_len);
	struct curl_slist *headers = NULL;

	const char *curl_header_accept_json          = "Accept: application/json";
	const char *curl_header_content_type_json    = "Content-Type: application/json";
	const char *curl_header_accept_geojson       = "Accept: application/geo+json";
	const char *curl_header_content_type_geojson = "Content-Type: application/geo+json";

	if(JsonObj) {
		JsonObj[JsonObj_len - 1] = 0;
		snprintf(JsonObj, JsonObj_len - 1, "%s%s%s", JsonObj_pre, buf, JsonObj_post);
	} else {
		ew2openapi_logit("et", "ew2openapi_handler_quakedb_ws_sendstring(): error allocating JsonObj.\n");
		return curl_http_code;
	}

	headers = curl_slist_append(headers, (flag_geojson)? curl_header_accept_geojson       : curl_header_accept_json);
	headers = curl_slist_append(headers, (flag_geojson)? curl_header_content_type_geojson : curl_header_content_type_json);
	headers = curl_slist_append(headers, "charsets: utf-8");

	/* Build part of API url based on Earthworm message type */
	strncpy(api_type_ew_msg, GetTypeName(reclogo->type), MAX_LEN_TYPE_EW_MESSAGE - 1);

	/* Remove from Earthworm messate type API url first 5 characters 'TYPE_',
	 * and lowercase the others. */
	for(i=0; i < strlen(api_type_ew_msg) - 5; i++) {
		ch = api_type_ew_msg[i+5];
		if (ch >= 'A' && ch <= 'Z') {
			ch = 'a' + (ch - 'A');
		}
		if (ch == '_' ) {
			ch = '-';
		}
		api_type_ew_msg[i] = ch;
	}
	api_type_ew_msg[i] = '\0';

	snprintf(url, MAX_API_URL - MAX_LEN_TYPE_EW_MESSAGE - sizeof(EW2OPENAPI_URL_PATH) - 1, "%s/%s/%s", ApiBaseUrl, EW2OPENAPI_URL_PATH, api_type_ew_msg);

	ew2openapi_logit("et", "URL-DATA: %s\n", url);
	ew2openapi_logit("et", "JSON-DATA: %s\n", JsonObj);

	curl = curl_easy_init();

	if(curl) {
		/* some servers don't like requests that are made without a user-agent
		   field, so we provide one */
		curl_easy_setopt(curl, CURLOPT_USERAGENT, EW2OPENAPI_USERAGENT);

		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, JsonObj);
		/* if we don't provide POSTFIELDSIZE, libcurl will strlen() by
		   itself */
		/* curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, (long)strlen(JsonObj)); */

		/* send all data to this function  */
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, ew2openapi_handler_quakedb_ws_WRITEFUNCTION_ew2openapi_logit);

		/* Perform the request, res will get the return code */
		res = curl_easy_perform(curl);

		/* Check for errors */
		if(res == CURLE_OK) {
			curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &curl_http_code);
			/* If http code is between 100 and 299, it should be ok. Print only the returned http code */
			if(curl_http_code >= 100 && curl_http_code < 300) {
				// Do nothing
			} else {
				switch(curl_http_code) {
					default:
						ew2openapi_logit("et", "Error POST to %s with http code %ld.\n", url, curl_http_code);
						break;
				}
			}
		} else {
			ew2openapi_logit("et", "curl_easy_perform() failed: %s\n",
					curl_easy_strerror(res));
		}

		/* always cleanup */
		curl_easy_cleanup(curl);
	}

	/* free JsonObj */
	free(JsonObj);

	return curl_http_code;
}

int ew2openapi_handler_quakedb_ws_handle(TYPE_DATA_HANDLER *data_handler) {
	char    *json_string = NULL;
	long    curl_http_code = 0;
	int     handler_status_code = HANDLER_STATUS_OK;
	char    flag_geojson = 0;
	if(
			data_handler->params->send_message_format_flags & MESSAGE_FORMAT_JSON
			||
			data_handler->params->send_message_format_flags & MESSAGE_FORMAT_GEOJSON
	  ) {

		/* Build the JSON string for posting to service */
		flag_geojson =  data_handler->params->send_message_format_flags & MESSAGE_FORMAT_GEOJSON;
		json_string = get_json_string_from_ew_msg(data_handler->Wrmsg, data_handler->msgSize, data_handler->reclogo, data_handler->params->ewinstancename, data_handler->ewUserName, data_handler->ewHostName, &flag_geojson);

	}

	if(json_string) {
		/* CURL message */
		ew2openapi_logit("et", "%s: %s\n", (flag_geojson)? "GeoJSON" : "JSON", json_string);
		curl_http_code = ew2openapi_handler_quakedb_ws_sendstring(data_handler->params->ApiBaseUrl, json_string, strlen(json_string), data_handler->reclogo, flag_geojson);
		ew2openapi_logit("et", "HTTP CODE: %ld\n", curl_http_code);
	}
	/* Based on curl_http_code classify the following status:
	 *
	 *   - HANDLER_STATUS_OK
	 *   - HANDLER_STATUS_ERROR_INFINITY_RETRIES
	 *   - HANDLER_STATUS_ERROR_LIMITED_RETRIES
	 *   - HANDLER_STATUS_ERROR_NO_RETRIES
	 *
	 * Refer https://httpstatuses.com/
	 *
	 */

	/* Init values before error classification */
	handler_status_code = HANDLER_STATUS_OK;

	switch(curl_http_code) {
		/* Generic errors: network connection, ... */
		case -1:
			/* Set handler_status_code to HANDLER_STATUS_ERROR_INFINITY_RETRIES */
			handler_status_code = HANDLER_STATUS_ERROR_INFINITY_RETRIES;
			break;

			/* 4xx error codes */
		case 408: // Request Timeout
		case 429: // Too Many Requests
			/* Set handler_status_code to HANDLER_STATUS_ERROR_INFINITY_RETRIES */
			handler_status_code = HANDLER_STATUS_ERROR_INFINITY_RETRIES;
			break;

		case 428: // Precondition Required
			/* Set handler_status_code to HANDLER_STATUS_ERROR_LIMITED_RETRIES_WITHIN_TIME */
			handler_status_code = HANDLER_STATUS_ERROR_LIMITED_RETRIES_WITHIN_TIME;
			break;

		case 422: // Unprocessable Entity
			handler_status_code = HANDLER_STATUS_ERROR_NO_RETRIES;
			break;

			/* 5xx error codes */
		case 500: // Internal Server Error
			// case 501: // Not Implemented
		case 502: // Bad Gateway
		case 503: // Service Unavailable
		case 504: // Gateway Timeout
			// case 505: // HTTP Version Not Supported
			// case 506: // Variant Also Negotiates
		case 507: // Insufficient Storage
			// case 508: // Loop Detected
			// case 510: // Not Extended
		case 511: // Network Authentication Required
		case 599: // Network Connect Timeout Error
			/* Set handler_status_code to HANDLER_STATUS_ERROR_INFINITY_RETRIES */
			handler_status_code = HANDLER_STATUS_ERROR_INFINITY_RETRIES;
			break;

			/* Other error codes */
		default:
			/* Do nothing */
			break;
	}

	return handler_status_code;
}
