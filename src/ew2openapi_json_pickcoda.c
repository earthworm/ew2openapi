#include <stdio.h>
#include <json-c/json.h>
#include <earthworm.h>
#include <rdpickcoda.h>
#include <site.h>

#include "ew2openapi_logit.h"
#include "ew2openapi_json_pickcoda.h"
#include "ew2openapi_utils.h"
#include "ew2openapi_json_utils.h"
#include "ew2openapi_geojson_utils.h"

#define MAX_JSON_STRING_PICKCODA 4096
char *get_json_string_from_pick_scnl_ew_struct(EWPICK *pick, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName, char flag_geojson) {
	/*Creating a json object*/
	json_object *jobj = json_object_new_object();
	json_object *jobj_ew_message = json_object_new_object();
	json_object *jobj_tmp = NULL;
	int j;

	static char json_string[MAX_JSON_STRING_PICKCODA];
	char tpick_datetime[EW2OPENAPI_DATETIME_MAXLEN_STR];
	int isite;
	double lat = 0.0, lon = 0.0;
	// double elev = 0.0;
	char Text[1024];

	/*
	   char *msgtype_name;
	   char *modid_name;
	   char *instid_name;

	   msgtype_name = GetTypeName(pick->msgtype);
	   modid_name = GetModIdName(pick->modid);
	   instid_name = GetInstName(pick->instid);
	   */

	json_string[MAX_JSON_STRING_PICKCODA - 1] = 0;

	epoch_to_datetime_str_iso8601_utc(tpick_datetime, EW2OPENAPI_DATETIME_MAXLEN_STR, pick->tpick);

	snprintf(json_string, MAX_JSON_STRING_PICKCODA - 1, "CALL sp_ins_ew_pick_scnl('%s', '%s', %d, '%s', '%s', '%s', '%s', '%c', %c, '%s', %ld, %ld, %ld);",
			/*
			   msgtype_name,
			   modid_name,
			   instid_name,
			   */
			ewInstanceName,
			GetModIdName(reclogo->mod),
			pick->seq,
			pick->net,
			pick->site,
			pick->comp,
			pick->loc,
			pick->fm,
			pick->wt,
			/*
			   pick->tpick,
			   */
			tpick_datetime,
			pick->pamp[0],
			pick->pamp[1],
			pick->pamp[2]
				);

	/* Add Earthworm logo to JSON object */
	json_object_ew_logo_objects_add(jobj, reclogo, ewInstanceName, ewUserName, ewHostName);

	json_object_object_add(jobj_ew_message,"pickId", json_object_new_int(pick->seq));
	json_object_object_add(jobj_ew_message,"network", json_object_new_string(pick->net));
	json_object_object_add(jobj_ew_message,"station", json_object_new_string(pick->site));
	json_object_object_add(jobj_ew_message,"component", json_object_new_string(pick->comp));
	json_object_object_add(jobj_ew_message,"location", json_object_new_string(pick->loc));
	json_object_object_add(jobj_ew_message,"firstMotion", json_object_new_string_len(&(pick->fm), 1));
	/* json_object_object_add(jobj_ew_message,"pickWeight", json_object_new_string_len(&(pick->wt), 1)); */
	json_object_object_add(jobj_ew_message,"pickWeight", json_object_new_int(pick->wt - '0'));
	json_object_object_add(jobj_ew_message,"timeOfPick", json_object_new_string(tpick_datetime));
	jobj_tmp = json_object_new_array();
	for(j=0; j<3; j++) {
		json_object_array_add(jobj_tmp, json_object_new_int64(pick->pamp[j]));
	}
	json_object_object_add(jobj_ew_message,"pAmplitude", jobj_tmp);

	/* Add Earthworm message to JSON object */
	json_object_ew_message_objects_add(jobj, jobj_ew_message);

	/* GeoJSON conversion */
	if(flag_geojson) {
		isite = site_index( pick->site, pick->net, pick->comp, pick->loc );
		if( isite < 0 ) {
			sprintf( Text, "%s.%s.%s.%s - Not in station list.",
					pick->site, pick->comp, pick->net, pick->loc );
			ew2openapi_logit("et", "%s\n", Text);
		} else {
			lat = Site[isite].lat;
			lon = Site[isite].lon;
			// elev = Site[isite].elev;
		}

		jobj = json_object_new_geojson_from_json_object(jobj, lon, lat);
	}

	/* Build JSON string from json object */
	snprintf(json_string, MAX_JSON_STRING_PICKCODA - 1, "%s", json_object_to_json_string_ew2openapi(jobj));

	/* clean the json object */
	json_object_put(jobj);

	return json_string;
}

char *get_json_string_from_coda_scnl_ew_struct(EWCODA *coda, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName) {
	/*Creating a json object*/
	json_object *jobj = json_object_new_object();
	json_object *jobj_ew_message = json_object_new_object();
	json_object *jobj_tmp = NULL;
	int j;

	static char json_string[MAX_JSON_STRING_PICKCODA];

	/*
	   char *msgtype_name;
	   char *modid_name;
	   char *instid_name;

	   msgtype_name = GetTypeName(coda->msgtype);
	   modid_name = GetModIdName(coda->modid);
	   instid_name = GetInstName(coda->instid);
	   */

	json_string[MAX_JSON_STRING_PICKCODA - 1] = 0;

	snprintf(json_string, MAX_JSON_STRING_PICKCODA - 1, "CALL sp_ins_ew_coda_scnl('%s', '%s', %d, '%s', '%s', '%s', '%s', %ld, %ld, %ld, %ld, %ld, %ld, %d);",
			/*
			   msgtype_name,
			   modid_name,
			   instid_name,
			   */
			ewInstanceName,
			GetModIdName(reclogo->mod),
			coda->seq,
			coda->net,
			coda->site,
			coda->comp,
			coda->loc,
			coda->caav[0],
			coda->caav[1],
			coda->caav[2],
			coda->caav[3],
			coda->caav[4],
			coda->caav[5],
			coda->dur
			);

	/* Add Earthworm logo to JSON object */
	json_object_ew_logo_objects_add(jobj, reclogo, ewInstanceName, ewUserName, ewHostName);

	json_object_object_add(jobj_ew_message,"pickId", json_object_new_int(coda->seq));
	json_object_object_add(jobj_ew_message,"station", json_object_new_string(coda->site));
	json_object_object_add(jobj_ew_message,"component", json_object_new_string(coda->comp));
	json_object_object_add(jobj_ew_message,"network", json_object_new_string(coda->net));
	json_object_object_add(jobj_ew_message,"location", json_object_new_string(coda->loc));
	jobj_tmp = json_object_new_array();
	for(j=0; j<6; j++) {
		json_object_array_add(jobj_tmp, json_object_new_int64(coda->caav[j]));
	}
	json_object_object_add(jobj_ew_message,"caav", jobj_tmp);
	json_object_object_add(jobj_ew_message,"dur", json_object_new_int64(coda->dur));

	/* Add Earthworm message to JSON object */
	json_object_ew_message_objects_add(jobj, jobj_ew_message);

	/* Build JSON string from json object */
	snprintf(json_string, MAX_JSON_STRING_PICKCODA - 1, "%s", json_object_to_json_string_ew2openapi(jobj));

	/* clean the json object */
	json_object_put(jobj);

	return json_string;
}


char *get_json_string_from_pick_scnl_ew_msg(char *msg, int msg_size, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName, char flag_geojson) {
	char *ret = NULL;
	char Text[1024];
	EWPICK pick;

	/* Read the pick into an EWPICK struct
	 *  *************************************/
	if( rd_pick_scnl( msg, msg_size, &pick ) != EW_SUCCESS )
	{
		sprintf( Text, "Error reading pick: %s", msg );
		ew2openapi_logit("et", "%s\n", Text);
	} else {
		ret = get_json_string_from_pick_scnl_ew_struct(&pick, reclogo, ewInstanceName, ewUserName, ewHostName, flag_geojson);
	}

	return ret;
}

char *get_json_string_from_coda_scnl_ew_msg(char *msg, int msg_size, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName) {
	char *ret = NULL;
	char Text[1024];
	EWCODA coda;

	/* Read the coda into an EWCODA struct
	 *  *************************************/
	if( rd_coda_scnl( msg, msg_size, &coda ) != EW_SUCCESS )
	{
		sprintf( Text, "Error reading coda: %s", msg );
		ew2openapi_logit("et", "%s\n", Text);
	} else {
		ret = get_json_string_from_coda_scnl_ew_struct(&coda, reclogo, ewInstanceName, ewUserName, ewHostName);
	}

	return ret;
}

