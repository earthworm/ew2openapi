#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <earthworm.h>

#include "ew2openapi_logit.h"
#include "ew2openapi_handler_rabbitmq.h"
#include "ew2openapi_json_utils.h"
#include "ew2openapi_handler_status.h"

/* Private functions */
int amqp_sendstring(char *buf, size_t len, long inkey, MSG_LOGO *logo, TYPE_PARAMS *params, char cur_send_message_format);

int ew2openapi_handler_rabbitmq_init(TYPE_PARAMS *params) {
	int ret = 0;
	/* TODO */
	return ret;
}

int ew2openapi_handler_rabbitmq_destroy(TYPE_PARAMS *params) {
	int ret = 0;
	/* TODO */
	return ret;
}

int amqp_sendstring(char *buf, size_t len, long inkey, MSG_LOGO *logo, TYPE_PARAMS *params, char cur_send_message_format)
{
	int ret = 0;

	char const *hostname;
	int port, status;
	char const *exchange;
#define MAX_MESSAGE_FORMAT_STRING 16
	char send_message_format_string[MAX_MESSAGE_FORMAT_STRING];
#define MAX_ROUTINGKEY_LEN 255
	char routingkey_var[MAX_ROUTINGKEY_LEN];
	char const *routingkey;
	// char const *messagebody;
	char const *vhost;
	char const *username;
	char const *password;
	amqp_socket_t *socket = NULL;
	amqp_connection_state_t conn;
	amqp_bytes_t amqp_message;

	switch(cur_send_message_format) {
		case MESSAGE_FORMAT_RAW:
			snprintf(send_message_format_string, MAX_MESSAGE_FORMAT_STRING-1, "RAW");
			break;
		case MESSAGE_FORMAT_JSON:
			snprintf(send_message_format_string, MAX_MESSAGE_FORMAT_STRING-1, "JSON");
			break;
		case MESSAGE_FORMAT_GEOJSON:
			snprintf(send_message_format_string, MAX_MESSAGE_FORMAT_STRING-1, "GEOJSON");
			break;
		default:
			/* TODO ERROR */
			break;
	}

	snprintf(routingkey_var, MAX_ROUTINGKEY_LEN - MAX_MESSAGE_FORMAT_STRING - 1, "EW.%s.%s.%s.%s.%s.%s", params->ewinstancename, GetKeyName(inkey), GetInstName(logo->instid), GetModIdName(logo->mod), GetTypeName(logo->type), send_message_format_string);

	amqp_message.bytes = buf;
	amqp_message.len = len;

	hostname = params->rmq_hostname;
	port = params->rmq_port;
	exchange = params->rmq_exchange;
	vhost = params->rmq_vhost;
	username = params->rmq_username;
	password = params->rmq_password;

	routingkey = routingkey_var;
	// messagebody = buf;

	conn = amqp_new_connection();

	socket = amqp_tcp_socket_new(conn);
	if (!socket) {
		die("creating TCP socket");
	}

	status = amqp_socket_open(socket, hostname, port);
	if (status) {
		die("opening TCP socket");
	}

	die_on_amqp_error(amqp_login(conn, vhost, 0, 131072, 0, AMQP_SASL_METHOD_PLAIN, username, password),
			"Logging in");
	amqp_channel_open(conn, 1);
	die_on_amqp_error(amqp_get_rpc_reply(conn), "Opening channel");

	{
		amqp_basic_properties_t props;
		props._flags = AMQP_BASIC_CONTENT_TYPE_FLAG | AMQP_BASIC_DELIVERY_MODE_FLAG;
		// props.content_type = amqp_cstring_bytes("text/plain");
		props.content_type = amqp_cstring_bytes("application/octet-stream");
		props.delivery_mode = 2; /* persistent delivery mode */
		die_on_error(amqp_basic_publish(conn,
					1,
					amqp_cstring_bytes(exchange),
					amqp_cstring_bytes(routingkey),
					0,
					0,
					&props,
					// amqp_cstring_bytes(messagebody)
					amqp_message
					),
				"Publishing");
	}

	die_on_amqp_error(amqp_channel_close(conn, 1, AMQP_REPLY_SUCCESS), "Closing channel");
	die_on_amqp_error(amqp_connection_close(conn, AMQP_REPLY_SUCCESS), "Closing connection");
	die_on_error(amqp_destroy_connection(conn), "Ending connection");

	return ret;
}

int ew2openapi_handler_rabbitmq_handle(TYPE_DATA_HANDLER *data_handler) {
	char    *json_string = NULL;
	int     handler_status_code = HANDLER_STATUS_OK;
	char    flag_geojson = 0;
	int     amqp_ret_code = 0;

	/* Convert message to JSON if required */
	json_string = NULL;
	flag_geojson =  0;
	if(
			data_handler->params->send_message_format_flags & MESSAGE_FORMAT_JSON
			||
			data_handler->params->send_message_format_flags & MESSAGE_FORMAT_GEOJSON
	  ) {

		/* Build the JSON string for posting to service */
		flag_geojson =  data_handler->params->send_message_format_flags & MESSAGE_FORMAT_GEOJSON;
		json_string = get_json_string_from_ew_msg(data_handler->Wrmsg, data_handler->msgSize, data_handler->reclogo, data_handler->params->ewinstancename, data_handler->ewUserName, data_handler->ewHostName, &flag_geojson);

	}

	/* Send messages to RabbitMQ */
	if(data_handler->params->send_message_format_flags & MESSAGE_FORMAT_RAW) {
		amqp_ret_code = amqp_sendstring(data_handler->Wrmsg, data_handler->msgSize, data_handler->inkey, data_handler->reclogo, data_handler->params, MESSAGE_FORMAT_RAW);
	}

	if(json_string) {
		amqp_ret_code = amqp_sendstring(json_string, strlen(json_string), data_handler->inkey, data_handler->reclogo, data_handler->params, (flag_geojson)? MESSAGE_FORMAT_GEOJSON : MESSAGE_FORMAT_JSON);
	}

	/* TODO Handle errors from RabbitMQ and set optionally flag_remove_last_msg and flag_wait_after_error */
	switch(amqp_ret_code) {

		default:
			/* Do nothing */
			break;

	}

	return handler_status_code;

}

