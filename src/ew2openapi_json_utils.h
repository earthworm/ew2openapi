#ifndef EW2OPENAPI_JSON_UTILS_H
#define EW2OPENAPI_JSON_UTILS_H 1

#include <json-c/json.h>

#include <transport.h>

const char *json_object_to_json_string_ew2openapi(struct json_object *obj);
struct json_object *json_object_new_string_from_char (char c);
struct json_object* json_object_new_double_with_precision(double d, int precision);

void json_object_ew_logo_objects_add(struct json_object *jobj, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName);
void json_object_ew_message_objects_add(struct json_object *jobj, struct json_object *jobj_ew_message);

char *get_json_string_from_ew_msg(char *Wrmsg, int msgSize, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName, char *flag_geojson);

#endif

