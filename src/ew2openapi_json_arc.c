#include <stdio.h>
#include <json-c/json.h>
#include <string.h>
#include <earthworm.h>
#include <time_ew.h>
#include <chron3.h>
#include <read_arc.h>
#include <site.h>

#include "ew2openapi_logit.h"
#include "ew2openapi_json_arc.h"
#include "ew2openapi_utils.h"
#include "ew2openapi_json_utils.h"
#include "ew2openapi_geojson_utils.h"
#include "ew2openapi_eq_version_name.h"

void ingv_quality_arc_ew_struct(struct Hsum *arcSum, struct Hpck *arcPck[], int n_arcPck, char *ingv_quality1, char *ingv_quality2) {
	double Nobs;
	double Max_wt;
	double error_depth;
	double error_horizontal;
	double rms;
	double gap;
	double dmin;
	double ev_depth;
	int i;

	/* Save maxima values for Pwt and Swt */
	Max_wt = -100.0;
	for(i=0; i < n_arcPck; i++) {
		if(arcPck[i]->Pwt > Max_wt) {
			Max_wt = arcPck[i]->Pwt;
		}
		if(arcPck[i]->Swt > Max_wt) {
			Max_wt = arcPck[i]->Swt;
		}
	}

	/* TODO Nobs */
	//  = arcSum->nph;

	/* Compute Nobs */
	Nobs = 0.0;
	for(i=0; i < n_arcPck; i++) {
		Nobs += (arcPck[i]->Pwt / Max_wt);
		Nobs += (arcPck[i]->Swt / Max_wt);
	}

	error_depth = arcSum->erz;
	error_horizontal = arcSum->erh;
	rms = arcSum->rms;
	gap = arcSum->gap;
	dmin = arcSum->dmin;
	ev_depth = arcSum->z;

	*ingv_quality1 = 'D';
	if(
			Nobs >= 4.0 &&
			rms <= 1.5 &&
			error_depth <= 10.0
	  ) {
		*ingv_quality1 = 'C';
	}
	if(
			Nobs >= 4.0 &&
			rms <= 0.9 &&
			error_horizontal <= 5.0 &&
			error_depth <= 10.0
	  ) {
		*ingv_quality1 = 'B';
	}
	if(
			Nobs >= 4.0 &&
			rms <= 0.45 &&
			error_horizontal <= 2.0 &&
			error_depth <= 4.0
	  ) {
		*ingv_quality1 = 'A';
	}


	*ingv_quality2 = 'D';
	if(
			Nobs >= 6.0 &&
			gap <= 180.0 &&
			dmin <= 100.0
	  ) {
		*ingv_quality2 = 'C';
	}

	if(
			Nobs >= 6.0 &&
			gap <= 135.0 &&
			( dmin <= 20.0 || dmin <= (2.0 * ev_depth))
	  ) {
		*ingv_quality2 = 'B';
	}

	if(
			Nobs >= 6.0 &&
			gap <= 90.0 &&
			( dmin <= 10.0 || dmin <= ev_depth)
	  ) {
		*ingv_quality2 = 'A';
	}

}

char *get_json_string_from_arc_ew_struct(struct Hsum *arcSum, struct Hpck *arcPck[], int n_arcPck, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName, char flag_geojson) {
	/*Creating a json object*/
	json_object *jobj = json_object_new_object();
	json_object *jobj_phase = NULL;
	json_object *jobj_phase_array = json_object_new_array();
	json_object *jobj_multiline = json_object_new_geojson_multilinestring();
	json_object *jobj_multiline_content = NULL;
	json_object *jobj_tmp = NULL;
	json_object *jobj_ew_message = json_object_new_object();

	/* Only for GeoJSON conversion */
	json_object *jfeatures_array = json_object_new_array();
	int isite;
	double lat = 0.0, lon = 0.0, elev = 0.0;
	char Text[1024];

	static char json_string[MAX_JSON_STRING_ARC];
	char json_string_tmp[MAX_JSON_STRING_ARC];
	char myjson_string_datetime_Pat[EW2OPENAPI_DATETIME_MAXLEN_STR];
	char myjson_string_datetime_Sat[EW2OPENAPI_DATETIME_MAXLEN_STR];
	char myjson_string_datetime_cdate[EW2OPENAPI_DATETIME_MAXLEN_STR];
	char ingv_quality1, ingv_quality2;
	char ingv_quality_str[4];

	int i;
	int j;

	json_string[MAX_JSON_STRING_ARC - 1] = 0;
	json_string_tmp[MAX_JSON_STRING_ARC - 1] = 0;

	ingv_quality_arc_ew_struct(arcSum, arcPck, n_arcPck, &ingv_quality1, &ingv_quality2);
	snprintf(ingv_quality_str, 4, "%c%c", ingv_quality1, ingv_quality2);

	timestr_to_datetime_str_iso8601_utc(myjson_string_datetime_cdate, arcSum->cdate);

	snprintf(json_string, MAX_JSON_STRING_ARC - 1, "CALL sp_ins_ew_arc_summary('%s', '%s', %ld, '%s', %f, %f, %f, %d, %d, %d, %d, %d, %d, %f, %d, %d, %f, %d, %d, %f, %f, %f, %f, %f, '%s',  '%c', %f, %f, '%c', %f, %f, %ld, '%c%c');",
			ewInstanceName,
			GetModIdName(reclogo->mod),
			arcSum->qid,
			/*
			   arcSum->ot,
			   */
			myjson_string_datetime_cdate,
			arcSum->lat,
			arcSum->lon,
			arcSum->z,
			arcSum->nph,
			arcSum->nphS,
			arcSum->nphtot,
			arcSum->nPfm,
			arcSum->gap,
			arcSum->dmin,
			arcSum->rms,
			arcSum->e0az,
			arcSum->e0dp,
			arcSum->e0,
			arcSum->e1az,
			arcSum->e1dp,
			arcSum->e1,
			arcSum->e2,
			arcSum->erh,
			arcSum->erz,
			arcSum->Md,
			arcSum->reg,
			/*
			   arcSum->cdate,
			   */
			arcSum->labelpref,
			arcSum->Mpref,
			arcSum->wtpref,
			arcSum->mdtype,
			arcSum->mdmad,
			arcSum->mdwt,
			arcSum->version,
			ingv_quality1,
			ingv_quality2
				);

	/* Add Earthworm logo to JSON object */
	json_object_ew_logo_objects_add(jobj, reclogo, ewInstanceName, ewUserName, ewHostName);

	json_object_object_add(jobj_ew_message,"quakeId", json_object_new_int64(arcSum->qid));
	// json_object_object_add(jobj_ew_message,"version", json_object_new_int64(arcSum->version));
	json_object_object_add(jobj_ew_message,"version", json_object_new_string(ew2openapi_get_name_version(arcSum->version)));
	// https://gitlab.rm.ingv.it/earthworm/ew2openapi/-/issues/53
	// Set originId using quakeId and version
	json_object_object_add(jobj_ew_message,"originId", json_object_new_int64((arcSum->qid * 10) + arcSum->version));
	json_object_object_add(jobj_ew_message,"originTime", json_object_new_string(myjson_string_datetime_cdate));
	json_object_object_add(jobj_ew_message,"latitude", json_object_new_double_with_precision(arcSum->lat, 6));
	json_object_object_add(jobj_ew_message,"longitude", json_object_new_double_with_precision(arcSum->lon, 6));
	json_object_object_add(jobj_ew_message,"depth", json_object_new_double_with_precision(arcSum->z, 2));
	json_object_object_add(jobj_ew_message,"nph", json_object_new_int(arcSum->nph));
	json_object_object_add(jobj_ew_message,"nphS", json_object_new_int(arcSum->nphS));
	json_object_object_add(jobj_ew_message,"nphtot", json_object_new_int(arcSum->nphtot));
	json_object_object_add(jobj_ew_message,"nPfm", json_object_new_int(arcSum->nPfm));
	json_object_object_add(jobj_ew_message,"gap", json_object_new_int(arcSum->gap));
	json_object_object_add(jobj_ew_message,"dmin", json_object_new_int(arcSum->dmin));
	json_object_object_add(jobj_ew_message,"rms", json_object_new_double_with_precision(arcSum->rms, 2));
	json_object_object_add(jobj_ew_message,"e0az", json_object_new_int(arcSum->e0az));
	json_object_object_add(jobj_ew_message,"e0dp", json_object_new_int(arcSum->e0dp));
	json_object_object_add(jobj_ew_message,"e0", json_object_new_double_with_precision(arcSum->e0, 2));
	json_object_object_add(jobj_ew_message,"e1az", json_object_new_int(arcSum->e1az));
	json_object_object_add(jobj_ew_message,"e1dp", json_object_new_int(arcSum->e1dp));
	json_object_object_add(jobj_ew_message,"e1", json_object_new_double_with_precision(arcSum->e1, 2));
	json_object_object_add(jobj_ew_message,"e2", json_object_new_double_with_precision(arcSum->e2, 2));
	json_object_object_add(jobj_ew_message,"erh", json_object_new_double_with_precision(arcSum->erh, 2));
	json_object_object_add(jobj_ew_message,"erz", json_object_new_double_with_precision(arcSum->erz, 2));
	json_object_object_add(jobj_ew_message,"Md", json_object_new_double_with_precision(arcSum->Md, 2));
	json_object_object_add(jobj_ew_message,"reg", json_object_new_string(arcSum->reg));
	json_object_object_add(jobj_ew_message,"labelpref", json_object_new_string_from_char(arcSum->labelpref));
	json_object_object_add(jobj_ew_message,"Mpref", json_object_new_double_with_precision(arcSum->Mpref, 2));
	json_object_object_add(jobj_ew_message,"wtpref", json_object_new_double_with_precision(arcSum->wtpref,2 ));
	json_object_object_add(jobj_ew_message,"mdtype", json_object_new_string_from_char(arcSum->mdtype));
	json_object_object_add(jobj_ew_message,"mdmad", json_object_new_double_with_precision(arcSum->mdmad, 2));
	json_object_object_add(jobj_ew_message,"mdwt", json_object_new_double_with_precision(arcSum->mdwt, 2));
	json_object_object_add(jobj_ew_message,"ingvQuality", json_object_new_string(ingv_quality_str));

	/* Extra fields */
	json_object_object_add(jobj_ew_message,"amplitudeMagnitude", json_object_new_double_with_precision(arcSum->amplitudeMagnitude, 2));
	json_object_object_add(jobj_ew_message,"numberOfAmpMagWeightCode", json_object_new_double_with_precision(arcSum->numberOfAmpMagWeightCode, 1));
	json_object_object_add(jobj_ew_message,"medianAbsDiffAmpMag", json_object_new_double_with_precision(arcSum->medianAbsDiffAmpMag, 2));
	json_object_object_add(jobj_ew_message,"preferredMagLabel", json_object_new_string_from_char(arcSum->labelpref));
	json_object_object_add(jobj_ew_message,"preferredMag", json_object_new_double_with_precision(arcSum->Mpref, 2));
	json_object_object_add(jobj_ew_message,"numberOfPreferredMags", json_object_new_double_with_precision(arcSum->wtpref, 1));


	/* GeoJSON conversion */
	if(flag_geojson) {
		json_object_ew_message_objects_add(jobj, jobj_ew_message);

		json_object_array_add(jfeatures_array, json_object_new_geojson_feature_point3(jobj, arcSum->lon, arcSum->lat, - arcSum->z));
	}

	for(i=0; i < n_arcPck; i++) {

		jobj_phase = json_object_new_object();

		epoch_to_datetime_str_iso8601_utc(myjson_string_datetime_Pat, EW2OPENAPI_DATETIME_MAXLEN_STR, arcPck[i]->Pat - GSEC1970);
		epoch_to_datetime_str_iso8601_utc(myjson_string_datetime_Sat, EW2OPENAPI_DATETIME_MAXLEN_STR, arcPck[i]->Sat - GSEC1970);
		timestr_to_datetime_str_iso8601_utc(myjson_string_datetime_cdate, arcPck[i]->cdate);

		snprintf(json_string_tmp, MAX_JSON_STRING_ARC - 1, "CALL sp_ins_ew_arc_phase('%s', '%s', %ld, '%s', '%s', '%s', '%s', '%c', '%c', '%c', '%c', '%s', '%s', %f, %f, %d, %d, %d, %d, '%c', '%c', '%c', %f, %d, %d, %f, %f, %f, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %ld);",
				ewInstanceName,
				GetModIdName(reclogo->mod),
				arcSum->qid,
				arcPck[i]->site,
				arcPck[i]->net,
				arcPck[i]->comp,
				arcPck[i]->loc,
				arcPck[i]->Plabel,
				arcPck[i]->Slabel,
				arcPck[i]->Ponset,
				arcPck[i]->Sonset,
				/*
				   arcPck[i]->Pat,
				   arcPck[i]->Sat,
				   */
				myjson_string_datetime_Pat,
				myjson_string_datetime_Sat,
				arcPck[i]->Pres,
				arcPck[i]->Sres,
				arcPck[i]->Pqual,
				arcPck[i]->Squal,
				arcPck[i]->codalen,
				arcPck[i]->codawt,
				arcPck[i]->Pfm,
				arcPck[i]->Sfm,
				/*
				   arcPck[i]->cdate,
				   myjson_string_datetime_cdate,
				   */
				arcPck[i]->datasrc,
				arcPck[i]->Md,
				arcPck[i]->azm,
				arcPck[i]->takeoff,
				arcPck[i]->dist,
				arcPck[i]->Pwt,
				arcPck[i]->Swt,
				arcPck[i]->pamp,
				arcPck[i]->codalenObs,
				arcPck[i]->ccntr[0],
				arcPck[i]->ccntr[1],
				arcPck[i]->ccntr[2],
				arcPck[i]->ccntr[3],
				arcPck[i]->ccntr[4],
				arcPck[i]->ccntr[5],
				arcPck[i]->caav[0],
				arcPck[i]->caav[1],
				arcPck[i]->caav[2],
				arcPck[i]->caav[3],
				arcPck[i]->caav[4],
				arcPck[i]->caav[5],
				arcSum->version
			);

		/* Add fields in json object phase */
		json_object_object_add(jobj_phase,"sta", json_object_new_string(arcPck[i]->site));
		json_object_object_add(jobj_phase,"comp", json_object_new_string(arcPck[i]->comp));
		json_object_object_add(jobj_phase,"net", json_object_new_string(arcPck[i]->net));
		json_object_object_add(jobj_phase,"loc", json_object_new_string(arcPck[i]->loc));
		json_object_object_add(jobj_phase,"Plabel", json_object_new_string_from_char(arcPck[i]->Plabel));
		json_object_object_add(jobj_phase,"Slabel", json_object_new_string_from_char(arcPck[i]->Slabel));
		json_object_object_add(jobj_phase,"Ponset", json_object_new_string_from_char(arcPck[i]->Ponset));
		json_object_object_add(jobj_phase,"Sonset", json_object_new_string_from_char(arcPck[i]->Sonset));
		json_object_object_add(jobj_phase,"Pat", json_object_new_string(myjson_string_datetime_Pat));
		json_object_object_add(jobj_phase,"Sat", json_object_new_string(myjson_string_datetime_Sat));
		json_object_object_add(jobj_phase,"Pres", json_object_new_double_with_precision(arcPck[i]->Pres, 2));
		json_object_object_add(jobj_phase,"Sres", json_object_new_double_with_precision(arcPck[i]->Sres, 2));
		json_object_object_add(jobj_phase,"Pqual", json_object_new_int(arcPck[i]->Pqual));
		json_object_object_add(jobj_phase,"Squal", json_object_new_int(arcPck[i]->Squal));
		json_object_object_add(jobj_phase,"codalen", json_object_new_int(arcPck[i]->codalen));
		json_object_object_add(jobj_phase,"codawt", json_object_new_int(arcPck[i]->codawt));
		json_object_object_add(jobj_phase,"Pfm", json_object_new_string_from_char(arcPck[i]->Pfm));
		json_object_object_add(jobj_phase,"Sfm", json_object_new_string_from_char(arcPck[i]->Sfm));
		json_object_object_add(jobj_phase,"datasrc", json_object_new_string_from_char(arcPck[i]->datasrc));
		json_object_object_add(jobj_phase,"Md", json_object_new_double_with_precision(arcPck[i]->Md, 2));
		json_object_object_add(jobj_phase,"azm", json_object_new_int(arcPck[i]->azm));
		json_object_object_add(jobj_phase,"takeoff", json_object_new_int(arcPck[i]->takeoff));
		json_object_object_add(jobj_phase,"dist", json_object_new_double_with_precision(arcPck[i]->dist, 2));
		json_object_object_add(jobj_phase,"Pwt", json_object_new_double_with_precision(arcPck[i]->Pwt, 2));
		json_object_object_add(jobj_phase,"Swt", json_object_new_double_with_precision(arcPck[i]->Swt, 2));
		json_object_object_add(jobj_phase,"pamp", json_object_new_int(arcPck[i]->pamp));
		json_object_object_add(jobj_phase,"codalenObs", json_object_new_int(arcPck[i]->codalenObs));

		jobj_tmp = json_object_new_array();
		for(j=0; j<6; j++) {
			json_object_array_add(jobj_tmp, json_object_new_int(arcPck[i]->ccntr[j]));
		}
		json_object_object_add(jobj_phase,"ccntr", jobj_tmp);

		jobj_tmp = json_object_new_array();
		for(j=0; j<6; j++) {
			json_object_array_add(jobj_tmp, json_object_new_int(arcPck[i]->caav[j]));
		}
		json_object_object_add(jobj_phase,"caav", jobj_tmp);

		/* Extra fields */
		json_object_object_add(jobj_phase,"amplitude", json_object_new_double_with_precision(arcPck[i]->amplitude, 2));
		json_object_object_add(jobj_phase,"ampUnitsCode", json_object_new_int(arcPck[i]->ampUnitsCode));
		json_object_object_add(jobj_phase,"ampType", json_object_new_int(arcPck[i]->ampType));
		json_object_object_add(jobj_phase,"ampMag", json_object_new_double_with_precision(arcPck[i]->ampMag, 2));
		json_object_object_add(jobj_phase,"ampMagWeightCode", json_object_new_int(arcPck[i]->ampMagWeightCode));
		json_object_object_add(jobj_phase,"importanceP", json_object_new_double_with_precision(arcPck[i]->importanceP, 3));
		json_object_object_add(jobj_phase,"importanceS", json_object_new_double_with_precision(arcPck[i]->importanceS, 3));

		/* GeoJSON conversion */
		if(flag_geojson) {
			isite = site_index( arcPck[i]->site, arcPck[i]->net, arcPck[i]->comp, arcPck[i]->loc );
			if( isite < 0 ) {
				sprintf( Text, "%s.%s.%s.%s - Not in station list.",
						arcPck[i]->site, arcPck[i]->comp, arcPck[i]->net, arcPck[i]->loc );
				ew2openapi_logit("et", "%s\n", Text);
			} else {
				lat = Site[isite].lat;
				lon = Site[isite].lon;
				elev = Site[isite].elev;
			}

			/* point */
			json_object_array_add(jfeatures_array, json_object_new_geojson_feature_point3(jobj_phase, lon, lat, elev));

			/* linestring */
			json_object_new_geojson_multilinestring_add(jobj_multiline, arcSum->lon, arcSum->lat, lon, lat);

		} else {
			/* Add json object phase into json object phase array */
			json_object_array_add(jobj_phase_array, jobj_phase);
		}

	}


	/* GeoJSON conversion */
	if(flag_geojson) {
		/* Add links from hypocenter to station */
		jobj_multiline_content = json_object_new_object();
		json_object_object_add(jobj_multiline_content, "link", json_object_new_string(""));
		json_object_array_add(jfeatures_array, json_object_new_geojson_feature_multilinestring(jobj_multiline_content, jobj_multiline));

		/* Create GeoJSON as FeatureCollection array */
		jobj = json_object_new_geojson_feature_collection(jfeatures_array);
	} else {
		/* Add phase to the Earthworm message JSON object */
		json_object_object_add(jobj_ew_message,"phases", jobj_phase_array);

		/* Add Earthworm message to JSON object */
		json_object_ew_message_objects_add(jobj, jobj_ew_message);
	}

	/* Build JSON string from json object */
	snprintf(json_string, MAX_JSON_STRING_ARC - 1, "%s", json_object_to_json_string_ew2openapi(jobj));

	if (strlen(json_string) + 1 > MAX_JSON_STRING_ARC - 1) {
		ew2openapi_logit("et", "json_string for arc messge will be truncated.\n");
	}

	/* clean the json object */
	json_object_put(jobj);

	return json_string;
}

char *get_json_string_from_arc_ew_msg(char *msg, int msg_size, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName, char flag_geojson) {
	char *ret = NULL;
	int i;
	HypoArc arc;

	arc.sum.qid = -1;
	arc.num_phases = 0;
	if( parse_arc( msg, &arc) != 0 || arc.sum.qid == -1 )
	{
		/* Error */
		ew2openapi_logit( "et" , "Error parsing arc file. Exit.\n" );
		exit(1);
	}

	ret = get_json_string_from_arc_ew_struct(&(arc.sum), arc.phases, arc.num_phases, reclogo, ewInstanceName, ewUserName, ewHostName, flag_geojson);

	for(i=0; i < arc.num_phases; i++) {
		if(arc.phases[i]) {
			free(arc.phases[i]);
		}
	}

	return ret;
}

