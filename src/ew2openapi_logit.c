#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <earthworm.h>

#include "ew2openapi_logit.h"

enum EW2OPENAPI_RUN_MODE ew2openapi_run_mode = RUN_MODE_AS_EW_MODULE;

void ew2openapi_set_run_mode( enum EW2OPENAPI_RUN_MODE flag_run_mode ) {
	ew2openapi_run_mode = flag_run_mode;
}

void ew2openapi_logit_init( const char *prog, short mid, int bufSize, int logflag )
{
	const char *EW_PARAMS_DEFAULT_DIR = "./"; /* If it is not set, default EW_PARAMS is the current working directory */
	const char *paramdir;    /* points to environment variable, EW_PARAMS      */

	/* if EW_PARAMS is not set, set default which must contain at least earthworm.d and earthworm_global.d files */
	paramdir = getenv( "EW_PARAMS" );
	if ( paramdir == (char *)NULL )
	{
		fprintf(stderr, "WARNING: EW_PARAMS not set! Now trying '%s' to find earthworm.d and earthworm_global.d files.\n", EW_PARAMS_DEFAULT_DIR);
		/* set EW_PARAMS to default working directory */
		if(setenv("EW_PARAMS", EW_PARAMS_DEFAULT_DIR, 1) == 0) {
			/* OK */
		} else {
			/* ERROR in ernno */
		}
		paramdir = getenv( "EW_PARAMS" );
	}

	fprintf(stderr, "EW_PARAMS is '%s'\n", paramdir);

	if(ew2openapi_run_mode == RUN_MODE_AS_EW_MODULE) {
		logit_init(prog, mid, bufSize, logflag);
	} else {
		/* Set logit_init() to not log */
		/* fprintf(stderr, "ew2openapi_logit_init() set to no log.\n"); */
		logit_init(prog, mid, bufSize, 0);

	}
}

void ew2openapi_logit( const char *flag, const char *format, ... )
{
	auto va_list ap;

	va_start( ap, format );
	if(ew2openapi_run_mode == RUN_MODE_AS_EW_MODULE) {
		logit_core(flag,format,ap);
	} else {
		int    flag_stdout      = 0;      /* 1 if output is also to stdout   */
		int    flag_stderr      = 0;      /* 1 if output is also to stderr   */
		// int    flag_time_stamp = 0;      /* 1 if output is time-stamped     */
		// int    flag_pid_stamp  = 0;      /* 1 if output is pid-stamped      */
		static const char *fl;

		/* Check flag argument
		 *******************/
		fl = flag;
		while ( *fl != '\0' )
		{
			if ( *fl == 'o' ) flag_stdout      = 1;
			if ( *fl == 'e' ) flag_stderr      = 1;
			// if ( *fl == 't' ) flag_time_stamp = 1;
			// if ( *fl == 'd' ) flag_pid_stamp  = 1;
			fl++;
		}

		/* If nor flag_stdout neither flag_stderr are set then set flag_stdout */
		if( !flag_stdout && !flag_stderr) {
			flag_stdout = 1;
		}

		if(flag_stdout) {
			vfprintf(stdout, format, ap);
		}

		if(flag_stderr) {
			vfprintf(stdout, format, ap);
		}

	}
	va_end( ap );
}


