#ifndef EW2OPENAPI_GEOJSON_UTILS_H
#define EW2OPENAPI_GEOJSON_UTILS_H 1

struct json_object *json_object_new_geojson_position (double x, double y);
struct json_object *json_object_new_geojson_position3 (double x, double y, double z);
struct json_object *json_object_new_geojson_linestring (double x1, double y1, double x2, double y2);
struct json_object *json_object_new_geojson_multilinestring ();
int json_object_new_geojson_multilinestring_add (json_object *jobj_multilinestring, double x1, double y1, double x2, double y2);

struct json_object *json_object_new_geojson_geometry_point (double x, double y);
struct json_object *json_object_new_geojson_geometry_point3 (double x, double y, double z);
struct json_object *json_object_new_geojson_geometry_linestring (double x1, double y1, double x2, double y2);
struct json_object *json_object_new_geojson_geometry_multilinestring (json_object *jobj_multilinestring);

struct json_object *json_object_new_geojson_feature_point (struct json_object *jobj_properties_content, double x, double y);
struct json_object *json_object_new_geojson_feature_point3 (struct json_object *jobj_properties_content, double x, double y, double z);
struct json_object *json_object_new_geojson_feature_linestring (struct json_object *jobj_properties_content, double x1, double y1, double x2, double y2);
struct json_object *json_object_new_geojson_feature_multilinestring (struct json_object *jobj_properties_content, struct json_object *jobj_multilinestring);

struct json_object *json_object_new_geojson_feature_collection(struct json_object *jfeatures_array);

struct json_object *json_object_new_geojson_from_json_object(struct json_object *jobj, double x, double y);
struct json_object *json_object_new_geojson_from_json_object3(struct json_object *jobj, double x, double y, double z);

#endif

