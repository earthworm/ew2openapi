#include <json-c/json.h>

#include "ew2openapi_geojson_utils.h"
#include "ew2openapi_json_utils.h"

#define GEO_COORDINATES_PRECISION 5

struct json_object *json_object_new_geojson_position (double x, double y) {
	json_object *jcoordinates = json_object_new_array();

	json_object_array_add(jcoordinates,json_object_new_double_with_precision(x, GEO_COORDINATES_PRECISION));
	json_object_array_add(jcoordinates,json_object_new_double_with_precision(y, GEO_COORDINATES_PRECISION));

	return jcoordinates;
}

struct json_object *json_object_new_geojson_position3 (double x, double y, double z) {
	json_object *jcoordinates = json_object_new_array();

	json_object_array_add(jcoordinates,json_object_new_double_with_precision(x, GEO_COORDINATES_PRECISION));
	json_object_array_add(jcoordinates,json_object_new_double_with_precision(y, GEO_COORDINATES_PRECISION));
	json_object_array_add(jcoordinates,json_object_new_double_with_precision(z, GEO_COORDINATES_PRECISION));

	return jcoordinates;
}

struct json_object *json_object_new_geojson_linestring (double x1, double y1, double x2, double y2) {
	json_object *jcoordinates = json_object_new_array();

	json_object_array_add(jcoordinates,json_object_new_geojson_position(x1, y1));
	json_object_array_add(jcoordinates,json_object_new_geojson_position(x2, y2));

	return jcoordinates;
}

struct json_object *json_object_new_geojson_multilinestring () {
	json_object *jcoordinates = json_object_new_array();
	return jcoordinates;
}

int json_object_new_geojson_multilinestring_add (json_object *jobj_multilinestring, double x1, double y1, double x2, double y2) {
	return json_object_array_add(jobj_multilinestring, json_object_new_geojson_linestring(x1, y1, x2, y2));
}


struct json_object *json_object_new_geojson_geometry_point (double x, double y) {
	json_object * jobj_content = json_object_new_object();

	json_object_object_add(jobj_content,"type",        json_object_new_string("Point"));
	json_object_object_add(jobj_content,"coordinates", json_object_new_geojson_position(x, y));

	return  jobj_content;
}

struct json_object *json_object_new_geojson_geometry_point3 (double x, double y, double z) {
	json_object * jobj_content = json_object_new_object();

	json_object_object_add(jobj_content,"type",        json_object_new_string("Point"));
	json_object_object_add(jobj_content,"coordinates", json_object_new_geojson_position3(x, y, z));

	return  jobj_content;
}

struct json_object *json_object_new_geojson_geometry_linestring (double x1, double y1, double x2, double y2) {
	json_object * jobj_content = json_object_new_object();

	json_object_object_add(jobj_content,"type",        json_object_new_string("LineString"));
	json_object_object_add(jobj_content,"coordinates", json_object_new_geojson_linestring(x1, y1, x2, y2));

	return  jobj_content;
}

struct json_object *json_object_new_geojson_geometry_multilinestring (json_object *jobj_multilinestring) {
	json_object * jobj_content = json_object_new_object();

	json_object_object_add(jobj_content,"type",        json_object_new_string("MultiLineString"));
	json_object_object_add(jobj_content,"coordinates", jobj_multilinestring);

	return  jobj_content;
}

struct json_object *json_object_new_geojson_feature_point (struct json_object *jobj_properties_content, double x, double y) {
	json_object * jobj_ret = json_object_new_object();

	json_object_object_add(jobj_ret,"type",       json_object_new_string("Feature"));
	json_object_object_add(jobj_ret,"geometry",   json_object_new_geojson_geometry_point(x, y));
	json_object_object_add(jobj_ret,"properties", jobj_properties_content);

	return jobj_ret;
}

struct json_object *json_object_new_geojson_feature_point3 (struct json_object *jobj_properties_content, double x, double y, double z) {
	json_object * jobj_ret = json_object_new_object();

	json_object_object_add(jobj_ret,"type",       json_object_new_string("Feature"));
	json_object_object_add(jobj_ret,"geometry",   json_object_new_geojson_geometry_point3(x, y, z));
	json_object_object_add(jobj_ret,"properties", jobj_properties_content);

	return jobj_ret;
}

struct json_object *json_object_new_geojson_feature_linestring (struct json_object *jobj_properties_content, double x1, double y1, double x2, double y2) {
	json_object * jobj_ret = json_object_new_object();

	json_object_object_add(jobj_ret,"type",       json_object_new_string("Feature"));
	json_object_object_add(jobj_ret,"geometry",   json_object_new_geojson_geometry_linestring(x1, y1, x2, y2));
	json_object_object_add(jobj_ret,"properties", jobj_properties_content);

	return jobj_ret;
}

struct json_object *json_object_new_geojson_feature_multilinestring (struct json_object *jobj_properties_content, struct json_object *jobj_multilinestring) {
	json_object * jobj_ret = json_object_new_object();

	json_object_object_add(jobj_ret,"type",       json_object_new_string("Feature"));
	json_object_object_add(jobj_ret,"geometry",   json_object_new_geojson_geometry_multilinestring(jobj_multilinestring));
	json_object_object_add(jobj_ret,"properties", jobj_properties_content);

	return jobj_ret;
}

struct json_object *json_object_new_geojson_feature_collection(struct json_object *jfeatures_array) {
	json_object * jobj_ret = json_object_new_object();

	json_object_object_add(jobj_ret,"type",       json_object_new_string("FeatureCollection"));
	json_object_object_add(jobj_ret,"features",   jfeatures_array);

	return jobj_ret;
}

struct json_object *json_object_new_geojson_from_json_object(struct json_object *jobj, double x, double y) {
	json_object *jfeatures_array = json_object_new_array();
	json_object *gjobj = NULL;

	json_object_array_add(jfeatures_array, json_object_new_geojson_feature_point(jobj, x, y));
	gjobj = json_object_new_geojson_feature_collection(jfeatures_array);

	return gjobj;
}

struct json_object *json_object_new_geojson_from_json_object3(struct json_object *jobj, double x, double y, double z) {
	json_object *jfeatures_array = json_object_new_array();
	json_object *gjobj = NULL;

	json_object_array_add(jfeatures_array, json_object_new_geojson_feature_point3(jobj, x, y, z));
	gjobj = json_object_new_geojson_feature_collection(jfeatures_array);

	return gjobj;
}
