#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifndef WIN32
#include <unistd.h>
#endif

#include <earthworm.h>

#include "ew2openapi_logit.h"
#include "ew2openapi_json_utils.h"
#include "ew2openapi_handler_convtool.h"
#include "ew2openapi_handler_status.h"

int ew2openapi_handler_convtool_init(TYPE_PARAMS *params) {
	int ret = 0;

	/* TODO */

	return ret;
}

int ew2openapi_handler_convtool_destroy(TYPE_PARAMS *params) {
	int ret = 0;

	/* TODO */

	return ret;
}

int ew2openapi_handler_convtool_handle(TYPE_DATA_HANDLER *data_handler) {
	int     handler_status_code = HANDLER_STATUS_OK;
	char    *json_string = NULL;
	FILE    *fout = NULL;
	/* TODO implement flag_geojson in argument */
	char    flag_geojson = 0;

	/* TODO */

	if(strcmp(data_handler->params->fnameout, "-") == 0) {
		fout = stdout;
	} else {
		fout = fopen(data_handler->params->fnameout, "w");
	}
	if(fout == NULL) {
		ew2openapi_logit("et", "Error opening output file %s. Exit.\n", data_handler->params->fnameout);
		return 1;
	}

	json_string = get_json_string_from_ew_msg(data_handler->Wrmsg, data_handler->msgSize, data_handler->reclogo, data_handler->params->ewinstancename, data_handler->ewUserName, data_handler->ewHostName, &flag_geojson);

	if(json_string == NULL) {
		ew2openapi_logit("et", "Error converting message. Exit.\n");
		return 1;
	} else {
		fprintf( fout, "%s", json_string);
		if(fout != stdout) fclose(fout);
		ew2openapi_logit("et", "Input: %s. Output: %s. Input file size: %ld. Output file size: %ld.\n", data_handler->params->fnamein, data_handler->params->fnameout, data_handler->msgSize, strlen(json_string));
	}

	return handler_status_code;
}

/************* Run as Earthworm messages conversion tool ***************
 **********************************************************************/
int earthworm_message_conversion_tool(TYPE_PARAMS *params, int (* p_data_handler_func_handle)(TYPE_DATA_HANDLER *))
{
	int ret = 0;
	unsigned char TYPE_MSG;
	char    *Wrmsg = NULL;
	long     msgSize = 0;
	MSG_LOGO   reclogo;

	FILE *fin = NULL;

	int     handler_status_code = HANDLER_STATUS_OK;

	char    buffer[128];
	int     nread = 0;

	TYPE_DATA_HANDLER data_handler;

	/* TODO get username Cross-Platform */
	char ewUserName[MAX_LENGTH_STRING] = "ew2openapi";

	/* Default instance name */
	char ewInstanceName[MAX_LENGTH_STRING] = "ew2openapi_convtool";

	char ewHostName[MAX_LENGTH_STRING] = "UNKNOWN";
	if(getsysname_ew(ewHostName, MAX_LENGTH_STRING) != 0) {
		/* error */
	}

	/* get instance name from configfile or default */
	if(params->ewinstancename[0] == 0) {
		strncpy(params->ewinstancename, ewInstanceName, 255);
	}

	/* Check input from file or stdin */
	if(strcmp(params->fnamein, "-") == 0) {
		fin = stdin;
	} else {
		fin = fopen(params->fnamein, "r");
	}
	if(fin == NULL) {
		ew2openapi_logit("et", "Error opening input file %s. Exit.\n", params->fnamein);
		return 1;
	}

	/* Read message */
	Wrmsg = malloc(1);
	if(Wrmsg == NULL) {
		ew2openapi_logit("et", "Error allocating Wrmsg memory. Exit.\n");
		return 1;
	}
	msgSize = 0;
	while ((nread = fread(buffer, 1, sizeof(buffer), fin)) > 0) {
		Wrmsg = realloc(Wrmsg, msgSize + nread + 1);
		if(Wrmsg == NULL) {
			ew2openapi_logit("et", "Error reallocating Wrmsg memory. Exit.\n");
			return 1;
		} else {
			memcpy(Wrmsg + msgSize, buffer, nread);
			msgSize += nread;
		}
	}
	Wrmsg[msgSize] = 0;
	if(fin != stdin) fclose(fin);

	if ( GetType(params->TYPE_MSG_ARG, &TYPE_MSG)  != 0 ) {
		/* GetType already print error message */
		/* ew2openapi_logit("et", "Error: type \"%s\" not found. Exit.\n" ); */
	} else {
		reclogo.instid = 0;
		if ( GetModId("MOD_EW2OPENAPI", &(reclogo.mod) ) != 0 ) {
			reclogo.mod = 0;
		}
		reclogo.type = TYPE_MSG;


		/* Set values in data_handler */
		data_handler.Wrmsg = Wrmsg;
		data_handler.msgSize = msgSize;

		/* TODO inkey initialization */
		data_handler.inkey = 0;

		data_handler.reclogo = &reclogo;
		data_handler.params = params;
		data_handler.ewUserName = ewUserName;
		data_handler.ewHostName = ewHostName;

		/* Call handler function passing data_handler variable */
		handler_status_code = p_data_handler_func_handle(&data_handler);

		switch(handler_status_code) {

			case HANDLER_STATUS_OK:
				/* Do nothing. Leave default values */
				break;

			case HANDLER_STATUS_ERROR_INFINITY_RETRIES:
				/* Do not remove item from queue, wait a while */
				/*
				 * flag_remove_last_msg  = 0;
				 * flag_wait_after_error = 1;
				 */
				break;

			case HANDLER_STATUS_ERROR_LIMITED_RETRIES_WITHIN_TIME:
				/* Compute time difference between now and inserted message time.
				 * If it exceeds max_difftime_seconds, then the message will be discarded. */

				/*
				 * time(&time_now); difftime_seconds = difftime(time_now, time_msg);
				 * if(difftime_seconds < max_difftime_seconds) {
				 *     [> Do not remove item from queue, wait until max_difftime_seconds <]
				 *     flag_remove_last_msg  = 0;
				 *     flag_wait_after_error = 1;
				 * }
				 */
				break;

			case HANDLER_STATUS_ERROR_LIMITED_RETRIES:
				/* Count how many times occurred the error to POST
				 * the same message before giving up after a maximum
				 * number of attempts */

				/*
				 * if(status_count_attempts < max_status_count_attempts) {
				 *     [> Do not remove item from queue, wait a while, count attempt <]
				 *     flag_remove_last_msg  = 0;
				 *     flag_wait_after_error = 1;
				 *     status_count_attempts++;
				 * } else {
				 *     [> Remove item from queue, reset count attempt <]
				 *     status_count_attempts = 0;
				 * }
				 */

				break;

			case HANDLER_STATUS_ERROR_NO_RETRIES:
				/* Do nothing. Leave default values */
				break;

			default:
				/* ERROR */
				ew2openapi_logit( "et", "%s(%s): error handler_status_code value %d not valid; exiting!\n",
						params->cmdname, params->MyModName, handler_status_code);
				/* exit( -1 ); */
				ret = -1;
				break;
		}

		if(Wrmsg) {
			free(Wrmsg);
			Wrmsg = NULL;
		}

	}

	return ret;
}

