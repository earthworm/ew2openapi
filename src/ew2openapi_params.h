#ifndef EW2OPENAPI_PARAMS_H
#define EW2OPENAPI_PARAMS_H 1

/* Flag values for selecting handlers */
#define FLAG_HANDLER_NONE        0
#define FLAG_HANDLER_QUAKEDB_WS  1
#define FLAG_HANDLER_KAFKA       2
#define FLAG_HANDLER_RABBITMQ    3
#define FLAG_HANDLER_OSC         4
#define FLAG_HANDLER_CONVTOOL    5

#define   MESSAGE_FORMAT_UNDEF     0
#define   MESSAGE_FORMAT_RAW       (1<<0)
#define   MESSAGE_FORMAT_JSON      (1<<1)
#define   MESSAGE_FORMAT_GEOJSON   (1<<2)

#define FILENAME_MAXLEN 1024
#define MAX_NUM_OF_RINGS 30

#define MAX_API_URL 1024

#define MAX_LENGTH_STRING 1024

struct TYPE_PARAMS;

typedef struct {
	char      InRing[MAX_RING_STR];
	MSG_LOGO  *GetLogo;         /* array for requesting module,type,instid  */
	short     nLogo;            /* number of logos to get                   */
	struct TYPE_PARAMS *params;
} TYPE_PARAMS_STACKER;

typedef struct TYPE_PARAMS {
	char      *cmdname;          /* pointer to executable name argv[0] */ 

	/* Variables set from ew2openapi_config() before starting threads
	 **********************************************************************/
	int       nRings;
	char      InRings[MAX_NUM_OF_RINGS][MAX_RING_STR]; /* names of transport rings for input  */
	TYPE_PARAMS_STACKER pstacker[MAX_NUM_OF_RINGS];

	MSG_LOGO  *GetLogo;         /* array for requesting module,type,instid  */
	short     nLogo;            /* number of logos to get                   */

	char      MyModName[MAX_MOD_STR];  /* speak as this module name/id      */
	int       LogSwitch;        /* 0 if no logfile should be written        */
	long      MaxMsgSize;       /* max size for input/output msgs           */
	int       QueueSize;	    /* max messages in output circular buffer   */
	char      QueueFilename[FILENAME_MAXLEN];         /* Filename for dumping and undumping the queue */
	char      ewinstancename[256]; /* EW instance name */

	char      ServiceType[256];    /* Api Base URL */

	char      ApiBaseUrl[MAX_API_URL];    /* Api Base URL */
	int       WaitSecAfterServiceError; /* Seconds to wait after a Service Error */

	char      send_message_format_flags;  /* Flags for message format conversion. (RAW, JSON, GEOJSON) */

	char      kafka_brokers[256];       /* Kafka Broker(s) comma-separated-values of host or host:port (default port 9092) */
	char      kafka_topic_prefix[256];  /* Kafka Topic Prefix */

	char      rmq_hostname[256];   /* RabbitMQ hostname */
	int       rmq_port;            /* RabbitMQ port */
	char      rmq_exchange[256];   /* RabbitMQ exchange */
	char      rmq_vhost[256];      /* RabbitMQ virtual host */
	char      rmq_username[256];   /* RabbitMQ username */
	char      rmq_password[256];   /* RabbitMQ password */

	char      osc_hostname[256];   /* OSC Server hostname */
	int       osc_port;            /* OSC Server port */

	char      *TYPE_MSG_ARG;   /* Stand-alone Conversion Tool */
	char      *fnamein;        /* Stand-alone Conversion Tool */
	char      *fnameout;       /* Stand-alone Conversion Tool */

	int       flag_handler; /* Flag for selecting handler */

	/* Variables set from ew2openapi_lookup() before starting threads
	 **********************************************************************/
	int        HeartBeatInt;     /* seconds between heartbeat messages       */
	char       HeartBeatRing[MAX_RING_STR];    /* name of transport ring for input  */
	SHM_INFO   HeartBeatRegion;         /* shared memory region to use for input    */
	long       HeartBeatRingKey;       /* key of transport ring for input    */
	MSG_LOGO   HeartLogo;       /* logo of heartbeat message          */
	MSG_LOGO   ErrorLogo;       /* logo of error message              */

} TYPE_PARAMS;

/* data passed to the handler */
typedef struct {
	char           *Wrmsg;
	long           msgSize;
	long           inkey;
	MSG_LOGO       *reclogo;
	TYPE_PARAMS    *params;
	char           *ewUserName;
	char           *ewHostName;
} TYPE_DATA_HANDLER;

#endif

