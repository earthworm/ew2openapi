#ifndef EW2OPENAPI_HANDLER_OSC_H
#define EW2OPENAPI_HANDLER_OSC_H 1

#include <transport.h>
#include "ew2openapi_params.h"

#define  OSC_DEFAULT_PORT       57110   /* Default OSC Server port */

int ew2openapi_handler_osc_init(TYPE_PARAMS *params);

int ew2openapi_handler_osc_destroy(TYPE_PARAMS *params);

int ew2openapi_handler_osc_handle(TYPE_DATA_HANDLER *data_handler);


#endif

