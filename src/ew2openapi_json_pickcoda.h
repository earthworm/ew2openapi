#ifndef EW2OPENAPI_JSON_PICKCODA_H
#define EW2OPENAPI_JSON_PICKCODA_H 1

#include <transport.h>

char *get_json_string_from_pick_scnl_ew_msg(char *msg, int msg_size, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName, char flag_geojson);
char *get_json_string_from_coda_scnl_ew_msg(char *msg, int msg_size, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName);

#endif

