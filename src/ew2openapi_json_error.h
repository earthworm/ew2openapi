#ifndef EW2OPENAPI_JSON_ERROR_H
#define EW2OPENAPI_JSON_ERROR_H 1

#include <transport.h>

#ifdef _WINNT
#include <winsock.h>
#endif /* _WINNT */

char *get_json_string_from_error_ew_msg(char *msg, int msg_size, MSG_LOGO *reclogo, char *ewInstanceName, char *ewUserName, char *ewHostName);

#endif

