# ew2openapi

Earthworm module for transporting Earthworm messages from rings to:
  - CaravelDB OpenAPI Webservice (Dante)
  - Apache Kafka Topic  (Raw Earthworm message, Converted JSON or GeoJSON messages).
  - RabbitMQ exchanges (Raw Earthworm message, Converted JSON or GeoJSON messages).
  - Converted JSON or GeoJSON files.
  - OSC. Open Sound Control Protocol.

Earthworm messages can optionally be converted in JSON format.

[[_TOC_]]

## Usage

`ew2openapi` can be used in two different ways:

   1. Launched as an Earthworm module to redirect data into CaravelDB OpenAPI Webservices or RabbitMQ systems.
   2. Stand-alone to convert Earthworm message to JSON ew2openapi format

#### Earthworm module

```sh
ew2openapi ew2openapi.d
```

#### Stand-alone JSON command-line conversion tool

```sh
ew2openapi TYPE_HYP2000ARC ./20180205-080122.0009627.arc ./20180205-080122.0009627.json 2> out.log
```

```sh
cat ./20180205-080122.0009627.arc | ew2openapi TYPE_HYP2000ARC - ./20180205-080122.0009627.json 2> out.log
```

```sh
cat ./20180205-080122.0009627.arc | ew2openapi TYPE_HYP2000ARC - - > ./20180205-080122.0009627.json 2> out.log
```

```sh
ew2openapi TYPE_HYP2000ARC ./20180205-080122.0009627.arc - > ./20180205-080122.0009627.json 2> out.log
```

## Dependencies

  * Earthworm http://earthworm.isti.com/trac/earthworm/
  * librdkafka https://github.com/confluentinc/librdkafka
  * RabbitMQ C client https://github.com/alanxz/rabbitmq-c
  * OSC https://github.com/radarsat1/liblo.git
  * json-c https://github.com/json-c/json-c
  * libcurl https://curl.haxx.se/docs/manpage.html

## Installation

```
git clone --recursive git@gitlab.rm.ingv.it:earthworm/ew2openapi.git
cd ew2openapi
```

#### json-c

```
mkdir json-c-build
cd json-c-build
cmake -DBUILD_SHARED_LIBS=OFF -DDISABLE_WERROR=ON -DCMAKE_INSTALL_PREFIX=./ ../json-c
make
make install
```

#### rabbitmq-c

```
cd rabbitmq-c
mkdir build && cd build
cmake ..
# cmake -DENABLE_SSL_SUPPORT=OFF ..
cmake --build .
```

#### librdkafka

```
cd librdkafka
./configure --prefix=`pwd`/build --enable-static
make
make install
```

#### liblo

```
cd liblo
./autogen.sh --enable-static
make
```

#### ew2openapi

```
make -f makefile.ux
# or
# make -f makefile.ux static
```

## Configuration

Templates `.d` and `.desc` files for ew2openapi configuration are available within the directory `params` of the current repository.

#### Common configuration parameters

```sh
 MyModuleId     MOD_EW2OPENAPI       # module id for this program

# Multiple declaration of InRing. The first one is used for reporting
# heartbeat and status messages. Be careful with messages duplicated
# in different rings.
 InRing         WAVE_RING           # transport ring to use for input
 InRing         PICK_RING           # transport ring to use for input
 InRing         HYPO_RING           # transport ring to use for input
# Earlybird Message types
#  InRing         ALARM_RING          # transport ring to use for input

 HeartBeatInt   30                  # EW internal heartbeat interval (sec)
 LogFile        1                   # If 0, don't write logfile
                                    # if 2, write to module log but not to
                                    # stderr/stdout .
```

```sh
# Unique name that indentifies the EW instance is usually declared as variable. Inherit from Mole.
EWInstanceName    ${EWMOLE_INSTANCENAME}
```

Select message types to read.

```sh
# Message types usually read from PICK_RING
 GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_PICK_SCNL
 GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_CODA_SCNL
 GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_QUAKE2K
 GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_LINK

# Message types usually read from HYPO_RING
 GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_HYP2000ARC
 GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_MAGNITUDE
 GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_STRONGMOTIONII

# Earlybird Message types
# GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_PICKTWC
# GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_HYPOTWC
# GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_ALARM

# Other message types
# WARNING: if you use copystatus, the messages TYPE_HEARTBEAT and TYPE_ERROR
# are duplicated on different rings. If you want to archive them, read only
# from the destination ring of the copystatus.
  GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_HEARTBEAT
  GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_ERROR
```

Set which format to use to convert Earthworm message.

```sh
# SendMessageFormat declare which format use to convert Earthworm messages and send to RabbitMQ server.
# If no SendMessageFormat is declared then default is only RAW.
# Available message conversion format are RAW, JSON or GeoJSON. JSON and GeoJSON arei mutually exclusive.
# RAW format:     messages are sent to RabbitMQ server as they are
# JSON format:    messages are converted to JSON format before sending to RabbitMQ server
# GeoJSON format: messages are converted to GeoJSON format before sending to RabbitMQ server.
#                 Only the following message types can be converted to GeoJSON:
#                     TYPE_PICK_SCNL, TYPE_CODA_SCNL, TYPE_QUAKE2K, TYPE_HYP2000ARC
#                 Other types of messages will be converted to JSON with no geographic features.

# SendMessageFormat RAW
SendMessageFormat JSON
# SendMessageFormat GeoJSON
```

```sh
# Station list needed only for GeoJSON conversion
 maxsite     3500
 site_file   all_stations.hinv
```

#### CaravelDB OpenAPI parameters

```sh
# ew2openapi connection parameter
ApiBaseUrl           ${EW2OPENAPI_APIBASEURL}
```

#### RabbitMQ parameters

```sh
# Rabbit DB connection parameters
RMQHostname        ${RABBITMQ_HOSTNAME}
RMQPort            ${RABBITMQ_PORT}
RMQExchange        ${RABBITMQ_EXCHANGE}
RMQVirtualHost     ${RABBITMQ_VIRTUALHOST}
RMQUsername        ${RABBITMQUSER}
RMQPassword        ${RABBITMQPASS}
```


#### RabbitMQ Routing key

Earthworm messages are automatically redirected to routing key based on the following rule:

```sh
EW.<ewInstance>.<ringName>.<instName>.<modName>.<logoTypeName>.<messageFormat>
```

Some examples:

```sh
EW.hew3_mole.PICK_RING.INST_INGV.MOD_BINDER_EW.TYPE_QUAKE2K.RAW
EW.hew3_mole.WAVE_RING.INST_INGV.MOD_GAPLIST.TYPE_HEARTBEAT.JSON
EW.hew3_mole.HYPO_RING.INST_INGV.MOD_EQASSEMBLE.TYPE_HYP2000ARC.GEOJSON
EW.hew3_mole.PICK_RING.INST_INGV.MOD_PICK_EW.TYPE_PICK_SCNL.GEOJSON
```

## JSON Earthworm message conversion

Each Earthworm message is converted in a JSON object which contains all fields of the message.

#### TYPE_HEARTBEAT

Example of TYPE_HEARTBEAT Earthworm message:

```json
{
  "ewLogo":{
    "type":"TYPE_HEARTBEAT",
    "module":"MOD_WAVESERVERV_7",
    "installation":"INST_INGV",
    "user":"ew",
    "hostname":"hew3.int.ingv.it",
    "instance":"hew3_mole"
  },
  "ewMessage":{
    "time":"2017-04-06 11:27:51.000000",
    "pid":88900
  }
}
```

#### TYPE_ERROR

Example of TYPE_ERROR Earthworm message:

```json
{
  "ewLogo":{
    "type":"TYPE_ERROR",
    "module":"MOD_GAPLIST",
    "installation":"INST_INGV",
    "user":"ew",
    "hostname":"hew3.int.ingv.it",
    "instance":"hew3_mole"
  },
  "ewMessage":{
    "time":"2017-04-04 11:27:00.000000",
    "p":"1 Channel MGRO.HHE.GU.-- alive."
  }
}
```

#### TYPE_PICK_SCNL

Example of TYPE_PICK_SCNL Earthworm message:

```json
{
  "ewLogo":{
    "type":"TYPE_PICK_SCNL",
    "module":"MOD_PICK_EW",
    "installation":"INST_INGV",
    "user":"ew",
    "hostname":"hew3.int.ingv.it",
    "instance":"hew3_mole"
  },
  "ewMessage":{
    "pickId":6303684,
    "station":"VAGA",
    "component":"HHZ",
    "network":"IV",
    "location":"--",
    "firstMotion":"U",
    "pickWeight":"2",
    "timeOfPick":"2017-04-06 11:29:13.910000",
    "pAmplitude":[
      109,
      101,
      122
    ]
  }
}
```

#### TYPE_LINK

Example of TYPE_LINK Earthworm message:

```json
{
  "ewLogo":{
    "type":"TYPE_LINK",
    "module":"MOD_BINDER_EW",
    "installation":"INST_INGV",
    "user":"ew",
    "hostname":"hew3.int.ingv.it",
    "instance":"hew3_mole"
  },
  "ewMessage":{
    "quakeId":182491,
    "pickId":6303735,
    "iphs":2
  }
}
```

#### TYPE_QUAKE2K

Example of TYPE_QUAKE2K Earthworm message:

```json
{
  "ewLogo":{
    "type":"TYPE_QUAKE2K",
    "module":"MOD_BINDER_EW",
    "installation":"INST_INGV",
    "user":"ew",
    "hostname":"hew3.int.ingv.it",
    "instance":"hew3_mole"
  },
  "ewMessage":{
    "quakeId":182493,
    "originTime":"2017-04-06 11:30:25.170000",
    "latitude":42.5010,
    "longitude":12.2690,
    "depth":12.00,
    "rms":0.30,
    "dmin":53.3,
    "ravg":87.1,
    "gap":128,
    "nph":5
  }
}
```

#### TYPE_HYP2000ARC

Example of TYPE_HYP2000ARC Earthworm message:


```json
{
  "ewLogo":{
    "type":"TYPE_HYP2000ARC",
    "module":"MOD_EQASSEMBLE",
    "installation":"INST_INGV",
    "user":"ew",
    "hostname":"hew3.int.ingv.it",
    "instance":"hew3_mole"
  },
  "ewMessage":{
    "quakeId":182492,
    "version":"ew prelim",
    "originTime":"2017-04-06 11:30:28.570000",
    "latitude":42.997501,
    "longitude":12.820833,
    "depth":4.87,
    "nph":7,
    "nphS":2,
    "nphtot":8,
    "nPfm":2,
    "gap":157,
    "dmin":26,
    "rms":0.73,
    "e0az":255,
    "e0dp":61,
    "e0":5.54,
    "e1az":50,
    "e1dp":26,
    "e1":2.57,
    "e2":1.44,
    "erh":2.67,
    "erz":4.85,
    "Md":0.00,
    "reg":"   ",
    "labelpref":" ",
    "Mpref":0.00,
    "wtpref":0.00,
    "mdtype":"D",
    "mdmad":0.00,
    "mdwt":0.00,
    "ingvQuality":"DD",
    "phases":[
      {
        "sta":"ATCC",
        "comp":"EHZ",
        "net":"IV",
        "loc":"--",
        "Plabel":" ",
        "Slabel":" ",
        "Ponset":"P",
        "Sonset":" ",
        "Pat":"2017-04-06 11:30:33.849998",
        "Sat":"2017-04-06 11:30:00.000000",
        "Pres":0.09,
        "Sres":0.00,
        "Pqual":0,
        "Squal":0,
        "codalen":0,
        "codawt":4,
        "Pfm":" ",
        "Sfm":" ",
        "datasrc":"W",
        "Md":0.00,
        "azm":325,
        "takeoff":100,
        "dist":25.50,
        "Pwt":2.28,
        "Swt":0.00,
        "pamp":209,
        "codalenObs":0,
        "ccntr":[
          0,
          0,
          0,
          0,
          0,
          0
        ],
        "caav":[
          0,
          0,
          0,
          0,
          0,
          0
        ]
      },
...
...
...
      {
        "sta":"COR1",
        "comp":"EHZ",
        "net":"IV",
        "loc":"--",
        "Plabel":" ",
        "Slabel":" ",
        "Ponset":" ",
        "Sonset":"S",
        "Pat":"2017-04-06 11:31:39.989998",
        "Sat":"2017-04-06 11:30:52.470001",
        "Pres":57.76,
        "Sres":-0.41,
        "Pqual":4,
        "Squal":3,
        "codalen":0,
        "codawt":4,
        "Pfm":" ",
        "Sfm":" ",
        "datasrc":"W",
        "Md":0.00,
        "azm":11,
        "takeoff":56,
        "dist":72.00,
        "Pwt":0.00,
        "Swt":0.57,
        "pamp":171,
        "codalenObs":0,
        "ccntr":[
          0,
          0,
          0,
          0,
          0,
          0
        ],
        "caav":[
          0,
          0,
          0,
          0,
          0,
          0
        ]
      }
    ]
  }
}
```

#### TYPE_MAGNITUDE

Example of TYPE_MAGNITUDE Earthworm message:

```json
{
  "ewLogo":{
    "type":"TYPE_MAGNITUDE",
    "module":"MOD_LOCALMAG_FINAL",
    "installation":"INST_INGV",
    "user":"ew",
    "hostname":"hew3.int.ingv.it",
    "instance":"hew3_mole"
  },
  "ewMessage":{
    "quakeId":182491,
    "version":"ew final",
    "mag":3.34,
    "error":1.33,
    "quality":0.30,
    "minDist":33.13,
    "azimuth":-1,
    "nStations":5,
    "nChannels":10,
    "qAuthor":"014101073:028130073",
    "qddsVersion":0,
    "iMagType":1,
    "magType":"ML",
    "algorithm":"MED",
    "ingvQuality":"C ",
    "phases":[
      {
        "sta":"LPEL",
        "comp":"HHE",
        "net":"IV",
        "loc":"--",
        "mag":0.96,
        "dist":33.13,
        "corr":0.00,
        "time1":"2017-04-06 11:29:25.020000",
        "amp1":-0.0387,
        "period1":-1.0000,
        "time2":"2017-04-06 11:29:25.050000",
        "amp2":0.0456,
        "period2":-1.0000
      },
      {
        "sta":"LPEL",
        "comp":"HHN",
        "net":"IV",
        "loc":"--",
        "mag":1.00,
        "dist":33.13,
        "corr":0.00,
        "time1":"2017-04-06 11:29:24.210000",
        "amp1":-0.0467,
        "period1":-1.0000,
        "time2":"2017-04-06 11:29:23.740000",
        "amp2":0.0452,
        "period2":-1.0000
      },
...
...
...
      {
        "sta":"ZONE",
        "comp":"HHE",
        "net":"IV",
        "loc":"--",
        "mag":3.34,
        "dist":495.55,
        "corr":0.00,
        "time1":"2017-04-06 11:30:30.440000",
        "amp1":-0.0635,
        "period1":-1.0000,
        "time2":"2017-04-06 11:30:30.820000",
        "amp2":0.0703,
        "period2":-1.0000
      },
      {
        "sta":"ZONE",
        "comp":"HHN",
        "net":"IV",
        "loc":"--",
        "mag":3.35,
        "dist":495.55,
        "corr":0.00,
        "time1":"2017-04-06 11:30:30.230000",
        "amp1":-0.0926,
        "period1":-1.0000,
        "time2":"2017-04-06 11:30:30.130000",
        "amp2":0.0440,
        "period2":-1.0000
      }
    ]
  }
}
```


#### TYPE_STRONGMOTIONII

Example of TYPE_STRONGMOTIONII Earthworm message:

```json
{
  "ewLogo": {
    "type": "TYPE_STRONGMOTIONII",
    "module": "MOD_GMEW",
    "installation": "INST_INGV",
    "instance": "hew3_mole",
    "user": "ew",
    "hostname": "hew3.int.ingv.it"
  },
  "ewMessage": {
    "quakeId": 74682,
    "station": "APRC",
    "component": "HHE",
    "network": "IV",
    "location": "--",
    "qAuthor": "014101073:029111073",
    "time": "2018-05-22 08:47:13.446000",
    "alternateTime": "1970-01-01 00:00:00.000000",
    "alternateCode": 0,
    "pga": 0.00269,
    "pgaTime": "2018-05-22 08:47:13.446000",
    "pgv": 3.2e-05,
    "pgvTime": "2018-05-22 08:47:21.046000",
    "pgd": 9e-06,
    "pgdTime": "2018-05-22 08:47:14.436000",
    "RSA": [
      {
        "value": 0.000724,
        "period": 0.3
      },
      {
        "value": 0.000309,
        "period": 1
      },
      {
        "value": 4.9e-05,
        "period": 3
      }
    ]
  }
}
```

## GeoJSON Earthworm message conversion

GeoJSON conversion encapsulate JSON Earthworm message conversion within the field `properties` of a `FeatureCollection`. GeoJSON format can be used only when geographic references are available within a specific message type, or inderictely derived by a hypoinverse station file.

An example for a TYPE_QUAKE2K message is:

```json
{
  "type":"FeatureCollection",
  "features":[
    {
      "type":"Feature",
      "geometry":{
        "type":"Point",
        "coordinates":[
          14.9101,
          37.7016
        ]
      },
      "properties":{
        "ewLogo":{
          "type":"TYPE_QUAKE2K",
          "module":"MOD_BINDER_EW",
          "installation":"INST_INGV",
		  "user":"ew",
		  "hostname":"hew3.int.ingv.it",
          "instance":"hew3_mole"
        },
        "ewMessage":{
          "quakeId":182435,
          "originTime":"2017-04-06 09:56:53.700000",
          "latitude":37.7016,
          "longitude":14.9101,
          "depth":19.52,
          "rms":0.44,
          "dmin":5.3,
          "ravg":127.6,
          "gap":52,
          "nph":35
        }
      }
    }
  ]
}
```

## Author

Matteo Quintiliani - Istituto Nazionale di Geofisica e Vulcanologia - Italy

Mail bug reports and suggestions to matteo.quintiliani [@] ingv.it

