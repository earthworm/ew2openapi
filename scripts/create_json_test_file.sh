#!/bin/bash

# Test ew2openapi standalone with TYPE_PICK_SCNL input file
ew2openapi TYPE_PICK_SCNL ./test/pick_ew/message.pick_scnl ./test/json/message.pick_scnl.json

# Test ew2openapi standalone with TYPE_QUAKE2K input file
ew2openapi TYPE_QUAKE2K ./test/binder_ew/message.quake2k ./test/json/message.quake2k.json

# Test ew2openapi standalone with TYPE_LINK input file
ew2openapi TYPE_LINK ./test/binder_ew/message.link ./test/json/message.link.json

# Test ew2openapi standalone with ARC input file
ew2openapi TYPE_HYP2000ARC ./test/arc/20180205-080122.0009627.arc ./test/json/20180205-080122.0009627.json
    # - sed -e 's/"hostname":[[:space:]]*"\([^"]\+\)"/"hostname": "DIFF_IGNORE_HOSTNAME"/' j1.json > jj1.json
    # - sed -e 's/"hostname":[[:space:]]*"\([^"]\+\)"/"hostname": "DIFF_IGNORE_HOSTNAME"/' j2.json > jj2.json
    # - grep -w -v "hostname" j1.json > jj1.json
    # - grep -w -v "hostname" j2.json > jj2.json
    # - diff jj1.json jj2.json

# Test ew2openapi standalone with ARC input file
ew2openapi TYPE_HYP2000ARC ./test/arc/20170118-1014097.arc ./test/json/20170118-1014097.json
    # - sed -e 's/"hostname":[[:space:]]*"\([^"]\+\)"/"hostname": "DIFF_IGNORE_HOSTNAME"/' j1.json > jj1.json
    # - sed -e 's/"hostname":[[:space:]]*"\([^"]\+\)"/"hostname": "DIFF_IGNORE_HOSTNAME"/' j2.json > jj2.json
    # - grep -w -v "hostname" j1.json > jj1.json
    # - grep -w -v "hostname" j2.json > jj2.json
    # - diff jj1.json jj2.json

# Test ew2openapi standalone with TYPE_MAGNITUDE input file
ew2openapi TYPE_MAGNITUDE ./test/localmag/lm_75662.localmag ./test/json/lm_75662.localmag.json

# Test ew2openapi standalone with TYPE_STRONGMOTIONII input file
ew2openapi TYPE_STRONGMOTIONII ./test/gmew/APRC.HHE.IV.--.001.gmew ./test/json/APRC.HHE.IV.--.001.gmew.json
    # - sed -e 's/"hostname":[[:space:]]*"\([^"]\+\)"/"hostname": "DIFF_IGNORE_HOSTNAME"/' j1.json > jj1.json
    # - sed -e 's/"hostname":[[:space:]]*"\([^"]\+\)"/"hostname": "DIFF_IGNORE_HOSTNAME"/' j2.json > jj2.json
    # - grep -w -v "hostname" j1.json > jj1.json
    # - grep -w -v "hostname" j2.json > jj2.json
    # - diff jj1.json jj2.json

