#!/bin/bash

# DANTEURLBASE=http://caravel.int.ingv.it
# DANTEURLBASE=http://caravel.int.ingv.it:8585
DANTEURLBASE=$1
INPUTFILE=$2

DIRNAME="`dirname $0`"
CURLFORMATFILE="${DIRNAME}/curl-format.txt"

SYNTAX="
Syntax: `basename $0`  <dantebaseurl>  [ <ew2openapi_log_file> ]

Examples:

      `basename $0`  http://caravel.int.ingv.it ew2openapi1_20211029.log

      grep TYPE_HYP2000ARC ew2openapi1_20211029.log | `basename $0`  http://caravel.int.ingv.it

      cat ew2openapi?_202111*.log | `basename $0`  http://caravel.int.ingv.it

"

DANTEURLPREFIX=api/quakedb/ew/v1

if [ -z "${DANTEURLBASE}" ]; then
	echo "${SYNTAX}"
	echo "ERROR: <dantebaseurl> is required. Exit."
	exit
fi

if [ -z "${INPUTFILE}" ]; then
	echo "WARNING: <ew2openapi_log_file> is not defined. Script expects input from stdin."
	INPUTFILE="-"
else
	if [ ! -f "${INPUTFILE}" ]; then
		echo "${SYNTAX}"
		echo "ERROR: file ${INPUTFILE} not found. Exit."
		exit
	fi
fi


cat ${INPUTFILE} | \
	grep "JSON:" | \
	sed -e "s/^.*:\([0-9]\{8\}_UTC_\)/\1/" | \
	sort | \
	sed -e "s/^.*JSON: //" | \
	sed -e 's/"instance":"/"instance":"test_logplayer_/g' | \
	while read JSONDATA; do
		echo ""
		echo "========================="
		EWTYPEMSG=`echo "${JSONDATA}" | sed -e 's/^{"ewLogo":{"type":"\([^"][^"]*\)".*$/\1/'`
		case ${EWTYPEMSG} in
			TYPE_QUAKE2K)
				DANTEURLAPI=quake2k
				;;
			TYPE_HYP2000ARC)
				DANTEURLAPI=hyp2000arc
				;;
			TYPE_MAGNITUDE)
				DANTEURLAPI=magnitude
				;;
			TYPE_PICK_SCNL)
				DANTEURLAPI=pick-scnl
				;;
			TYPE_STRONGMOTIONII)
				DANTEURLAPI=strongmotionii
				;;
			*)
				echo "ERROR: message type ${EWTYPEMSG} not handled. Skip."
				DANTEURLAPI=""
				;;
		esac


		if [ ! -z "${DANTEURLAPI}" ]; then
			DANTEURLCOMPLETE=${DANTEURLBASE}/${DANTEURLPREFIX}/${DANTEURLAPI}
			# echo "${EWTYPEMSG} --> ${DANTEURLAPI}"
			# echo ${JSONDATA}
			echo curl -i -w "@${CURLFORMATFILE}"  -d "{\"data\": ${JSONDATA} }" -H "Content-Type: application/json" -X POST ${DANTEURLCOMPLETE}
			echo ""
			curl -i -w "@${CURLFORMATFILE}"  -d "{\"data\": ${JSONDATA} }" -H "Content-Type: application/json" -X POST ${DANTEURLCOMPLETE}
			# sleep 3
			echo ""
		fi
	done

