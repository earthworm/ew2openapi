#!/bin/bash

DIRNAME="`dirname $0`"

FILE_HISTORY=${DIRNAME}/../HISTORY
FILE_VERSION=${DIRNAME}/../VERSION
FILE_ew2openapi_version=${DIRNAME}/../src/ew2openapi_version.h

RELEASE_PREFIX_STRING="* Release"

# grep "${RELEASE_PREFIX_STRING}" ${FILE_HISTORY} | head -n 1 | sed -e "s/${RELEASE_PREFIX_STRING}//" | sed  -e "s/^[ ]*//" -e "s/[ ]*$//" -e "s/[ ][ ]*/ /g" -e "s/[()]//g" -e "s/\//-/g" | awk '{printf "%s - %s", $1, $2;}' > ${FILE_VERSION}
grep "${RELEASE_PREFIX_STRING}" ${FILE_HISTORY} | head -n 1 | sed -e "s/${RELEASE_PREFIX_STRING}//" | sed  -e "s/^[ ]*//" -e "s/[ ]*$//" -e "s/[ ][ ]*/ /g" -e "s/\//-/g" | awk '{printf "%s %s", $1, $2;}' > ${FILE_VERSION}

VERSION="`cat ${FILE_VERSION}`"

echo "/* DO NOT EDIT. Automatically generated. Change file HISTORY in ew2openapi_version main directory. */" > ${FILE_ew2openapi_version}
echo "#define EW2OPENAPI_NAME \"ew2openapi\"" >> ${FILE_ew2openapi_version}
echo "#define EW2OPENAPI_VERSION \"${VERSION}\"" >> ${FILE_ew2openapi_version}
echo "#define EW2OPENAPI_NAME_AND_VERSION EW2OPENAPI_NAME\" \"EW2OPENAPI_VERSION" >> ${FILE_ew2openapi_version}
echo "#define EW2OPENAPI_USERAGENT EW2OPENAPI_NAME\"/\"EW2OPENAPI_VERSION" >> ${FILE_ew2openapi_version}
echo "#define EW2OPENAPI_URL_PATH \"quakedb/ew/v1\"" >> ${FILE_ew2openapi_version}
echo "#define EW2OPENAPI_URL_PATH_STATUS \"status\"" >> ${FILE_ew2openapi_version}

cat ${FILE_ew2openapi_version}

